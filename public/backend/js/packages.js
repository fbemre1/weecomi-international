$(document).ready(function(){

  // Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}



// Base url al bulunduğu dizin neyse.
var base_url = window.location.origin;

// Datatables hazırla.
var table = $("#packages").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
  buttons: [
		    {text: 'Yeni ekle',action: function ( e, dt, node, config ) {
					$('#createpackage').modal('show');
                }},
        { extend: 'colvis', text:'Göster/Gizle' },
        { extend: 'copy', text: 'Kopyala' },
        { extend: 'excel', text: 'Excele Aktar' },
        { extend: 'print', text: 'Yazdır' },


    ],
  language: {
        url: base_url+"/backend/assets/js/Turkish.json",
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/paketayarlaricek",
  "columns":[
    {"data":"id",className: "tableid"},
		{"data":"name"},
		{"data":"price"},
		{"data":"tax"},
		{"data":"factor"},
		{"data":"limit"},
		{"data":"created_at"},
		{"data":null}
  ],
  "columnDefs": [
            {
                "data":null,
                "targets": [7],
                "defaultContent": '<button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">İşlem</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:;" id="view" data-toggle="modal" data-target="#editor">Düzenle</a><a class="dropdown-item" href="javascript:;" id="delete">Sil</a></div>',
              }],
  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});



// Datatables hazırla.
var table1 = $("#packagesdetails").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
	buttons: [
		{text: 'Yeni ekle',action: function ( e, dt, node, config ) {
			$('#createdetail').modal('show');
						}},
        { extend: 'colvis', text:'Göster/Gizle' },
        { extend: 'copy', text: 'Kopyala' },
        { extend: 'excel', text: 'Excele Aktar' },
        { extend: 'print', text: 'Yazdır' },
    ],
  language: {
        url: base_url+"/backend/assets/js/Turkish.json",
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/paketdetaygetir",
  "columns":[
    {"data":"id",className: "tableidd"},
    {"data":"package_id"},
		{"data":"text",className:"detailtext"},
		{"data":"created_at"},
		{"data":null}
  ],
  "columnDefs": [
            {
                "data":null,
                "targets": [4],
                "defaultContent": '<button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">İşlem</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:;" id="detailupdater" data-toggle="modal" data-target="#detailupdate">Düzenle</a><a class="dropdown-item" href="javascript:;" id="deletedetails">Sil</a></div>',
              }],
  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});




// silme işlemi.
$(".show").on("click",".show > #delete",function(){
var index = $("#delete").index(this);
var id = $("td.tableid").eq(index).text();
var _token = $("input[name=_token]").val();


toastr.warning("<button type='button' id='confirmationRevertYes' class='btn clear'>Evet</button>&nbsp;<button type='button' class='btn clear'>Hayır</button>",'İşlemi gerçekleşmek istediğinizden emin misiniz ?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
          $("#confirmationRevertYes").click(function(){

            $.ajax({
              url:base_url+"/panel/paketsil",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){
          				toastr[r.status](r.head,r.content);
                  table.ajax.reload();
              }
            });

          });
        }
  });

});


$(".show").on("click",".show > #deletedetails",function(){
var index = $("#deletedetails").index(this);
var id = $("td.tableidd").eq(index).text();
var _token = $("input[name=_token]").val();


toastr.warning("<button type='button' id='confirmationRevertYes' class='btn clear'>Evet</button>&nbsp;<button type='button' class='btn clear'>Hayır</button>",'İşlemi gerçekleşmek istediğinizden emin misiniz ?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
          $("#confirmationRevertYes").click(function(){

            $.ajax({
              url:base_url+"/panel/paketdetaysil",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){
          				toastr[r.status](r.head,r.content);
                  table1.ajax.reload();
              }
            });

          });
        }
  });

});


// Düzenleme İşlemi bilgileri getir.
$(".show").on("click",".show > #view",function(){


var index = $("#view").index(this);
var id = $("td.tableid").eq(index).text();
$("#package_id").val(id);

var _token = $("input[name=_token]").val();

            $.ajax({
              url:base_url+"/panel/paketgetir",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){

                $("#package_name").val(r.name);
                $("#package_price").val(r.price);
                $("#package_kdv").val(r.tax);
                $("#package_factor").val(r.factor);
                $("#package_limit").val(r.limit);


              }
            });

});



// Düzenleme İşlemi bilgileri getir.
$(".show").on("click",".show > #detailupdater",function(){


var index = $("#detailupdater").index(this);
var id = $("td.tableidd").eq(index).text();
$("#detailerid").val(id);
var _token = $("input[name=_token]").val();

            $.ajax({
              url:base_url+"/panel/paketdetayduzenlecek",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){
                $("#selectorpackage").val(r.package_id);
								$("#package_textr").val(r.text);



              }
            });

});


$("#form1").ajaxForm({
	type:"POST",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table.ajax.reload();
	}
});


$("#form2").ajaxForm({
	type:"POST",
	url:base_url+"/panel/paketekle",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table.ajax.reload();
	}
});

$("#form3").ajaxForm({
	type:"POST",
	url:base_url+"/panel/paketdetayekle",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table1.ajax.reload();
	}
});

$("#form4").ajaxForm({
	type:"POST",
	url:base_url+"/panel/paketdetayduzenle",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table1.ajax.reload();
	}
});


// ilgili formu validasyon gerçekleştir.
    $("#form1").validate({
      rules: {
        package_name: {
          required: true,
        },
        package_price: {
          required: true,
        },
        package_kdv: {
          required: true,
        },
        package_factor: {
          required: true,
        },
				package_limit: {
          required: true,
        },
      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});

// ilgili formu validasyon gerçekleştir.
    $("#form2").validate({
      rules: {
        package_name: {
          required: true,
        },
        package_price: {
          required: true,
        },
        package_kdv: {
          required: true,
        },
        package_factor: {
          required: true,
        },
				package_limit: {
          required: true,
        },
      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});


// ilgili formu validasyon gerçekleştir.
    $("#form3").validate({
      rules: {
        package_id: {
          required: true,
        },
        package_text: {
          required: true,
        },
      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});


// ilgili formu validasyon gerçekleştir.
    $("#form4").validate({
      rules: {
        package_id: {
          required: true,
        },
        package_text: {
          required: true,
        },
      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});




});
