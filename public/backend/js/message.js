$(document).ready(function(){


	// Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}

// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;


// ilgili formu post et.
    $("#form").ajaxForm({
      type:"POST",
      success:function(r){

				if(r.notify == "success"){
				$(".alert").attr("class","alert alert-success");
				$(".alert").html("<b>Mesaj Durumu = Açık</b>");
			  }
				else{
					$(".alert").attr("class","alert alert-dark");
					$(".alert").html("<b>Mesaj Durumu = Kapalı</b>");
				}

				var msg = $("#msgcnt").val();
				toastr[r.status](r.head,r.content);
				var clone = '<div class="card-body buffymag"> <div class="media"> <img class="d-flex mr-3 height-50" src="/user_avatar/default.png" alt="Generic placeholder image"> <div class="media-body"> <h6 class="mt-0 mb-1 font-weight-normal">'+r.fullname+'</h6> <small>2018-08-29 16:32:19</small> <div class="collapse my-3 show" id="message1"> <div> '+msg+' </div> </div> </div> </div> </div>';
				$(".msgcontent").prepend(clone);
				$("#msgcnt").val("");
      }
    });

// ilgili formu validasyon gerçekleştir.
    $("#form").validate({
      rules: {
        msg_content: {
          required: true,
          minlength:2
        }


      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});




  });
