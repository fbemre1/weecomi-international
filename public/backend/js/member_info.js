$(document).ready(function(){

	// Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}

// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;

// ilgili formu post et.
    $("#formpost").ajaxForm({
      type:"POST",
      success:function(r){
				toastr[r.status](r.head,r.content);
      }
    });

// ülkeleri getir.
    var country_id = $(".countries option:selected").val();
    $.ajax({
      type:"GET",
      url: base_url+"/sehirler/"+country_id,
      success:function(r){
        $(".cities").html(r);
      }
    });

// şehirleri getir.
    $(".countries").change(function(){

      var country_id = $(".countries option:selected").val();


      $.ajax({
        type:"GET",
        url: base_url+"/sehirler/"+country_id,
        success:function(r){
          $(".cities").html(r);
        }
      });

    });

// ilgili formu validasyon gerçekleştir.
    $("#formpost").validate({
      rules: {
        firstname: {
          required: true,
          minlength:2
        },
        lastname: {
          required: true,
          minlength:2
        },
        identity: {
          required: true,
          minlength:2
        },
        email: {
          required: true,
        },
        telephone: {
          required: true,
        },
        birthday: {
          required: true,
        },
        country: {
          required: true,
        },
        city: {
          required: true,
        },
        district: {
          required: true,
        },
        address: {
          required: true,
          minlength:5,
        },

      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});


// Fake file seçici
$("#fileselector").click(function(){
  $(".file").trigger('click');
});

// form avatar olayları
$(".file").change(function(index){

  console.log($(this).get(0).files[0].type);

  if($(this).get(0).files[0].type == "image/jpeg" || $(this).get(0).files[0].type == "image/png" || $(this).get(0).files[0].type == "image/jpeg"){

    if($(this).get(0).files.length == 0){
      $(".result").html("resim seçilmedi.");
    }
    else{

      var preview = $(".preview");

      var input = $(event.currentTarget);
      var file = input[0].files[0];
      var reader = new FileReader();

      reader.onload = function(e){
      image_base64 = e.target.result;
      preview.html("<img src='"+image_base64+"'/>");
      };
      reader.readAsDataURL(file);

      console.log(file);

      var photoname = $(this).get(0).files[0].name;
      $(".result").html(photoname+" resim seçildi.");

    }


  }
  else{
    alert("sadece jpg,png,jpeg uzantıları geçerlidir.");
  }



});


  });
