$(document).ready(function(){

  // Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}



// Base url al bulunduğu dizin neyse.
var base_url = window.location.origin;

// Datatables hazırla.
var table = $("#pendingmembers").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
  buttons: [
        { extend: 'colvis', text:'Göster/Gizle' },
        { extend: 'copy', text: 'Kopyala' },
        { extend: 'excel', text: 'Excele Aktar' },
        { extend: 'print', text: 'Yazdır' },


    ],
  language: {
        url: base_url+"/backend/assets/js/Turkish.json",
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/onaybekleyenuyelercek",
  "columns":[
    {"data":"id",className: "tableid"},
    {"data":"sponsor_code"},
		{"data":"fullname"},
		{"data":"email"},
		{"data":"telephone"},
		{"data":"package_name"},
		{"data":"register_date"},
    {"data":null}
  ],
  "columnDefs": [
            {
                "data":null,
                "targets": [ 7 ],
                "defaultContent": '<button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">İşlem</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:;" id="approve">Onayla</a><a class="dropdown-item" href="javascript:;" id="view" data-toggle="modal" data-target="#editor">Düzenle</a><a class="dropdown-item" href="javascript:;" id="delete">Sil</a></div>',
              }],
  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});


// Onaylama İşlemi.
$(".show").on("click",".show > #approve",function(){
var index = $("#approve").index(this);
var id = $("td.tableid").eq(index).text();
var _token = $("input[name=_token]").val();


toastr.warning("<button type='button' id='confirmationRevertYes' class='btn clear'>Evet</button>&nbsp;<button type='button' class='btn clear'>Hayır</button>",'İşlemi gerçekleşmek istediğinizden emin misiniz ?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
          $("#confirmationRevertYes").click(function(){

            $.ajax({
              url:base_url+"/panel/onaybekleyenuyeleronayla",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){
          				toastr[r.status](r.head,r.content);
                  table.ajax.reload();
              }
            });


          });
        }
  });




});


// silme işlemi.
$(".show").on("click",".show > #delete",function(){
var index = $("#delete").index(this);
var id = $("td.tableid").eq(index).text();
var _token = $("input[name=_token]").val();


toastr.warning("<button type='button' id='confirmationRevertYes' class='btn clear'>Evet</button>&nbsp;<button type='button' class='btn clear'>Hayır</button>",'İşlemi gerçekleşmek istediğinizden emin misiniz ?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
          $("#confirmationRevertYes").click(function(){

            $.ajax({
              url:base_url+"/panel/onaybekleyenuyelersil",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){
          				toastr[r.status](r.head,r.content);
                  table.ajax.reload();
              }
            });


          });
        }
  });

});


// Düzenleme İşlemi.
$(".show").on("click",".show > #view",function(){

var index = $("#delete").index(this);
var id = $("td.tableid").eq(index).text();
$("#rowid").val(id);
var _token = $("input[name=_token]").val();

            $.ajax({
              url:base_url+"/panel/onaybekleyenuyelerduzenlegetir",
              type:"POST",
              data:{id:id,_token:_token},
              success:function(r){
                $("#firstname").val(r.firstname);
                $("#lastname").val(r.lastname);
                $("#email").val(r.email);
                $("#telephone").val(r.telephone);
                $('#package_id').val(r.package_id);
                $('#bank_id').val(r.bank_id);

              }
            });

});







$("#form").ajaxForm({
	type:"POST",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table.ajax.reload();
	}
});


// ilgili formu validasyon gerçekleştir.
    $("#form").validate({
      rules: {
        firstname: {
          required: true,
          minlength:2
        },
        lastname: {
          required: true,
          minlength:2
        },
        email: {
          required: true,
        },
        telephone: {
          required: true,
        },
				package_id: {
          required: true,
        },
				bank_id: {
          required: true,
        },

      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});




});
