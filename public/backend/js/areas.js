// Base url al bulunduğu dizin neyse.
var base_url = window.location.origin;

$(document).ready(function(){

  // Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}





// Datatables hazırla.
var table = $("#pendingmembers").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
  buttons: [
		{text: 'Ülke Seç',action: function ( e, dt, node, config ) {
			      $('#countryselect').modal('show');
						}},
				{text: 'Yeni ekle',action: function ( e, dt, node, config ) {
					$('#addarea').modal('show');
								}},
        { extend: 'colvis', text:'Göster/Gizle' },
        { extend: 'copy', text: 'Kopyala' },
        { extend: 'excel', text: 'Excele Aktar' },
        { extend: 'print', text: 'Yazdır' },


    ],
  language: {
        url: base_url+"/backend/assets/js/Turkish.json",
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/bolgegetir",
  "columns":[
    {"data":"id",className: "tableid"},
    {"data":"area_name"},
		{"data":"created_at"},
    {"data":null}
  ],
  "columnDefs": [
            {
                "data":null,
                "targets": [ 3 ],
                "defaultContent": '<button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">İşlem</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:;" id="cityaddx" data-toggle="modal" data-target="#cityadd">Şehir Ekle</a><a class="dropdown-item" href="javascript:;" id="citydeletex" data-toggle="modal" data-target="#citydelete">Şehir Sil</a><a class="dropdown-item" href="javascript:;" id="view" data-toggle="modal" data-target="#areaedit">Düzenle</a><a class="dropdown-item" href="javascript:;" id="delete">Sil</a></div>',
              }],
  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});



// Düzenleme İşlemi.
$(".show").on("click",".show > #view",function(){

var data = table.row( $(this).parents('tr') ).data();

$("#areaname").val(data.area_name);
$("#country").val(data.country_id);
$("#area_id").val(data.id);


});




$(".show").on("click",".show > #cityaddx",function(){

var data = table.row( $(this).parents('tr') ).data();
$(".city_area_id").val(data.id);
// ilk önce eklenmiş varmı bak.


$.ajax({
	url:base_url+"/sehirleracik/"+data.country_id,
	type:"GET",
	success:function(r){
		$("#city").html(r);
	}
});


});



$(".show").on("click",".show > #citydeletex",function(){

var data = table.row( $(this).parents('tr') ).data();
$(".city_area_id").val(data.id);
// ilk önce eklenmiş varmı bak.


$.ajax({
	url:base_url+"/panel/sehirduzenle/"+data.id,
	type:"GET",
	success:function(r){
		$(".cityeditor").html(r);
	}
});


});







$("#areaform").ajaxForm({
	type:"POST",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table.ajax.reload();
	}
});


// ilgili formu validasyon gerçekleştir.
    $("#areaform").validate({
      rules: {
        area_name: {
          required: true,
        },
				country_id: {
          required: true,
        },
      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});


$("#editarform").ajaxForm({
	url:base_url+"/panel/bolgeduzenle",
	type:"POST",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table.ajax.reload();
	}
});


// ilgili formu validasyon gerçekleştir.
    $("#editarform").validate({
      rules: {
        area_name: {
          required: true,
        },
				country_id: {
          required: true,
        },
      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});


$("#cityadd").ajaxForm({
	url:base_url+"/panel/sehirekle",
	type:"POST",
	success:function(r){
		toastr[r.status](r.head,r.content);
		table.ajax.reload();
	}
});


// ilgili formu validasyon gerçekleştir.
$("#city_add").validate({
	rules: {
		'city[]': {
			required: true,
		},
	},
errorPlacement: function(error, element) {
	error.insertAfter(element);
}
});





});


// Dom element Fonks.

function removecity(event){

console.log(event.parent());

return false;

	toastr.warning("<button type='button' id='confirmationRevertYes' class='btn clear'>Evet</button>&nbsp;<button type='button' class='btn clear'>Hayır</button>",'İşlemi gerçekleşmek istediğinizden emin misiniz ?',
	  {
	      closeButton: false,
	      allowHtml: true,
	      onShown: function (toast) {

document.getElementById("confirmationRevertYes").onclick = function(){
	$.ajax({
		url:base_url+"/panel/sehirsil/"+id,
		type:"GET",
		processData: false,
		data:{id:id},
		success:function(r){
				toastr[r.status](r.head,r.content);
		}
	});

}





	        }
	  });

}
