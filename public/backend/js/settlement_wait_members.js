$(document).ready(function(){


	// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;
  // Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}


// Translate işlemleri başla.
var scripts = document.getElementsByTagName('script');
keys = [];
var bali;
var language;
$(scripts).each(function(index){
bali = scripts[index].src;

var finded = bali.includes("settlement_wait_members.js");

if(finded == true){
	var pack =  scripts[index].src;
	var pack = pack.split("=");
	language = pack[1];
}

});
// Translate işlemleri bitiş.

console.log("Language Datatables = "+language);

// Datatables hazırla.
var table = $("#settlementmembers").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
  buttons: [
        { extend: 'colvis', text:'Göster/Gizle' },
        { extend: 'copy', text: 'Kopyala' },
        { extend: 'excel', text: 'Excele Aktar' },
        { extend: 'print', text: 'Yazdır' },
    ],
  language: {
        url: base_url+"/backend/assets/js/datatableslang/"+language,
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/yerlesimbekleyenuyelercek",
  "columns":[
    {"data":"id",className: "tableid"},
    {"data":"sponsor_code"},
		{"data":"fullname"},
		{"data":"email"},
		{"data":"telephone"},
		{"data":"package_name"},
		{"data":"register_date"},
    {"data":null}
  ],
  "columnDefs": [
            {
                "data":null,
                "targets": [ 7 ],
                "defaultContent": '<button type="button" id="selector" class="btn btn-block btn-primary">Yerini Seç</button></div>',
              }],
  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});


// Yerleşme İşlemi.
$("#settlementmembers tbody ").on("click","#selector",function(){
var index = $("#selector").index(this);
var id = $("td.tableid").eq(index).text();
var _token = $("input[name=_token]").val();

location.replace("/panel/yerlesimagacim/?user_id="+id);

});







});
