$(document).ready(function() {

	var base_url = window.location.origin;

	$(".accordion").on("click", ".clicks", function(evt) {

	var id = evt.currentTarget.id;
	var icon = evt.currentTarget.childNodes[0];
	
	if(evt.currentTarget.childNodes[0].className == "icon-plus"){
		evt.currentTarget.innerHTML = "<i class='icon-minus'></i>";
	}
	else{
		evt.currentTarget.innerHTML = "<i class='icon-plus'></i>";
	}


	if(evt.currentTarget.offsetParent.nextElementSibling.className == "submenu"){
		evt.currentTarget.offsetParent.nextElementSibling.remove();
		return false;
	}


		$.ajax({
			url:base_url+"/panel/ekibimgetir/"+id,
			type:"get",
			beforeSend: function() {
				$(evt.currentTarget).parent("li").after("<img class='spinnerx' src='https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif' width=100px>");
      },
			success:function(r){
				$(evt.currentTarget).parent("li").after(r);
				$(".spinnerx").remove();
			}
		});

	});

});
