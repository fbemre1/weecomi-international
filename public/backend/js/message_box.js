$(document).ready(function(){




	// Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}

// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;


  $( "#autocek" ).autocomplete({
				source: function(request, response) {
    $.ajax({
        url: base_url+'/panel/gelenkutusukulgetir',
        dataType: "json",
        cache: false,
        type: "get",
        data: { term: request.term }
        }).done(function(data) {
				$(".userid").val(data.id);
        response(data);
        });
        }

			});
	$( "#autocek" ).autocomplete( "option", "appendTo", "#form" );

// ilgili formu post et.
    $("#form").ajaxForm({
			url:base_url+"/panel/gelenkutusu",
      type:"POST",
      success:function(r){
				toastr[r.status](r.head,r.content);

				if(r.status == "success"){
					location.replace("");
				}

      }
    });

// ülkeleri getir.
    var country_id = $(".countries option:selected").val();
    $.ajax({
      type:"GET",
      url: base_url+"/sehirler/"+country_id,
      success:function(r){
        $(".cities").html(r);
      }
    });

// şehirleri getir.
    $(".countries").change(function(){

      var country_id = $(".countries option:selected").val();


      $.ajax({
        type:"GET",
        url: base_url+"/sehirler/"+country_id,
        success:function(r){
          $(".cities").html(r);
					$(".cities").prepend("<option selected disabled value=''>Seçiniz</option>");
        }
      });

    });

// ilgili formu validasyon gerçekleştir.
    $("#form").validate({
      rules: {
        message_content: {
          required: true,
          minlength:2
        },
				category: {
          required: true,
        },
				subject: {
          required: true,
        },

      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});




  });
