$(document).ready(function(){

	// Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}

// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;

// ilgili formu post et.
    $("#formpost").ajaxForm({
      type:"POST",
      success:function(r){
				toastr[r.status](r.head,r.content);
      }
    });


// ilgili formu validasyon gerçekleştir.
    $("#formpost").validate({
      rules: {
        registrar_id: {
          required: true,
        },
				parent_id: {
          required: true,
        },
				left_side: {
          required: true,
        },
				right_side: {
          required: true,
        },

      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});




  });
