$(document).ready(function(){

	// Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}


// şehirleri getir.
    $(".countries").change(function(){

      var country_id = $(".countries option:selected").val();


      $.ajax({
        type:"GET",
        url: base_url+"/sehirler/"+country_id,
        success:function(r){
          $(".cities").html(r);
        }
      });

    });

// Fake file seçici
$("#fileselector").click(function(){
  $("#filesec").trigger('click');
});

// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;

// ilgili formu post et.
    $("#formpost").ajaxForm({
      type:"POST",
      success:function(r){
				toastr[r.status](r.head,r.content);
      }
    });


// ilgili formu validasyon gerçekleştir.
    $("#formpost").validate({
      rules: {
        sponsor_code: {
          required: true,
        },
				sector: {
          required: true,
        },
				store_name: {
          required: true,
        },
				title: {
          required: true,
        },
				title: {
          required: true,
        },
				taxador_number: {
          required: true,
        },
				username: {
          required: true,
        },
				email: {
          required: true,
        },
				telephone: {
          required: true,
        },
				discount_rate: {
          required: true,
        },
				maps: {
          required: true,
        },
				working_start: {
          required: true,
        },
				working_end: {
          required: true,
        },
				country: {
          required: true,
        },
				city: {
          required: true,
        },
				district: {
          required: true,
        },
				address: {
          required: true,
        },


      },
  errorPlacement: function(error, element) {
      error.insertAfter(element);
  }
});




  });
