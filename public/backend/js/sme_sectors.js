$(document).ready(function(){


	// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;
  // Toastr Ayarları.
	toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "progressBar":true,
}


// Translate işlemleri başla.
var scripts = document.getElementsByTagName('script');
keys = [];
var bali;
var language;
$(scripts).each(function(index){
bali = scripts[index].src;

var finded = bali.includes("sme_sectors.js");

if(finded == true){
	var pack =  scripts[index].src;
	var pack = pack.split("=");
	language = pack[1];
}

});
// Translate işlemleri bitiş.

console.log("Language Datatables = "+language);

// Datatables hazırla.
var table = $("#sme_sectors").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
  buttons: [
		    {
						text: 'Sektör Ekle',
						action: function ( e, dt, node, config ) {
							$('#addsector').modal('show');
						}
				},
        { extend: 'colvis', text:'Göster/Gizle' },
        { extend: 'copy', text: 'Kopyala' },
        { extend: 'excel', text: 'Excele Aktar' },
        { extend: 'print', text: 'Yazdır' },

    ],
  language: {
        url: base_url+"/backend/assets/js/datatableslang/"+language,
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/sektorcek",
  "columns":[
    {"data":"id",className: "tableid"},
    {"data":"sector_tr"},
		{"data":"sector_en"},
		{"data":"created_at"},
    {"data":null}
  ],
	"columnDefs": [
						{
								"data":null,
								"targets": [ 4 ],
								"defaultContent": '<button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">İşlem</button><div class="dropdown-menu"><a class="dropdown-item" href="javascript:;" id="view" data-toggle="modal" data-target="#editor">Düzenle</a><a class="dropdown-item" href="javascript:;" id="delete">Sil</a></div>',
							}],
  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});


// update işlemi
$('#sme_sectors tbody').on( 'click', '#view', function () {
        var data = table.row( $(this).parents('tr') ).data();

				$("#rowid").val(data.id);

				$("#sector_tr").val(data.sector_tr);
				$("#sector_en").val(data.sector_en);
    } );

		$('#sme_sectors tbody').on( 'click', '#delete', function () {
		        var data = table.row( $(this).parents('tr') ).data();
						var _token = $("input[name=_token]").val();
						$.ajax({
							url:base_url+"/panel/sektorsil",
							type:"post",
							data:{id:data.id,_token:_token},
							success:function(r){
								toastr[r.status](r.head,r.content);
								table.ajax.reload();
							}
						});

		    } );





		$("#formupdate").ajaxForm({
			url:base_url+"/panel/sektorguncelle",
			type:"POST",
			success:function(r){
				toastr[r.status](r.head,r.content);
				table.ajax.reload();
			}
		});


		$("#form").ajaxForm({
			url:base_url+"/panel/sektorekle",
			type:"POST",
			success:function(r){
				toastr[r.status](r.head,r.content);
				table.ajax.reload();
			}
		});


		// ilgili formu validasyon gerçekleştir.
		    $("#formupdate").validate({
		      rules: {
		        sector_tr: {
		          required: true,
		          minlength:2
		        },
		        sector_en: {
		          required: true,
		          minlength:2
		        }
		      },
		  errorPlacement: function(error, element) {
		      error.insertAfter(element);
		  }
		});

		// ilgili formu validasyon gerçekleştir.
				$("#form").validate({
					rules: {
						sector_tr: {
							required: true,
							minlength:2
						},
						sector_en: {
							required: true,
							minlength:2
						}
					},
			errorPlacement: function(error, element) {
					error.insertAfter(element);
			}
		});






});
