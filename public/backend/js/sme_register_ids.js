$(document).ready(function(){


	// Base url al bulunduğu dizin neyse.
	var base_url = window.location.origin;



// Translate işlemleri başla.
var scripts = document.getElementsByTagName('script');
keys = [];
var bali;
var language;
$(scripts).each(function(index){
bali = scripts[index].src;

var finded = bali.includes("sme_register_ids.js");

if(finded == true){
	var pack =  scripts[index].src;
	var pack = pack.split("=");
	language = pack[1];
}

});
// Translate işlemleri bitiş.

console.log("Language Datatables = "+language);

// Datatables hazırla.
var table = $("#sme_sectors").DataTable({
  responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Detaylar';
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        },
  dom: 'Bfrtpli',
  buttons: [
		   
    ],
  language: {
        url: base_url+"/backend/assets/js/datatableslang/"+language,
        buttons: {
                copyTitle: 'İşlem Başarılı.',
                copyKeys: 'Appuyez sur <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> pour copier les données du tableau à votre presse-papiers. <br><br>Pour annuler, cliquez sur ce message ou appuyez sur Echap.',
                copySuccess: {
                    _: '%d Satır Kopyalandı.',
                    1: '1 Satır Kopyalandı'
                }
            }
  },
  "processing":true,
  "serverSide":true,
  "ajax":base_url+"/panel/kobikayitidlericek",
  "columns":[
    {"data":"id",className: "tableid"},
    {"data":"sme_id"},
    {"data":"sme_name"},
		{"data":"created_at"},
  ],

  "lengthMenu": [ [20, 30, 40, -1], [20, 30, 40, "Hepsi"] ],
  "pageLength": 20,

});




});
