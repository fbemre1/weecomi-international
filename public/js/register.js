  $(document).ready(function(){

    // Toastr Ayarları.
    toastr.options = {
    "debug": false,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "fadeIn": 300,
    "fadeOut": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 99999000,
    "progressBar":true,
  }


    // Base url al bulunduğu dizin neyse.
    var base_url = window.location.origin;

      $( function() {
        $( "#datepicker" ).datepicker();
      } );

    $(".country").change(function(){

      var country_id = $(".country option:selected").val();

      $.ajax({
        url:base_url+"/sehirleracik/"+country_id,
        type:"GET",
        success:function(r){
          $(".city").html(r);
        }
      });


    });




      // ilgili formu post et.
          $("#registerform").ajaxForm({
            type:"POST",
            success:function(r){
              console.log(r);
              toastr[r.status](r.head,r.content);
            }
          });



    $("#registerform").validate({
			rules: {
				reference_code: {
					required: true,
				},
        username: {
					required: true,
				},
        first_name: {
					required: true,
				},
        last_name: {
					required: true,
				},
        identity: {
					required: true,
				},
        email: {
					required: true,
				},
        birthday: {
					required: true,
				},
        country: {
      required: true
        },
        telephone: {
					required: true,
				},
        password: {
					required: true,
				},
        confirm_password: {
					equalTo: "#password",
          required:true,
				},
        agree: {
					required: true,
				},
        address: {
					required: true,
				},
        city: {
					required: true,
				},
        district: {
					required: true,
				},


			}
		});





  });
