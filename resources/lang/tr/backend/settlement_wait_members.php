<?php
// New_register.php Çevirisi.
return [
'settlementwaitmembers' => 'Yerleşim Bekleyen Üyeler',
'membercode' => 'Üye Kodu',
'fullname' => 'Adı Soyadı',
'email' => 'Email',
'telephone' => 'Telefonu',
'package' => 'Paketi',
'registerdate' => 'Kayıt Tarihi',
'actions' => 'İşlemler',
'lang' => 'turkish.json',
];
