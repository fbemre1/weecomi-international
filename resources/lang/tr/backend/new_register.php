<?php
// New_register.php Çevirisi.
return [
'newregister' => 'Yeni Kayıt',
'stepone' => 'Adım 1',
'steptwo' => 'Adım 2',
'username' => 'Kullanıcı adı',
'firstname' => 'Adı',
'lastname' => 'Soyadı',
'identity' => 'Tc Kimlik',
'email' => 'Email',
'password' => 'Şifre',
'againpassword' => 'Tekrar Şifre',
'telephone' => 'Telefon',
'birthday' => 'Doğum Tarihi',
'country' => 'Ülke',
'city' => 'İl',
'firstselectcountry' => 'İlk Önce Ülke Seçiniz',
'district' => 'İlçe',
'address' => 'Adres',
'select' => 'Seç',
'packageselect' => 'Paket Seçimi',
'paymentmethod' => 'Ödeme Tipi',
'next' => 'Bir Sonraki Adım',
'bankselect' => 'Banka Seç',
'save' => 'Kaydet'
];
