<?php
// Backend/Member.blade.php Çevirisi.
return [
'memberinfo' => 'Üye Bilgileri',
'profileimage' => 'Profil Resmi',
'packageinfo' => 'Paket Bilgileri',
'packagetype' => 'Paket Tipi',
'packagetime' => 'Paket Süresi',
'selectimage' => 'Resim Seç',
'username' => 'Kullanıcı adı',
'firstname' => 'Adı',
'lastname' => 'Soyadı',
'identity' => 'Kimliği',
'email' => 'Email',
'password' => 'Şifre',
'againpassword' => 'Tekrar Şifre',
'packagestartdate' => 'Paket Başlangıç Tarihi',
'packageenddate' => 'Paket Bitiş Tarihi',
'telephone' => 'Telefon',
'birthday' => 'Doğum Tarihi',
'country' => 'Ülke',
'city' => 'İl',
'district' => 'İlçe',
'address' => 'Adres',
'firstselectcountry' => 'İlk Önce Ülke Seçin',
'save' => 'Kaydet',

];
