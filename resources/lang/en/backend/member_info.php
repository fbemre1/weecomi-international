<?php
// Backend/Member.blade.php Çevirisi.
return [
'memberinfo' => 'Member İnformation',
'profileimage' => 'Profile İmage',
'packageinfo' => 'Package information',
'packagetype' => 'Package Type',
'packagetime' => 'Package Time',
'selectimage' => 'Select İmage',
'username' => 'Username',
'firstname' => 'Firstname',
'lastname' => 'Lastname',
'identity' => 'İdentity',
'email' => 'Email',
'password' => 'Password',
'againpassword' => 'Again Password',
'packagestartdate' => 'Package Start Date',
'packageenddate' => 'Package End Date',
'telephone' => 'Telephone',
'birthday' => 'Birthday',
'country' => 'Country',
'city' => 'City',
'district' => 'District',
'address' => 'Address',
'firstselectcountry' => 'First Select Country',
'save' => 'Save',

];
