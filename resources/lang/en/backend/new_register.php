<?php
// New_register.php Çevirisi.
return [
'newregister' => 'New Register',
'stepone' => 'Step 1',
'steptwo' => 'Step 2',
'username' => 'Username',
'firstname' => 'Firstname',
'lastname' => 'Lastname',
'identity' => 'İdentity',
'email' => 'Email',
'password' => 'Password',
'againpassword' => 'Again Password',
'telephone' => 'Telephone',
'birthday' => 'Birthday',
'country' => 'Country',
'city' => 'City',
'firstselectcountry' => 'First Select Country',
'district' => 'District',
'address' => 'Address',
'select' => 'Select',
'packageselect' => 'Package Select',
'paymentmethod' => 'Payment Method',
'next' => 'next step',
'bankselect' => 'Bank Select',
'save' => 'save'
];
