<?php
// New_register.php Çevirisi.
return [
'smesregister' => 'Smes Define',
'sponsor_code' => 'Sponsor Code',
'sector' => 'Sector',
'storename' => 'Store Name',
'title' => 'Title',
'taxadortaxnum' => 'Tax Administration / Tax Number',
'username' => 'Username',
'email' => 'Email',
'telephone' => 'Telephone',
'discountrate' => 'Discount Rate',
'maps' => 'Google Maps',
'workinghours' => 'Working Hours',
'country' => 'Country',
'city' => 'City',
'district' => 'District',
'address' => 'Address',
'companylogo' => 'Company Logo',
'select' => 'Select',
'startdate' => 'Start Time',
'enddate' => 'Finish Time',
'fileselect' => 'İmage Select',
'save' => 'Save',
'password' => 'Password',
'sectorsettings' => 'Sector Settings',
];
