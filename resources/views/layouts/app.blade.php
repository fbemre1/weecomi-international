<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Weecomi</title>

    <base href="{{ URL::to("") }}">
    <link rel="shortcut icon" type="image/png" href="favicon.png">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karla:400%7CPoppins:400,400i,500,700">

    <link rel="stylesheet" href="/css/fontawesome-all.min.css">

    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <link rel="stylesheet" href="/plugins/swiper/swiper.min.css">

    <link rel="stylesheet" href="/plugins/magnific-popup/magnific-popup.min.css">

    <link rel="stylesheet" href="/css/animate.min.css">

    <link rel="stylesheet" href="/css/style.css">

    <link rel="stylesheet" href="/css/responsive.css">

    <link rel="stylesheet" href="/css/custom.css">
    @yield("css")
</head>

<body>

<div class="preLoader">

<span class="spin">
</span>
</div>

<header class="main-header bg-primary">

    <div class="container">

        <div class="row">

            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-5 col-9">

                <div class="logo" data-animate="fadeInDown" data-delay=".5"><a href="index.html"> <img data-rjs="3"
                                                                                                       src="img/weecomi_white.png"
                                                                                                       alt="Weecomi">
                        <img data-rjs="3" src="img/weecomi.png" alt="Weecomi"> </a>
                </div>
            </div>

            <div class="col-xl-7 col-lg-8 col-md-6 col-sm-3 col-3">
                <nav data-animate="fadeInDown" data-delay=".75">

                    <div class='header-menu'>
                        <ul>
                            <li><a href='{{ route('home') }}'>Anasayfa</a>
                            </li>

                            <li><a href='#'>Kurumsal <i class="fas fa-caret-down"></i></a>
                                <ul>
                                    <li><a href="{{ route('hakkimizda') }}">Hakkımızda</a></li>
                                    <li><a href="{{ route('kurucumuz') }}">Kurucumuz</a></li>
                                    <li><a href="{{ route('medyadabiz') }}">Medyada Biz</a></li>
                                    <li><a href="{{ route('belgelerimiz') }}">Belgelerimiz</a></li>
                                </ul>
                            </li>

                            <li><a href='#'>Dünyada Biz <i class="fas fa-caret-down"></i></a>
                                <ul>
                                    <li><a href="{{ route('kobilerimiz') }}">Kobilerimiz</a></li>
                                    <li><a href="{{ route('bayilerimiz') }}">Bayilerimiz</a></li>
                                </ul>
                            </li>

                            <li><a href="{{ route('nedir') }}">Weecomi Nedir ?</a>
                            </li>

                            <li><a href="{{ route('iletisim') }}">İletişim</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>


            @if(!Sentinel::forceCheck())

                <div class="register-button" data-animate="fadeInDown" data-delay="1"><a
                            class="btn btn-transparent btn-block" href="#" data-toggle="modal" data-target="#myModal">Back
                        Office</a>
                </div>

                <div class="register-button" data-animate="fadeInDown" data-delay="1"><a
                            class="btn btn-transparent btn-block" href="{{ route('register') }}">Üye Ol</a>
                </div>

            @else

                <div class="register-button" data-animate="fadeInDown" data-delay="1"><a
                            class="btn btn-transparent btn-block"
                            href="{{ URL::to('panel') }}">{{ ucfirst(Sentinel::getUser()->first_name)  }} {{ Sentinel::getUser()->last_name  }}</a>
                </div>



                <div class="register-button" data-animate="fadeInDown" data-delay="1"><a
                            class="btn btn-transparent btn-block" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">Çıkış
                        Yap</a>
                </div>

                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            @endguest


        </div>
    </div>
</header>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                Kullanıcı Paneli
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form action="{{ route('login') }}" method="POST" id="loginform">
                    @csrf
                    <div class="form-group">
                        <label>Kullanıcı adı</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    <div class="form-group">
                        <label>Şifre</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>


            </div>

            <div class="modal-footer">

                <button type="submit" class="btn btn-primary">Giriş yap</button>
                <a href="#" class="text-left">Şifremi Unuttum ?</a>
            </div>
            </form>
        </div>

    </div>
</div>

@yield("content")


<footer class="main-footer text-white bg-rotate position-relative">

    <div class="top-footer">

        <div class="container">

            <div class="row">

                <div class="col-lg-4 col-sm-6">

                    <div class="footer-widget"><h3 class="widget-title" data-animate="fadeInUp" data-delay=".0">Weecomi
                            Haberler</h3>

                        <div class="footer-posts">
                            <ul class="list-unstyled">
                                <li data-animate="fadeInUp" data-delay=".05">

                                    <p>Posted on <a href="#">Jan 19, 2017</a></p><h4><a href="#">In Major Hiring Push,
                                            Web Hosting Powerhouse Go Daddy to Expand Its Services</a></h4></li>
                                <li data-animate="fadeInUp" data-delay=".1">

                                    <p>Posted on <a href="#">Jan 19, 2017</a></p><h4><a href="#">In Major Hiring Push,
                                            Web Hosting Powerhouse Go Daddy to Expand Its Services</a></h4></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6">

                    <div class="footer-widget"><h3 class="widget-title" data-animate="fadeInUp" data-delay=".15">
                            İletişim Adreslerimiz</h3>

                        <div class="footer-contacts">
                            <ul class="list-unstyled">
                                <li data-animate="fadeInUp" data-delay=".2">

<span>Telefon<i>:</i>
</span> <a href="tel:+01234567898">(+01) 2345-67898</a>, <a href="tel:+9876543210">(+98) 7654-3210</a></li>
                                <li data-animate="fadeInUp" data-delay=".25">

<span>E-mail<i>:</i>
</span> <a href="mailto:yourMail@example.com">yourMail@example.com</a></li>
                                <li data-animate="fadeInUp" data-delay=".3">

<span>Adres<i>:</i>
</span>
                                    <address>3520 Cameron Road, Niagara Falls, NY 14301.</address>
                                </li>
                                <li data-animate="fadeInUp" data-delay=".35">

<span>Sosyal<i>:</i>
</span> <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a> <a href="#" target="_blank"><i
                                                class="fab fa-twitter"></i></a> <a href="#" target="_blank"><i
                                                class="fab fa-google-plus-g"></i></a> <a href="#" target="_blank"><i
                                                class="fab fa-linkedin-in"></i></a> <a href="#" target="_blank"><i
                                                class="fas fa-rss"></i></a> <a href="#" target="_blank"><i
                                                class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12">

                    <div class="footer-widget"><h3 class="widget-title" data-animate="fadeInUp" data-delay=".4">İletişim
                            Formu</h3>

                        <div class="footer-form">

                            <form action="http://themelooks.net/demo/bnscloud/html/preview/sendmail.php" method="post"
                                  class="karla position-relative">

                                <div class="form-row">

                                    <div class="col">
                                        <input class="form-control" name="contactName" type="text"
                                               placeholder="Adınız Soyadınız" required data-animate="fadeInUp"
                                               data-delay=".45">
                                    </div>

                                    <div class="col">
                                        <input class="form-control" type="email" name="contactEmail"
                                               placeholder="Email Adresiniz" required data-animate="fadeInUp"
                                               data-delay=".5">
                                    </div>
                                </div>
                                <input class="form-control" type="text" name="contactSubject" placeholder="Konu"
                                       required data-animate="fadeInUp" data-delay=".55">
                                <textarea class="form-control w-100" name="contactMessage" rows="4" placeholder="Mesaj"
                                          required data-animate="fadeInUp" data-delay=".6">
</textarea>
                                <input type="submit" class="btn btn-transparent poppins" value="Gönder"
                                       data-animate="fadeInUp" data-delay=".65">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-footer position-relative">

        <div class="container">

            <div class="row align-items-center">

                <div class="col-md-5 order-last order-md-first">

<span class="copyright-text" data-animate="fadeInDown" data-delay=".7">Copyright &copy; 2018 <a href="#">Weecomi</a>. Tüm Hakları Saklıdır.
</span>
                </div>

                <div class="col-md-7 order-first order-md-last">
                    <ul class="footer-menu text-md-right list-inline">
                        <li data-animate="fadeInDown" data-delay=".75"><a href="#">Hakkımızda</a></li>
                        <li data-animate="fadeInDown" data-delay=".8"><a href="#">İletişim</a></li>
                        <li data-animate="fadeInDown" data-delay=".85"><a href="#">Gizlilik Politikası</a></li>
                        <li data-animate="fadeInDown" data-delay=".9"><a href="#">Şartlar ve Koşullar</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <a class="back-to-top" href="#" data-animate="fadeInDown" data-delay=".95"> <i class="fas fa-arrow-up"></i> </a>
    </div>


</footer>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/plugins/swiper/swiper.min.js"></script>
<script src="/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/plugins/parsley/parsley.min.js"></script>
<script src="/plugins/retinajs/retina.min.js"></script>
<script src="/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/plugins/waypoints/sticky.min.js"></script>
<script src="/js/menu.js"></script>
<script src="/js/scripts.js"></script>
<script src="/validation/jquery.validate.min.js"></script>
<script src="/validation/methods.js"></script>
<script src="/validation/localization/messages_tr.js"></script>
<script src="/js/index.js"></script>


@yield("jss")
</body>

</html>
