<!-- <li>
    <a href="#">
        <div class="avatar float-left">
            <img src="{{ URL::to('') }}/backend/assets/img/dummy/u4.png" alt="">
            <span class="avatar-badge busy"></span>
        </div>
        <h4>
            Support Team
            <small><i class="icon icon-clock-o"></i> 5 mins</small>
        </h4>
        <p>Why not buy a new awesome theme?</p>
    </a>
</li> -->

@if($notifications->isEmpty())
<li>
<a href="#">  hiç mesajınız yok :(</a>
</li>
@else
@foreach($notifications as $write)
<li>
    <a href="/panel/{{ $write->parameter }}">
            <small><i class="icon icon-clock-o"></i> {{ $write->created_at->diffForHumans() }}</small>

        <span style="display:block;">{{ trans('backend/notifications.nnewmessage') }}</span>
    </a>
</li>
@endforeach
@endif
