<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ URL::to('') }}/backend/assets/img/basic/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ URL::to('') }}/backend/assets/css/jquery-ui.css">
    @yield('css')
    <title>Weecomi Back Office</title>
    <link rel="stylesheet" href="{{ URL::to('') }}/backend/assets/css/app.css">
</head>
<body class="light">

<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">

  @include('backend.layouts.sidebar')

  @yield('content')

  <div class="control-sidebar-bg shadow white fixed"></div>
  </div>
  <script src="{{ URL::to('') }}/backend/assets/js/app.js"></script>
  <script src="{{ URL::to('') }}/validation/jquery.validate.min.js"></script>
  <script src="{{ URL::to('') }}/validation/localization/messages_tr.min.js"></script>
  <script src="{{ URL::to('') }}/backend/assets/js/jquery.form.min.js"></script>
  <script src="{{ URL::to('') }}/backend/assets/js/toastr.min.js"></script>
  <script src="{{ URL::to('') }}/backend/js/home.js"></script>

  @yield('js')



  </body>

  </html>
