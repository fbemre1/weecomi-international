
<aside class="main-sidebar fixed offcanvas shadow" data-toggle='offcanvas'>
    <section class="sidebar">
        <div class="w-150px mt-3 mb-3 ml-3">
            <img src="https://weecomi.com/images/new-alt-menu-logo.png" alt="">
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
               aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm absolute fab-right-bottom fab-top btn-primary shadow1 ">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src="/user_avatar/default.png" alt="{{ Sentinel::getUser()->first_name }}">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">{{ ucfirst(Sentinel::getUser()->first_name) }} {{ Sentinel::getUser()->last_name }}</h6>
                        <a href="#"><i class="icon-circle text-primary blink"></i> Online</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        <a href="{{ URL::to('') }}/{{ Request::segment(1) }}/uyelikbilgilerim" class="list-group-item list-group-item-action ">
                            <i class="mr-2 icon-user text-blue"></i>{{ trans('Backend/sidebar.memberinformation') }}
                        </a>
                        <a href="{{ URL::to('') }}/{{ Request::segment(1) }}/sifredegistir" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-cogs text-yellow"></i>{{ trans('Backend/sidebar.changepassword') }}</a>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-exit_to_app text-purple"></i>{{ trans('Backend/sidebar.logout') }}</a>
                                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header light mt-3"><strong>Ana Menü</strong></li>
            <li class="treeview active"><a href="{{ URL::to('panel') }}">
                <i class="icon icon-sailing-boat-water purple-text s-18"></i> <span>{{ trans('Backend/sidebar.dashboard') }}</span>
            </a>

            </li>
            <li class="treeview"><a href="#">
                <i class="icon icon icon-package blue-text s-18"></i>
                <span>{{ trans('Backend/sidebar.teammanagement') }}</span>
<i class="icon icon-angle-left s-18 pull-right"></i>
            </a>
                <ul class="treeview-menu">
                    <li><a href="{{ URL::to('panel/yerlesimbekleyenuyeler') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.settlementwaitmembers') }} <span class="badge r-3 badge-primary pull-right">4</span></a></li>
                    <li><a href="{{ URL::to('panel/onaybekleyenuyeler') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.pendingmembers') }} <span class="badge r-3 badge-primary pull-right">7</span></a></li>
                    <li><a href="{{ URL::to('panel/yerlesimagacim') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.settlementree') }} <span class="badge r-3 badge-primary pull-right">14</span></a></li>
                    <li><a href="{{ URL::to('panel/ekibim') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.team') }} <span class="badge r-3 badge-primary pull-right">14</span></a></li>
                    <li><a href="{{ URL::to('panel/yenikayit') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.newregister') }} <span class="badge r-3 badge-primary pull-right">14</span></a></li>


                </ul>
            </li>
            <li class="treeview"><a href="#"><i class="icon icon-account_box light-amber-text s-18"></i> <span>{{ trans('Backend/sidebar.smemanagement') }}</span><i
                    class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">

                    <li><a href="{{ URL::to('panel/kobikayit') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.smedefine') }} </a></li>
                    <li><a href="{{ URL::to('panel/kobikayitidleri') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.smeregisterids') }} <span class="badge r-3 badge-primary pull-right">14</span></a></li>
                </ul>
            </li>

            <li class="treeview"><a href="#"><i class="icon icon-inbox2 pink-text s-18"></i> <span>{{ trans('Backend/sidebar.messages') }}</span><i
                    class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ URL::to('panel/bolgeolustur') }}"><i class="icon icon-circle-o"></i>Bölge Oluştur</a></li>
                    <li><a href="{{ URL::to('panel/gelenkutusu') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.inbox') }}</a></li>
                    <li><a href="{{ URL::to('panel/gonderilmis') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.sentmessages') }}</a></li>
                    <li><a href="{{ URL::to('panel/mesajolustur') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.newmessagecreate') }} </a></li>

                </ul>
            </li>








            <li class="header light mt-3"><strong>{{ trans('Backend/sidebar.sitemanagement') }}</strong></li>

            <li class="treeview"><a href="#"><i class="icon icon-settings text-lime s-18"></i> <span>{{ trans('Backend/sidebar.generalsettings') }}</span> <i
                    class="icon icon-angle-left s-18 pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ URL::to('panel/seoayarlari') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.seosettings') }} </a></li>
                  <li><a href="{{ URL::to('panel/sektorayarlari') }}"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.sectorsettings') }} </a></li>


              </ul>
            </li>


            <li><a href="{{ URL::to('panel/paketayarlari') }}"><i class="icon icon-settings text-lime s-18"></i> <span>{{ trans('Backend/sidebar.packagesettings') }}</span></a></li>
            <li><a href="#"><i class="icon icon-settings text-lime s-18"></i> <span>{{ trans('Backend/sidebar.banksettings') }}</span></a></li>
            <li><a href="#"><i class="icon icon-envelope text-lime s-18"></i> <span>{{ trans('Backend/sidebar.mailsettings') }}</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon-pages text-lime s-18"></i> <span>{{ trans('Backend/sidebar.subpages') }}</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.pages') }}</a>
                    </li>
                    <li><a href="#"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.addpage') }}</a>
                    </li>


                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon-newspaper-o text-lime s-18"></i> <span>Blog</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.posts') }}</a>
                    </li>
                    <li><a href="#"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.addpost') }}</a>
                    </li>


                </ul>
            </li>


            <li class="header light mt-3"><strong>{{ trans('Backend/sidebar.documents') }}</strong></li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon-package text-lime s-18"></i> <span>{{ trans('Backend/sidebar.documents') }}</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.sme') }}</a>
                    </li>
                    <li><a href="#"><i class="icon icon-date_range"></i>{{ trans('Backend/sidebar.customer') }}</a>
                    </li>
                    <li><a href="#"><i class="icon icon-date_range"></i>{{ trans('Backend/sidebar.dealer') }}</a>
                    </li>

                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="icon icon-documents3 text-lime s-18"></i> <span>{{ trans('Backend/sidebar.presentations') }}</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="icon icon-circle-o"></i>{{ trans('Backend/sidebar.sme') }}</a>
                    </li>
                    <li><a href="#"><i class="icon icon-date_range"></i>{{ trans('Backend/sidebar.customer') }}</a>
                    </li>
                    <li><a href="#"><i class="icon icon-date_range"></i>{{ trans('Backend/sidebar.dealer') }}</a>
                    </li>

                </ul>
            </li>





        </ul>
    </section>
</aside>
<!--Sidebar End-->
<div class="has-sidebar-left">
    <div class="pos-f-t">
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
            <div class="search-bar">
                <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                       placeholder="start typing...">
            </div>
            <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
               aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
        </div>
    </div>
</div>
    <div class="sticky">
        <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
            <div class="relative">
                <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                    <i></i>
                </a>
            </div>
            <!--Top Menu Start -->
<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <!-- Messages-->
        <li class="dropdown custom-dropdown messages-menu msgajax">
            <a href="#" class="nav-link" data-toggle="dropdown">
                   <i class="icon-message"></i>
                   <span class="badge badge-success badge-mini rounded-circle">{{ count($notifications) }}</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu pl-2 pr-2">



                        @include("backend.include.notifications")




                    </ul>
                </li>
                <li class="footer s-12 p-2 text-center"><a href="#">Tüm Mesajlar</a></li>
            </ul>
        </li>
        <!-- Notifications -->
        <li class="dropdown custom-dropdown notifications-menu">
            <a href="#" class=" nav-link" data-toggle="dropdown" aria-expanded="false">
                <i class="icon-notifications "></i>
                <span class="badge badge-danger badge-mini rounded-circle">4</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li class="header">You have 10 notifications</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        <li>
                            <a href="#">
                                <i class="icon icon-data_usage text-success"></i> 5 new members joined today
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon icon-data_usage text-danger"></i> 5 new members joined today
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon icon-data_usage text-yellow"></i> 5 new members joined today
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="footer p-2 text-center"><a href="#">Hepsi</a></li>
            </ul>
        </li>


        <li class="dropdown custom-dropdown notifications-menu languagepadding">
          <?php $locale = Session::get("locale"); ?>
            <select class="form-control" onchange="if (this.value) window.location.href=this.value">
              <option selected disabled>{{ trans("Backend/smes.select") }}</option>
              <option value="/lang/tr" <?php echo @$locale == "tr" ? "selected":""; ?>>Turkish</option>
              <option value="/lang/en" <?php echo @$locale == "en" ? "selected":""; ?>>English</option>
            </select>
        </li>



    </ul>
</div>
        </div>
    </div>
</div>

<script>
function test(a) {
    var x = (a.value || a.options[a.selectedIndex].value);  //crossbrowser solution =)
    alert(x);
}
</script>
