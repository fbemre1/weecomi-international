@extends("backend.layouts.app")

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Onay Bekleyen Üyeler
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Onay Bekleyen Üyeler</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">

        <div class="tab-content my-3" id="v-pills-tabContent">

@csrf


            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">

                  <div class="col-md-12">
                      <div class="card no-b">
                            <div class="card-body">

                          <table id="pendingmembers" class="table table-striped table-bordered nowrap" style="width:100%;">
                              <thead>
                              <tr>
                                  <th>id</th>
                                  <th>Üye Kodu</th>
                                  <th>Adı Soyadı</th>
                                  <th>Email</th>
                                  <th>Telefon</th>
                                  <th>Paket</th>
                                  <th>Kayıt Tarihi</th>
                                  <th>İşlemler</th>
                              </tr>
                              </thead>
                              <tbody>

                              </tfoot>
                          </table>
                      </div>
                          </div>
                  </div>

                </div>


            </div>







        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="editor">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Düzenleme</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="form">
              @csrf
              <input type="hidden" name="id" id="rowid">
              <div class="form-group">
                <label>Adı</label>
                <input type="text" class="form-control" id="firstname" placeholder="Adı Soyadı" name="firstname">
              </div>
              <div class="form-group">
                <label>Soyadı</label>
                <input type="text" class="form-control" id="lastname"  placeholder="Soyadı" name="lastname">
              </div>
              <div class="form-group">
                <label>Email Adresi</label>
                <input type="text" class="form-control" id="email"  placeholder="Email Adresi" name="email">
              </div>
              <div class="form-group">
                <label>Telefon</label>
                <input type="text" class="form-control" id="telephone" placeholder="Telefon" name="telephone">
              </div>
              <div class="form-group">
                <label>Paket Adı</label>
                <select class="form-control" id="package_id" name="package_id">

@foreach($packages as $package)
<option value="{{ $package->id }}">{{ $package->name  }}</option>
@endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Banka Adı</label>
                <select class="form-control" id="bank_id" name="bank_id">
                  @foreach($banks as $bank)
                  <option value="{{ $bank->id }}">{{ $bank->bank_name  }}</option>
                  @endforeach
                </select>
              </div>

              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>

</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/pending_members.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.flash.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/jszip.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/vfs_fonts.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.html5.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.print.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.responsive.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/responsive.bootstrap.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.colVis.min.js"></script>

@stop
