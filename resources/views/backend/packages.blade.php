@extends("backend.layouts.app")

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Paket Ayarları
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Paket Ayarları</a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-2"
                           role="tab" aria-controls="v-pills-all"><i class="icon"></i>Paket Özellikleri</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">

        <div class="tab-content my-3" id="v-pills-tabContent">

@csrf


            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">

                  <div class="col-md-12">
                      <div class="card no-b">
                            <div class="card-body">

                          <table id="packages" class="table table-striped table-bordered nowrap" style="width:100%;">
                              <thead>
                              <tr>
                                  <th>id</th>
                                  <th>Paket adı</th>
                                  <th>Paket Fiyatı</th>
                                  <th>Paket Kdv</th>
                                  <th>Paket Çarpanı</th>
                                  <th>Paket Limiti</th>
                                  <th>Kayıt Tarihi</th>
                                  <th>İşlemler</th>
                              </tr>
                              </thead>
                              <tbody>

                              </tfoot>
                          </table>
                      </div>
                          </div>
                  </div>

                </div>


            </div>




            <div class="tab-pane animated fadeInUpShort show" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">

                  <div class="col-md-12">
                      <div class="card no-b">
                            <div class="card-body">

                              <table id="packagesdetails" class="table table-striped table-bordered nowrap" style="width:100%;">
                                  <thead>
                                  <tr>
                                      <th>id</th>
                                      <th>Paket adı</th>
                                      <th>Paket Özelliği</th>
                                      <th>Kayıt Tarihi</th>
                                      <th>İşlemler</th>
                                  </tr>
                                  </thead>
                                  <tbody>

                                  </tfoot>
                              </table>
                      </div>
                          </div>
                  </div>

                </div>


            </div>




        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="editor">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Düzenleme</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="form1">
              @csrf
              <input type="hidden" name="id" id="package_id">
              <div class="form-group">
                <label>Paket Adı</label>
                <input type="text" class="form-control" id="package_name" placeholder="Paket Adı" name="package_name">
              </div>
              <div class="form-group">
                <label>Paket Fiyatı</label>
                <input type="number" class="form-control" id="package_price"  placeholder="Paket Fiyatı" name="package_price">
              </div>
              <div class="form-group">
                <label>Paket Kdv</label>
                <input type="number" class="form-control" id="package_kdv"  placeholder="Paket Kdv" name="package_kdv">
              </div>
              <div class="form-group">
                <label>Paket Çarpanı</label>
                <input type="number" class="form-control" id="package_factor" placeholder="Paket Çarpanı" name="package_factor">
              </div>
              <div class="form-group">
                <label>Paket Limiti</label>
                <input type="number" class="form-control" id="package_limit" placeholder="Paket Limiti" name="package_limit">
              </div>
              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="createpackage">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Yeni Paket Ekle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="form2">
              @csrf

              <div class="form-group">
                <label>Paket Adı</label>
                <input type="text" class="form-control" id="package_name" placeholder="Paket Adı" name="package_name">
              </div>
              <div class="form-group">
                <label>Paket Fiyatı</label>
                <input type="number" class="form-control" id="package_price"  placeholder="Paket Fiyatı" name="package_price">
              </div>
              <div class="form-group">
                <label>Paket Kdv</label>
                <input type="number" class="form-control" id="package_kdv"  placeholder="Paket Kdv" name="package_kdv">
              </div>
              <div class="form-group">
                <label>Paket Çarpanı</label>
                <input type="number" class="form-control" id="package_factor" placeholder="Paket Çarpanı" name="package_factor">
              </div>
              <div class="form-group">
                <label>Paket Limiti</label>
                <input type="number" class="form-control" id="package_limit" placeholder="Paket Limiti" name="package_limit">
              </div>
              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="createdetail">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Yeni Özellik Ekle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="form3">
              @csrf

              <div class="form-group">
                <label>Paket Seç</label>
                <select class="form-control" name="package_id">
                  @foreach($packages as $package)
                  <option value="{{ $package->id }}">{{ $package->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Paket Özelliği</label>
                <input type="text" class="form-control" id="package_text"  placeholder="Paket Özelliği" name="package_text">
              </div>
              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="detailupdate">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Yeni Özellik Düzenle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="form4">
              @csrf
        <input type="hidden" name="id" id="detailerid">
              <div class="form-group">
                <label>Paket Seç</label>
                <select class="form-control" name="package_id" id="selectorpackage">
                  @foreach($packages as $package)
                  <option value="{{ $package->id }}">{{ $package->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Paket Özelliği</label>
                <input type="text" class="form-control" id="package_textr"  placeholder="Paket Özelliği" name="package_text">
              </div>
              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>

</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/packages.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.flash.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/jszip.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/vfs_fonts.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.html5.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.print.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.responsive.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/responsive.bootstrap.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.colVis.min.js"></script>

@stop
