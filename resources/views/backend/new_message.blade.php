
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">

@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Yeni Mesaj Oluştur
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Yeni Mesaj Oluştur</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">

<div class="card">

        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">

@csrf


<div class="col-md-6">
  <form action="" method="POST" id="form">
    @csrf

  @if(Sentinel::getUser()->user_type == 1)
    <div class="form-group">
      <label>Adı Soyadı</label>
      <input type="text" id="autocek" class="form-control" name="firstname" placeholder="ad soyad,kullanıcı adı,tel,tc,email"/>
      <input type="hidden" value="" class="userid" name="user_id">
    </div>



    <div class="form-group">
      <label>Ülke</label>
      <select class="form-control countries" name="country">
      <option value="" selected>Seçiniz</option>
      @foreach($countries as $country)
      <option value="{{ $country->id }}">{{ $country->name }}</option>
      @endforeach
      </select>
    </div>

    <div class="form-group">
      <label>Şehir</label>
      <select class="form-control cities" name="city">
      <option selected >Seçiniz</option>
      </select>
    </div>


    @endif

    <div class="form-group">
      <label>Mesaj Konu</label>
      <select class="form-control" name="category">
      <option selected value="">Seçiniz</option>
      <option>Ödeme Sorunları</option>
      <option>Muhasebe</option>
      <option>Diğer</option>
      </select>
    </div>

    <div class="form-group">
      <label>Mesaj Başlığı</label>
      <input type="text" name="subject" class="form-control">
    </div>

    <div class="form-group">
      <label>Mesaj İçeriği</label>
      <textarea type="textarea" class="form-control" placeholder="Mesaj İçeriği" name="message_content"></textarea>
    </div>


    <button type="submit" class="btn btn-primary">Kaydet</button>
  </form>
</div>










                </div>





                </div>


            </div>




        </div>
        </div>
    </div>


</div>
@stop

@section('js')


<script src="{{ URL::to('') }}/backend/js/new_message.js"></script>
<script src="{{ URL::to('') }}/js/jquery-ui.js"></script>


  @stop
