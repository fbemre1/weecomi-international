
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">

@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ trans('Backend/member_info.memberinfo') }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>{{ trans('Backend/member_info.memberinfo') }}</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">

<div class="card">

        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">
                  <form class="form-horizontal tpater" action="" method="POST" id="formpost">
@csrf

<div class="row">

<div class="col-md-6">
  <div class="form-group">
      <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/member_info.profileimage') }}</label>

      <div class="col-sm-12">
        <div class="row">
        <div class="col-md-2 preview">
        <img src="{{ URL::to('') }}/user_avatar/{{ $user->avatar }}" style="border:1px solid #e1e1e1">
      </div>

      <div class="col-md-6" style="border:1px solid #e1e1e1">
        <p>{{ trans('Backend/member_info.packageinfo') }}</p>
        <p>{{ trans('Backend/member_info.packagetype') }} : Bronz</p>
        <p>{{ trans('Backend/member_info.packagetime') }} 112 </p>
        <button class="btn btn-primary" id="fileselector" type="button" style="margin-bottom:20px;"><i class="icon-cloud_upload" ></i> {{ trans('Backend/member_info.selectimage') }}</button>
        <input type="file" class="file" name="file" style="display:none;">
        <div class="result"></div>
      </div>
      </div>
      </div>
  </div>
  </div>



</div>

<div class="row">
                    <div class="col-md-6">



                      <div class="form-group">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/member_info.username') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="username"  placeholder="{{ trans('Backend/member_info.username') }}" type="text" value="{{ $user->username }}" disabled>
                          </div>
                      </div>


                      <div class="form-group">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/member_info.firstname') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="firstname"  placeholder="{{ trans('Backend/member_info.firstname') }}" type="text" value="{{ $user->first_name }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="inputEmail" class="col-sm-4 control-label">{{ trans('Backend/member_info.lastname') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="lastname"  placeholder="{{ trans('Backend/member_info.lastname') }}" type="text" value="{{ $user->last_name }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-4 control-label">{{ trans('Backend/member_info.identity') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="identity"  placeholder="{{ trans('Backend/member_info.identity') }}" type="text" value="{{ $user->identity }}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/member_info.email') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="email"  placeholder="{{ trans('Backend/member_info.email') }}" type="email" value="{{ $user->email }}">
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">{{ trans('Backend/member_info.telephone') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="telephone"  placeholder="{{ trans('Backend/member_info.telephone') }}" type="tel" value="{{ $user->telephone }}">
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/member_info.birthday') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="birthday" autocomplete="birthday" id="datepicker" placeholder="{{ trans('Backend/member_info.birthday') }}" type="text" value="{{ $user->birthday }}">
                          </div>
                      </div>

                      </div>

                      <div class="col-md-6">

                        <div class="form-group">
                            <label  class="col-sm-4 control-label">{{ trans('Backend/member_info.packagestartdate') }}</label>

                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="packagestart" value="04.07.2017" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-4 control-label">{{ trans('Backend/member_info.packageenddate') }}</label>

                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="packagefinish" value="04.07.2018" disabled>
                            </div>
                        </div>




                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/member_info.country') }}</label>

                            <div class="col-sm-12">
                                <select class="form-control countries" name="country" autocomplete="country"  type="text">
                                <option selected disabled>Seçiniz</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" <?php echo $country->id == $user->country ? "selected":""; ?>>{{ $country->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/member_info.city') }}</label>

                            <div class="col-sm-12">
                                <select class="form-control cities" autocomplete="city" name="city" type="text">
                                <option>{{ trans('Backend/member_info.firstselectcountry') }}</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/member_info.district') }}</label>

                            <div class="col-sm-12">
                              <input type="text" name="district" class="form-control" autocomplete="district" placeholder="{{ trans('Backend/member_info.district') }}" value="{{ $user->district }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/member_info.address') }}</label>

                            <div class="col-sm-12">
                              <textarea class="form-control" name="address" placeholder="{{ trans('Backend/member_info.address') }}">{{ $user->address }}</textarea>
                            </div>
                        </div>




                        </div>
                        </div>







                </div>



<div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">{{ trans('Backend/member_info.save') }}</button>
                                    </div>
                                </div>
                                </div>

                                </form>


                </div>


            </div>




        </div>
        </div>
    </div>


</div>
@stop

@section('js')
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>

<script src="{{ URL::to('') }}/backend/assets/js/datepicker-tr.js"></script>
<script src="{{ URL::to('') }}/backend/js/member_info.js"></script>


  @stop
