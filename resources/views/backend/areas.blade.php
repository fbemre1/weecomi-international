@extends("backend.layouts.app")

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Bölge Oluştur ({{ $defaultarea->name }})
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Bölge Oluştur</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">

        <div class="tab-content my-3" id="v-pills-tabContent">

@csrf


            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">

                  <div class="col-md-12">
                      <div class="card no-b">
                            <div class="card-body">

                          <table id="pendingmembers" class="table table-striped table-bordered nowrap" style="width:100%;">
                              <thead>
                              <tr>
                                  <th>id</th>
                                  <th>Bölge Adı</th>
                                  <th>Kayıt Tarihi</th>
                                  <th>İşlemler</th>
                              </tr>
                              </thead>
                              <tbody>

                              </tfoot>
                          </table>
                      </div>
                          </div>
                  </div>

                </div>


            </div>







        </div>
    </div>





    <div class="modal" tabindex="-1" role="dialog" id="addarea">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Yeni Bölge Ekle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="areaform">
              @csrf
              <div class="form-group">
                <label>Bölge adı</label>
                <input type="text" class="form-control" placeholder="Yeni bölge adınız" name="area_name">
              </div>

              <div class="form-group">
                <label>Ülke Seç</label>
                <select class="form-control" id="package_id" name="country_id">

<option selected disabled value="">Seçiniz</option>
@foreach($countries as $country)
<option value="{{ $country->id }}">{{ $country->name }}</option>
@endforeach

                </select>
              </div>




              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="areaedit">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Bölge Düzenle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="editarform">
              <input type="hidden" name="id" id="area_id" value="">
              @csrf
              <div class="form-group">
                <label>Bölge adı</label>
                <input type="text" class="form-control" id="areaname" placeholder="Yeni bölge adınız" name="area_name">
              </div>

              <div class="form-group">
                <label>Ülke Seç</label>
                <select class="form-control" id="country" name="country_id">

<option selected disabled value="">Seçiniz</option>
@foreach($countries as $country)
<option value="{{ $country->id }}">{{ $country->name }}</option>
@endforeach

                </select>
              </div>




              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="cityadd">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Şehir Ekle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="city_add">
              <input type="hidden" name="area_id" class="city_area_id">
              @csrf
              <div class="form-group">
                <label>Şehir Seç</label>
                <select class="select2" id="city" name="city[]" multiple>



                </select>
              </div>




              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="citydelete">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Şehir Düzenle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="city_add">
              <input type="hidden" name="area_id" class="city_area_id">
              @csrf
              <div class="form-group">
                <table class="table tablm">
    <thead>
      <tr>
        <th>id</th>
        <th>şehir adı</th>
        <th>işlem</th>
      </tr>
    </thead>
    <tbody class="cityeditor">


    </tbody>
  </table>
              </div>




              <button type="button" class="btn btn-primary" data-dismiss="modal">Kapat</button>
            </form>
          </div>

        </div>
      </div>
    </div>


</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/areas.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.flash.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/jszip.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/vfs_fonts.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.html5.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.print.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.responsive.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/responsive.bootstrap.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.colVis.min.js"></script>
@stop
