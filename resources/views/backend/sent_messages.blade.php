@extends('backend.layouts.app')
@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection



@section('content')


<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Gönderilmiş Mesajlar
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Gönderilmiş Mesajlar</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">

@if(count($messagebox) > 0)
<div class="card">

        <div class="tab-content" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">




                         <ul class="list-unstyled bordermight">



@foreach($messagebox as $write)

@if($write["status"] == 0)
<?php $stat = "seconday"; ?>
<?php $valtext = "Kapalı"; ?>
@else
<?php $stat = "success"; ?>
<?php $valtext = "Açık"; ?>
@endif

                             <li class="my-1">
                               <a href="{{ URL::to('panel/mesajdetay') }}/{{ $write["id"] }}">
                                 <div class="card no-b p-3">
                                     <div class="">
                                         <div class="float-right image mr-3">

                                          <button type="button" class="btn btn-{{$stat}} btn-sm">{{ $valtext }}</button>
                                         </div>
                                         <div class="image mr-3  float-left">
                                             <img class="w-40px" src="/user_avatar/default.png" alt="User Image">
                                         </div>
                                         <div>
                                             <div>
                                               @if($write["receiver_id"] == null)
                                                 <strong>Weecomi</strong>
                                                 @else
                                                 <strong>{{ $write["receiver_name"] }}</strong>
                                                 @endif
                                             </div>
                                             <small> {{ $write["subject"] }}</small>
                                         </div>
                                     </div>
                                 </div>
                               </a>
                             </li>
@endforeach


                         </ul>



                     <div class="card-footer white" style="margin-bottom:10px;">
                         {{ count($messagebox) }} {{ trans('Backend\message_box.ticketcount') }}
                         <button data-toggle="modal" data-target="#editor" class="btn btn-sm btn-danger float-right">{{ trans('Backend\message_box.new_messages') }}</button>
                     </div>




            </div>





        </div>
        </div>
        @else
        <div class="container-fluid pt-5">
        <div class="text-center p-5">
            <i class="icon-note-important s-64 text-primary"></i>
            <h4 class="my-3">Hiç Mesajınız Yok.</h4>
            <p>Gönderilen kutunuzda hiç mesajınız yok.</p>
            <a href="#" data-toggle="modal" data-target="#editor" class="btn btn-primary shadow btn-lg"><i class="icon-plus-circle mr-2 "></i>Yeni Mesaj Oluştur</a>
        </div>
    </div>
        @endif

    </div>



    <div class="modal" tabindex="-1" role="dialog" id="editor">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Yeni Mesaj</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <form action="" method="POST" id="form">
              @csrf

@if(Sentinel::getUser()->id == 1)
              <div class="form-group">
                <label>Adı Soyadı</label>
                <input type="text" id="autocek" class="form-control" name="firstname" placeholder="ad soyad,kullanıcı adı,tel,tc,email"/>
                <input type="hidden" value="" class="userid" name="user_id">
              </div>



              <div class="form-group">
                <label>Ülke</label>
                <select class="form-control countries" name="country">
                <option value="" selected>Seçiniz</option>
                @foreach($countries as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Şehir</label>
                <select class="form-control cities" name="city">
                <option selected >Seçiniz</option>
                </select>
              </div>


              @endif

              <div class="form-group">
                <label>Mesaj Konu</label>
                <select class="form-control" name="category">
                <option selected value="">Seçiniz</option>
                <option>Ödeme Sorunları</option>
                <option>Muhasebe</option>
                <option>Diğer</option>
                </select>
              </div>

              <div class="form-group">
                <label>Mesaj Başlığı</label>
                <input type="text" name="subject" class="form-control">
              </div>

              <div class="form-group">
                <label>Mesaj İçeriği</label>
                <textarea type="textarea" class="form-control" placeholder="Mesaj İçeriği" name="message_content"></textarea>
              </div>








              <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>
          </div>

        </div>
      </div>
    </div>


</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/message_box.js"></script>
<script src="{{ URL::to('') }}/js/jquery-ui.js"></script>

  @stop
