
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Seo Ayarları
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Seo Ayarları</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">
<div class="card">
        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">
                  <form class="form-horizontal tpater" action="" method="POST" id="form">
@csrf


<div class="row">
                    <div class="col-md-6">

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">Site Başlığı</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="site_title" placeholder="Site Başlığı" type="text" value="">
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">Site Logo</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="site_logo"  type="file" value="">
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">Site Favicon</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="site_favicon"  type="file" value="">
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">Açıklama</label>

                          <div class="col-sm-12">
                            <textarea class="form-control" name="site_desc" placeholder="Site Açıklaması"></textarea>
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">E-mail Adresi</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="site_title" placeholder="E-posta Adresi" type="email" value="">
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">Site Varsayılan Lisanı</label>

                          <div class="col-sm-12">
                              <select class="form-control">
                                <option>Turkish</option>
                                <option>English</option>
                                <option>Germany</option>
                                <option>Azerbaican</option>
                                <option>Turkmenistan</option>
                                <option>Uzbekistan</option>
                              </select>
                          </div>
                      </div>


                      <div class="form-group">
                          <label  class="col-sm-4 control-label">Google Analytics</label>

                          <div class="col-sm-12">
                            <textarea class="form-control" name="site_desc" placeholder="Site Açıklaması"></textarea>
                          </div>
                      </div>




                      </div>

                        </div>
                        </div>







                </div>



<div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">{{ trans('Backend/member_info.save') }}</button>
                                    </div>
                                </div>
                                </div>

                                </form>


                </div>


            </div>
            </div>




        </div>
    </div>


</div>
@stop

@section('js')
<script src="{{ URL::to('') }}/backend/js/password_change.js"></script>

  @stop
