
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ trans('Backend/new_register.newregister') }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all">{{ trans('Backend/new_register.stepone') }}</a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2">{{ trans('Backend/new_register.steptwo') }}</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <form class="form-horizontal tpater" action="" method="POST" id="formpost">
    <div class="container-fluid animatedParent animateOnce my-3">
<div class="card">
        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">

@csrf
<input type="hidden" name="registrar_id" value="{{ $user->id }}">
<input type="hidden" name="parent_id" value="{{ @$registerinfo['parent_id'] }}">
<input type="hidden" name="left_side" value="{{ @$registerinfo['left_side'] }}">
<input type="hidden" name="right_side" value="{{ @$registerinfo['right_side'] }}">
<div class="row">
                    <div class="col-md-6">

                              <input class="form-control" name="reference_code" type="hidden" value="{{ $reference_code }}">


                      <div class="form-group">
                          <label for="inputEmail" class="col-sm-4 control-label">{{ trans('Backend/new_register.username') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="username"   placeholder="{{ trans('Backend/new_register.username') }}" type="text" >
                          </div>
                      </div>


                      <div class="form-group">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/new_register.firstname') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="first_name"  placeholder="{{ trans('Backend/new_register.firstname') }}" type="text" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="inputEmail" class="col-sm-4 control-label">{{ trans('Backend/new_register.lastname') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="last_name" placeholder="{{ trans('Backend/new_register.lastname') }}" type="text" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">{{ trans('Backend/new_register.identity') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="identity"   placeholder="{{ trans('Backend/new_register.identity') }}" type="text" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/new_register.email') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="email"   placeholder="{{ trans('Backend/new_register.email') }}" type="email" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/new_register.password') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" id="password" name="password"  placeholder="{{ trans('Backend/new_register.password') }}" type="password" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/new_register.againpassword') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="confirm_password"  placeholder="{{ trans('Backend/new_register.againpassword') }}" type="password" >
                          </div>
                      </div>




                      </div>

                      <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-sm-4 control-label">{{ trans('Backend/new_register.telephone') }}</label>

                            <div class="col-sm-12">
                                <input class="form-control" name="telephone" autocomplete="telephone"  placeholder="{{ trans('Backend/new_register.telephone') }}" type="tel" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-4 control-label">{{ trans('Backend/new_register.birthday') }}</label>

                            <div class="col-sm-12">
                                <input class="form-control" name="birthday" autocomplete="birthday" id="datepicker" placeholder="{{ trans('Backend/new_register.birthday') }}" type="text" value="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/new_register.country') }}</label>

                            <div class="col-sm-12">
                                <select class="form-control countries" name="country" autocomplete="country"  type="text">
                                <option selected disabled>{{ trans('Backend/new_register.select') }}</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/new_register.city') }}</label>

                            <div class="col-sm-12">
                                <select class="form-control cities" autocomplete="city" name="city" type="text">
                                <option value="">{{ trans('Backend/new_register.firstselectcountry') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/new_register.district') }}</label>

                            <div class="col-sm-12">
                              <input type="text" name="district" class="form-control"  placeholder="{{ trans('Backend/new_register.district') }}" value="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-2 control-label">{{ trans('Backend/new_register.address') }}</label>

                            <div class="col-sm-12">
                              <textarea class="form-control" name="address" placeholder="{{ trans('Backend/new_register.address') }}"></textarea>
                            </div>
                        </div>




                        </div>
                        </div>







                </div>



<div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="button" class="btn btn-danger" id="formsubmit">{{ trans('Backend/new_register.next') }}</button>
                                    </div>
                                </div>
                                </div>




                </div>








            </div>



            <div class="tab-pane animated fadeInUpShort show " id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2">

                <div class="row my-3">
<div class="col-md-12">

@csrf
<input type="hidden" name="registrar_id" value="{{ $user->id }}">
<div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/new_register.packageselect') }}</label>

                          <div class="col-sm-12">
                              <select class="form-control" name="package_id">
                                <option value="">{{ trans('Backend/new_register.select') }}</option>
                                @foreach($packages as $package)
                                <option value="{{ $package->id }}">{{ $package->name }}</option>
                                @endforeach
                              </select>
                          </div>
                      </div>


                      <div class="form-group">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/new_register.paymentmethod') }}</label>

                          <div class="col-sm-12">
                            <select class="form-control method" name="payment_method">
                            <option value="">{{ trans('Backend/new_register.select') }}</option>
                            <option value="1">Kredi Kartı</option>
                            <option value="2">Havale</option>
                            </select>
                          </div>
                      </div>

                      <div class="form-group banks" style="display:none;">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/new_register.bankselect') }}</label>

                          <div class="col-sm-12">
                            <select class="form-control method" name="bank_id">
                            <option value="">{{ trans('Backend/new_register.select') }}</option>
                            @foreach($banklists as $banklist)
                            <option value="{{ $banklist->id }}">{{ $banklist->bank_name }}</option>
                            @endforeach
                            </select>
                          </div>
                      </div>







                      </div>


                        </div>







                </div>



<div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit"  class="btn btn-danger">{{ trans('Backend/new_register.save') }}</button>
                                    </div>
                                </div>
                                </div>




                </div>








            </div>







        </div>








    </div>
    </div>
    </form>


</div>
@stop

@section('js')
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>

<script src="{{ URL::to('') }}/backend/assets/js/datepicker-tr.js"></script>
<script src="{{ URL::to('') }}/backend/js/new_register.js"></script>

  @stop
