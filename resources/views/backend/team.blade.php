
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">

@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Ekibim
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Ekibim</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>

    @if(isset($users[Sentinel::getUser()->id]) AND !empty($users[Sentinel::getUser()->id]) )
    <div class="container-fluid animatedParent animateOnce my-3">

<div class="card">

        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">


                 <div class="col-md-12">

                   <div class="teamlist">

                     <ul class="accordion">




                       @foreach($users[Sentinel::getUser()->id] as $write)

                       <?php
                       if($write['countryflag']){
                         $country = $write['countryflag'];
                       }
                       else{
                         $country = "none";
                       }


                       $icon=null;
                       if(isset($users[$write['id']]) AND !empty($users[$write['id']])){
                         $icon='<i class="icon-plus"></i>';
                       }

                       



                       ?>

                         <li>

                       		<a href="javascript:;" class="clicks" id="{{ $write['id'] }}">@php echo $icon @endphp</a><img src="/flags/{{ $country }}.png" width="25px" style="margin-right:10px;">{{ $write['fullname'] }}
                          <span id="teamspan"> / {{ $write['tel'] }} - </span>
                          <span id="teamspan">{{ $write['email'] }}</span>


                          @if(Sentinel::getUser()->user_type == 1)

                            <span class="teamspanright"><a href=""> <i class="icon-pencil"> </i> </a></span>
                          @endif



                          <span class="teamspanright"> Kariyer </span>




                        </li>

                       @endforeach


</ul>

                   </div>


                </div>




            </div>




        </div>
        </div>
    </div>


</div>

@else
<div class="container-fluid pt-5">
        <div class="text-center p-5">
            <i class="icon-note-important s-64 text-primary"></i>
            <h4 class="my-3">Ekibiniz Yok</h4>
            <p>Ekip oluşturmak için aşağıdaki butona tıklayın</p>
            <a href="{{ route('yenikayit') }}" class="btn btn-primary shadow btn-lg"><i class="icon-plus-circle mr-2 "></i>Yeni Bayi Kaydet</a>
        </div>
    </div>
@endif

@stop

@section('js')
<script src="{{ URL::to('') }}/backend/js/team.js"></script>


  @stop
