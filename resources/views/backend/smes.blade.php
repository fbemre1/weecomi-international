
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ trans('Backend/smes.smesregister') }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all">{{ trans('Backend/smes.smesregister') }}</a>
                    </li>




                </ul>
            </div>
        </div>
    </header>
    <form class="form-horizontal tpater" action="" method="POST" id="formpost">
      @csrf
    <div class="container-fluid animatedParent animateOnce my-3">
      <div class="card">
        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">


<div class="row">
                    <div class="col-md-6">




                      <div class="form-group">
                          <label for="inputEmail" class="col-sm-4 control-label">{{ trans('Backend/smes.sponsor_code') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="sponsor_code"  placeholder="{{ trans('Backend/smes.sponsor_code') }}" type="text">
                          </div>
                      </div>


                      <div class="form-group">
                          <label for="inputName" class="col-sm-4 control-label">{{ trans('Backend/smes.sector') }}</label>

                          <div class="col-sm-12">
                              <select class="form-control" name="sector">
                                <option selected disabled>{{ trans('Backend/smes.select') }}</option>

                                @foreach($sectors as $sector)
                                <option value="{{ $sector->id }}">{{ $sector->$lang }}</option>
                                @endforeach
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="inputEmail" class="col-sm-4 control-label">{{ trans('Backend/smes.storename') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="store_name" placeholder="Mağazanın Adı" type="text" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">{{ trans('Backend/smes.title') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="title"   placeholder="{{ trans('Backend/smes.title') }}" type="text" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/smes.taxadortaxnum') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="taxador_number"   placeholder="{{ trans('Backend/smes.taxadortaxnum') }}" type="text" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/smes.username') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="username"  placeholder="{{ trans('Backend/smes.username') }}" type="text" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/smes.password') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="password"  placeholder="{{ trans('Backend/smes.password') }}" type="password" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/smes.email') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="email"  placeholder="{{ trans('Backend/smes.email') }}" type="email" >
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">{{ trans('Backend/smes.telephone') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="telephone"  placeholder="{{ trans('Backend/smes.telephone') }}" type="tel">
                          </div>
                      </div>


                      </div>

                      <div class="col-md-6">



                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.discountrate') }}</label>

                            <div class="col-sm-12">
                                <input class="form-control" name="discount_rate"  placeholder="{{ trans('Backend/smes.discountrate') }}" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.maps') }}</label>

                            <div class="col-sm-12">
                              <input type="text" name="maps" class="form-control"  placeholder="{{ trans('Backend/smes.maps') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.workinghours') }}</label>

<div class="col-md-12">
<div class="row">
                            <div class="col-sm-6">
                              <input type="text" name="working_start" class="form-control datepicker"  autocomplete="off"  placeholder="{{ trans('Backend/smes.startdate') }}">
                            </div>

                            <div class="col-sm-6">
                              <input type="text" name="working_end" class="form-control datepicker"  autocomplete="off"  placeholder="{{ trans('Backend/smes.enddate') }}">
                            </div>
                            </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.country') }}</label>

                            <div class="col-sm-12">
                                <select class="form-control countries" name="country" autocomplete="country"  type="text">
                                <option selected disabled>{{ trans('Backend/smes.select') }}</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.city') }}</label>

                            <div class="col-sm-12">
                                <select class="form-control cities" autocomplete="city" name="city" type="text">
                                <option value="">{{ trans('Backend/smes.select') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.district') }}</label>

                            <div class="col-sm-12">
                              <input type="text" name="district" class="form-control"  placeholder="{{ trans('Backend/smes.district') }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.address') }}</label>

                            <div class="col-sm-12">
                              <textarea class="form-control" name="address" placeholder="{{ trans('Backend/smes.address') }}"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label  class="col-sm-6 control-label">{{ trans('Backend/smes.companylogo') }}</label>

                            <div class="col-sm-12">
                              <button class="btn btn-primary" id="fileselector" type="button" style="margin-bottom:20px;"><i class="icon-cloud_upload"></i> {{ trans('Backend/smes.fileselect') }}</button>
                              <input id="filesec" type="file" name="file" style="display:none">
                            </div>
                        </div>






                        </div>
                        </div>







                </div>



<div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">{{ trans('Backend/smes.save') }}</button>
                                    </div>
                                </div>
                                </div>

                </div>


            </div>



        </div>
        </div>








    </div>
    </form>


</div>
@stop

@section('js')
<script>
  $(document).ready(function(){

    $('.datepicker').datetimepicker({
  datepicker:false,
  format:'H:i'
});






  });
</script>

<script src="{{ URL::to('') }}/backend/assets/js/datepicker-tr.js"></script>
<script src="{{ URL::to('') }}/backend/js/smes.js"></script>


  @stop
