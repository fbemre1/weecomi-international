@extends("backend.layouts.app")

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ trans('Backend/sme_register_ids.smeregisterid') }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>{{ trans('Backend/sme_register_ids.smeregisterid') }}</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">

        <div class="tab-content my-3" id="v-pills-tabContent">

@csrf


            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">

                  <div class="col-md-12">
                      <div class="card no-b">
                            <div class="card-body">

                          <table id="sme_sectors" class="table table-striped table-bordered nowrap" style="width:100%;">
                              <thead>
                              <tr>
                                  <th>id</th>
                                  <th>{{ trans('Backend/sme_register_ids.smeids') }}</th>
                                  <th>{{ trans('Backend/sme_register_ids.smenames') }}</th>
                                  <th>{{ trans('Backend/sme_register_ids.created_at') }}</th>
                              </tr>
                              </thead>
                              <tbody>

                              </tfoot>
                          </table>
                      </div>
                          </div>
                  </div>

                </div>


            </div>







        </div>
    </div>



</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/sme_register_ids.js?={{ trans('Backend/sme_register_ids.datatablelang') }}"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.flash.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/jszip.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/vfs_fonts.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.html5.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.print.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.responsive.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/responsive.bootstrap.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.colVis.min.js"></script>

@stop
