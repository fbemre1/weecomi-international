
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ URL::to('') }}/backend/assets/img/basic/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @yield('css')
    <title>Weecomi Back Office</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{ URL::to('') }}/backend/assets/css/app.css">
    <style>

.rwd-table{
  cursor:pointer;

}

.rwd-table tr:hover{
  background:#e1e1e1;
}

img.akbank-logo {
    width: 65%;
}

.errorplace{
  font-weight:bold;
}

        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
    </style>
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
<div class="page">
<div class="col-md-6 offset-3" style="margin-top:5%">


  <div class="card no-b  shadow">

    <div id="step-1" class="acticard-body text-center p-5">
<h3>Ödeme Yapmadınız, Lütfen Ödemenizi Gerçekleştirin.</h3>

<br>

<form action="" method="POST" id="packagefilter">
  @csrf
<div class="form-group">
<label>Paket Seçiniz </label>
<select class="form-control package" name="package">
<option selected disabled>Seçiniz</option>
@foreach($packages as $package)
<option value="{{ $package->id  }}">{{ $package->name }}</option>
@endforeach
</select>
</div>

<div class="form-group">
<label>Ödeme Yöntemi Seçiniz </label>
<select class="form-control method" name="package">
<option selected disabled>Seçiniz</option>
<option value="1">Kredi Kartı</option>
<option value="2">Havale</option>
</select>
</div>

<div class="package_info">
  <center><b>Paket Bilgileri</b></center>
  <br>
  <ul class="package_list">
    <ul>
</div>





<div class="transfer" style="display:none;">

<br>
<input type="hidden" name="selectbank" class="selectbank">
  <table class="rwd-table">
    <tbody>
      <tr>
        <th style="font-weight:500"></th>
        <th>Banka</th>
        <th>Şube/Şube Kodu</th>
        <th>Hesap Sahibi</th>
        <th>Hesap No</th>
        <th>IBAN NO</th>
        <th>Seç</th>

      </tr>

      @foreach($banklists as $banklist)
      <tr>
        <td data-th="::::">
          <img class="akbank-logo" src="/img/bank/{{ $banklist->bank_image }}">
        </td>
        <td data-th="Banka">
          {{ $banklist->bank_name }}
        </td>
        <td data-th="Şube/Şube Kodu">
          {{ $banklist->branch_code }}
        </td>
        <td data-th="Hesap Sahibi">
          {{ $banklist->holder_name }}
        </td>
        <td data-th="Hesap No">
          {{ $banklist->account_no }}
        </td>
        <td data-th="IBAN No">
          {{ $banklist->iban_no }}
        </td>
        <td data-th="işlem">
          <input type="radio" name="selectbank" class="selectbank" value="{{ $banklist->id }}">
        </td>
      </tr>
      @endforeach




    </tbody>
  </table>
<br>
  <p class="errorplace"></p>
  <p style="color:red; font-weight:bold;">Ödemeyi Yaptıktan sonra havale bildirimi yap butonuna basınız.</p>


</div>


<br>

</form>


<a href="javascript:;" class="btn btn-primary mb-3 btn-lg transfer transbtn" style="display:none;">Havale bildirimi yap</a>

<form action="http://127.0.0.1/deneme/iframe_ornek.php" method="POST">
<input type="hidden" name="userid" value="{{ $user->username }}" class="userid">
<input type="hidden" name="packageid" value="" class="packageid">
<input type="hidden" name="methodid" value="" class="methodid">
<input type="hidden" name="bankselector" value="" class="bankselector">
<input type="submit" class="btn btn-primary mb-3 btn-lg paymentbox" style="display:none;" value="Ödeme yap"></input>
</form>


       <a href="#step-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="btn btn-primary mb-3 btn-lg">Çıkış Yap</a>


               <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
       {{ csrf_field() }}
       </form>

    </div>
  </div>


</div>

</div>

<div class="control-sidebar-bg shadow white fixed"></div>
</div>

  <script src="{{ URL::to('') }}/backend/assets/js/app.js"></script>

<script src="{{ URL::to('') }}/backend/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('') }}/backend/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="validation/jquery.validate.min.js"> </script>
<script src="validation/localization/messages_tr.js"> </script>

<script>

$(document).ready(function(){

  $("#packagefilter").validate({
    rules: {
      package: {
        required: true,
      },
      selectbank: {
        required: true,
      }

    },
    errorElement: "div",
    errorPlacement: function (error, element) {

          error.appendTo($('.errorplace'));
      },
      messages: {
        package: "Lütfen bir paket seçiniz",
        selectbank: "Lütfen bir banka seçiniz",
      },
  });


  $(".rwd-table tr").click(function(){
    var index = $(".rwd-table tr").index(this);
    $(".rwd-table tr").not(':eq(0)').css("background","#fff");
    $(".rwd-table tr").eq(index).css("background","var(--green)");
    var a = $(".selectbank").eq(index).attr("checked",true);
    $(".bankselector").val(a.val());
  });

$(".package").change(function(){
  var package_id = $(".package option:selected").val();
  $(".packageid").val(package_id);
  $.ajax({
    url:"{{ URL::to('') }}/paketdetay/"+package_id,
    type:"GET",
    success:function(r){
      $(".package_list").html(r);
    }
  })
});



$(".paymentbox").click(function(){
  $("#packagefilter").valid();
});

$(".transbtn").click(function(){

  var filter = $("#packagefilter").valid();

  if(filter == true){

    _token = $("input[name=_token]").val();
    userid = $("input[name=userid]").val();
    packageid = $("input[name=packageid]").val();
    methodid = $("input[name=methodid]").val();
    bankselector = $("input[name=bankselector]").val();




      $.ajax({
        url:"{{ URL::to('') }}/panel/bildirimyap",
        type:"POST",
        data:{_token:_token,userid:userid,packageid:packageid,methodid:methodid,bankselector:bankselector},
        success:function(r){

          swal(r.head,
          r.content,
          r.status);
        


        }

      });

  }







});


$(".method").change(function(){
  method = $(".method option:selected").val();
  $(".methodid").val(method);

if(method == 1){
  $(".paymentbox").fadeIn();
}
else{
  $(".paymentbox").fadeOut();
}

if(method == 2){
  $(".transfer").fadeIn();
}
else{
  $(".transfer").fadeOut();
}


});

});

</script>

<style>

.wrapperchart{
  width:100%;
  height:100%;
}

.rwd-table {
    color: #404040;
    border-radius: 3px;
    overflow: hidden;
}

.rwd-table {
    margin: auto;
    min-width: 300px;
    max-width: 120%;
    width: 100%;
    border-collapse: collapse;
}

.rwd-table tr:first-child {
    border-top: none;
    background: #ADB3B8;
    color: #fff;
}
.rwd-table tr {
    border-color: #c5c5c5;
}
.rwd-table tr {
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    background-color: #fff;
    font-size: 12px;
}


</style>


</body>

</html>
