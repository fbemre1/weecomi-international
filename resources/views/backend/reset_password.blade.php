
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Şifre Değiştir
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>Şifre Değiştir</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">
<div class="card">
        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">
                  <form class="form-horizontal tpater" action="" method="POST" id="form">
@csrf


<div class="row">
                    <div class="col-md-6">



                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/member_info.password') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="password" id="password" placeholder="{{ trans('Backend/member_info.password') }}" type="password" value="">
                          </div>
                      </div>

                      <div class="form-group">
                          <label  class="col-sm-4 control-label">{{ trans('Backend/member_info.againpassword') }}</label>

                          <div class="col-sm-12">
                              <input class="form-control" name="confirm_password" id="confirm_password" autocomplete="confirm_password"  placeholder="{{ trans('Backend/member_info.againpassword') }}" type="password" value="">
                          </div>
                      </div>
                      </div>

                        </div>
                        </div>







                </div>




                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">{{ trans('Backend/member_info.save') }}</button>
                                    </div>
                                </div>


                                </form>


                </div>


            </div>




        </div>
        </div>
    </div>


</div>
@stop

@section('js')
<script src="{{ URL::to('') }}/backend/js/password_change.js"></script>

  @stop
