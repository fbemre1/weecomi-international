
@extends('backend.layouts.app')

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection

@section('content')
<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Yerleşim Kaydı Tamamla
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all">Yerleşim Kaydı Tamamla</a>
                    </li>




                </ul>
            </div>
        </div>
    </header>
    <form class="form-horizontal tpater" action="" method="POST" id="formpost">
    <div class="container-fluid animatedParent animateOnce">

        <div class="tab-content my-3" id="v-pills-tabContent">

            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">
<div class="col-md-12">

@csrf
<input type="hidden" name="registrar_id" value="{{ @$hidden[registrar_id] }}">
<input type="hidden" name="parent_id" value="{{ @$hidden[parent_id] }}">
<input type="hidden" name="left_side" value="{{ @$hidden[left_side] }}">
<input type="hidden" name="right_side" value="{{ @$hidden[right_side] }}">
<div class="row">
                    <div class="col-md-6">

                      <div class="form-group">

                        <label for="inputEmail" class="col-sm-4 control-label">Sponsor</label>

                        <div class="col-sm-12">
                            <input class="form-control" name="username" placeholder="Sponsor kişi" type="text" value="{{ ucfirst($sponsor->firstname) }} {{ $sponsor->lastname }}" disabled>
                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputEmail" class="col-sm-4 control-label">Kullanıcı adı</label>

                        <div class="col-sm-12">
                            <input class="form-control" name="username" placeholder="Kullanıcı adı" type="text" value="{{ $settlement_user->username }}" disabled>
                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputEmail" class="col-sm-4 control-label">Adı</label>

                        <div class="col-sm-12">
                            <input class="form-control" name="username" placeholder="Adı" type="text" value="{{ $settlement_user->firstname }}" disabled>
                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputEmail" class="col-sm-4 control-label">Soyadı</label>

                        <div class="col-sm-12">
                            <input class="form-control" name="username" placeholder="Soyadı" type="text" value="{{ $settlement_user->lastname }}" disabled>
                        </div>

                      </div>

                      </div>


                        </div>




                </div>



<div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Kaydı Tamamla</button>
                                    </div>
                                </div>
                                </div>

                </div>


            </div>




        </div>








    </div>
    </form>


</div>
@stop

@section('js')
<script src="{{ URL::to('') }}/backend/js/settlement_register.js"></script>
  @stop
