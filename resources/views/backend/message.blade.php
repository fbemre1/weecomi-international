@extends('backend.layouts.app')
@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/css/jquery-ui.css">
@endsection



@section('content')


<div class="page has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ $messagebox->subject }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>{{ trans('Backend/messages.messagesdetail') }}</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce my-3">

@if($messagebox->status == 0)
<?php $status = "Kapalı"; ?>
<?php $notify = "secondary"; ?>
@else
<?php $notify = "success"; ?>
<?php $status = "Açık"; ?>
@endif


      <div class="alert alert-{{ $notify }}" role="alert" style="text-align:center;">

<b>Mesaj Durumu = {{ $status }}</b>

      </div>

      <div class="card b-0  msgcontent">



        @foreach($messages as $write)


          <div class="card-body buffymag">


                  <div class="media">
                      <img class="d-flex mr-3 height-50" src="/user_avatar/default.png"
                           alt="Generic placeholder image">
                      <div class="media-body">
                          <h6 class="mt-0 mb-1 font-weight-normal">{{ $write["sender_name"] }}</h6>
                          <small>{{ $write["created_at"] }}</small>
                          <div class="collapse my-3 show" id="message1">
                              <div>
                                  {{ $write["content"] }}
                              </div>

                          </div>
                      </div>
                  </div>


          </div>
  @endforeach

          <form method="POST" action="" id="form">
            @csrf
          <div class="card-footer white" style="border-top:none;">
            <textarea class="form-control" id="msgcnt" placeholder="bir mesaj yaz." name="msg_content"></textarea>
            <input type="hidden" name="msg_id" value="{{ $messagebox->id }}">
          </div>
          <div class="col-md-12">
          <button class="btn btn-primary" style="margin:10px;" type="submit">Gönder</button>
         </div>
         </div>
         </form>




    </div>






</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/message.js"></script>
<script src="{{ URL::to('') }}/js/jquery-ui.js"></script>

  @stop
