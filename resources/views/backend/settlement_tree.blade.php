@extends("backend.layouts.app")
@section('css')
<link rel="stylesheet" type="text/css" href="/css/developer.css">
<link rel="stylesheet" type="text/css" href="/css/tree.css">
@stop

@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ trans('Backend/settlement_tree.settlementtree') }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>{{ trans('Backend/settlement_tree.settlementtree') }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

<div class="container-fluid animatedParent animateOnce my-3">

  <div class="row">
                <div class="col-md-12">
                  <div class="region region-content">
      <div id="block-system-main" class="block block-system clearfix">


    <div class="binary-genealogy-tree binary_tree_extended">
    <div class="binary-genealogy-level-0 clearfix">
      <div class="no_padding parent-wrapper clearfix">
        <div class="node-centere-item binary-level-width-100">

  		<div class="node-item-root">
          <div class="binary-node-single-item user-block user-0"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="business.admin" title="business.admin"></a></div>
  <span class="wrap_content "><a href="#"></a><a href="#">ROOT</a></span>


  </div>
    </div>



          <div class="parent-wrapper clearfix">


  		  <div class="node-left-item binary-level-width-50"> <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-25"></span>


  			<div class="node-item-root">
              <div class="binary-node-single-item user-block user-1">
  			<div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="mlm.member" title="mlm.member"></a></div>
              <span class="wrap_content "><a href="#"></a><a href="#">first.remember</a></span>

              </div>
              </div>

              <div class="parent-wrapper clearfix">


  			<div class="node-left-item binary-level-width-25"> <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-12"></span>

  			   <div class="node-item-root">
                  <div class="binary-node-single-item user-block user-3"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="John" title="John"></a></div>
                  <span class="wrap_content "><a href="#"></a><a href="#">John</a></span>
  				</div>
                  </div>


  				<div class="parent-wrapper clearfix">
                        <div class="node-left-item binary-level-width-50"> <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-25"></span>
                          <div class="node-item-1-child-left">
                            <div class="binary-node-single-item user-block user-9"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="Add new member" title="Add new member"></a></div><span class="wrap_content"><a href="#">Add new member</a></span></div>
                                                        <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                    </div>
                        </div>
                        <div class="node-right-item binary-level-width-50"> <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-25"></span>
                          <div class="node-item-1-child-right">
                            <div class="binary-node-single-item user-block user-10"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="Add new member" title="Add new member"></a></div><span class="wrap_content"><a href="#">Add new member</a></span></div>
                                                        <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                    </div>
                        </div>
                      </div>



                                    </div>



                  <div class="node-right-item binary-level-width-25"> <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-12"></span>
                    <div class="node-item-2-child-right node-item-root">
                      <div class="binary-node-single-item user-block user-4"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="p1" title="p1"></a></div>
  <span class="wrap_content "><a href="#"></a><a href="#">p1</a></span>


  </div>
                                        </div>
                                        <div class="parent-wrapper clearfix">
                        <div class="node-left-item binary-level-width-50"> <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-25"></span>
                          <div class="node-item-1-child-left">
                            <div class="binary-node-single-item user-block user-9"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/img/{{ trans('Backend/settlement_tree.addmember') }}" width="70" height="70" alt="Add new member" title="Add new member"></a></div><span class="wrap_content"><a href="#">Add new member</a></span></div>
                                                        <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                    </div>
                        </div>
                        <div class="node-right-item binary-level-width-50"> <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-25"></span>
                          <div class="node-item-1-child-right">
                            <div class="binary-node-single-item user-block user-10"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/img/{{ trans('Backend/settlement_tree.addmember') }}" width="70" height="70" alt="Add new member" title="Add new member"></a></div><span class="wrap_content"><a href="#">Add new member</a></span></div>
                                                        <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                    </div>
                        </div>
                      </div>
                                    </div>

              </div>



            </div>


            <div class="node-right-item binary-level-width-50"> <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-25"></span>

              <div class="node-item-1-child-right   node-item-root">
  				 <div class="binary-node-single-item user-block user-2"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/user_avatar/default.png" width="70" height="70" alt="second.memeber" title="second.memeber"></a></div>
  				 <span class="wrap_content "><a href="#"></a><a href="#">second.memeber</a></span>
  				 </div>
              </div>

              <div class="parent-wrapper clearfix">

                            <div class="node-left-item binary-level-width-25"> <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-12"></span>
                  <div class="node-item-2-child-left ">
                    <div class="binary-node-single-item user-block user-5"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/img/{{ trans('Backend/settlement_tree.addmember') }}" width="70" height="70" alt="Add new member" title="Add new member"></a></div><span class="wrap_content"><a href="#">Add new member</a></span></div>
                                     </div>
                                </div>

                <div class="node-right-item binary-level-width-25"> <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-12"></span>
                  <div class="node-item-2-child-right ">
                    <div class="binary-node-single-item user-block user-6"><div class="images_wrapper"><a href="#"><img class="profile-rounded-image-small" src="/img/{{ trans('Backend/settlement_tree.addmember') }}" width="70" height="70" alt="Add new member" title="Add new member"></a></div><span class="wrap_content"><a href="#">Add new member</a></span></div>
                                    </div>
                                </div>

               </div>

            </div>



          </div>
        </div>
      </div>
    </div>
  </div>

  </div>
   </div>
   </div>
   </div>

</div>




</div>




@stop
