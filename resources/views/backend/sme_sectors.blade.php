@extends("backend.layouts.app")

@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to('') }}/backend/assets/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        {{ trans('Backend/sme_sectors.sectorsettings') }}
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all"
                           role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>{{ trans('Backend/sme_sectors.sectorsettings') }}</a>
                    </li>



                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">

        <div class="tab-content my-3" id="v-pills-tabContent">

@csrf


            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">

                <div class="row my-3">

                  <div class="col-md-12">
                      <div class="card no-b">
                            <div class="card-body">

                          <table id="sme_sectors" class="table table-striped table-bordered nowrap" style="width:100%;">
                              <thead>
                              <tr>
                                  <th>id</th>
                                  <th>{{ trans('Backend/sme_sectors.sectornametr') }}</th>
                                  <th>{{ trans('Backend/sme_sectors.sectornameen') }}</th>
                                  <th>{{ trans('Backend/sme_sectors.created_at') }}</th>
                                  <th>{{ trans('Backend/sme_sectors.actions') }}</th>
                              </tr>
                              </thead>
                              <tbody>

                              </tfoot>
                          </table>
                      </div>
                          </div>
                  </div>

                </div>


            </div>







        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="editor">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Düzenleme</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="formupdate">
              @csrf
              <input type="hidden" name="id" id="rowid">
              <div class="form-group">
                <label>{{ trans('Backend/sme_sectors.sectornametr') }}</label>
                <input type="text" class="form-control" id="sector_tr" placeholder="{{ trans('Backend/sme_sectors.sectornametr') }}" name="sector_tr">
              </div>
              <div class="form-group">
                <label>{{ trans('Backend/sme_sectors.sectornameen') }}</label>
                <input type="text" class="form-control" id="sector_en"  placeholder="{{ trans('Backend/sme_sectors.sectornameen') }}" name="sector_en">
              </div>

              <button type="submit" class="btn btn-primary">{{ trans('Backend/sme_sectors.save') }}</button>
            </form>
          </div>

        </div>
      </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="addsector">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Sektör Ekle</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" method="POST" id="form">
              @csrf
              <div class="form-group">
                <label>{{ trans('Backend/sme_sectors.sectornametr') }}</label>
                <input type="text" class="form-control" id="sector_tr" placeholder="{{ trans('Backend/sme_sectors.sectornametr') }}" name="sector_tr">
              </div>
              <div class="form-group">
                <label>{{ trans('Backend/sme_sectors.sectornameen') }}</label>
                <input type="text" class="form-control" id="sector_en"  placeholder="{{ trans('Backend/sme_sectors.sectornameen') }}" name="sector_en">
              </div>

              <button type="submit" class="btn btn-primary">{{ trans('Backend/sme_sectors.save') }}</button>
            </form>
          </div>

        </div>
      </div>
    </div>

</div>
@stop

@section('js')

<script src="{{ URL::to('') }}/backend/js/sme_sectors.js?={{ trans('Backend/sme_sectors.datatablelang') }}"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.flash.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/jszip.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/vfs_fonts.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.html5.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.print.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/dataTables.responsive.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/responsive.bootstrap.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/buttons.colVis.min.js"></script>

@stop
