@extends("layouts.app")

@section("content")
<section class="title-bg-dark">
<div class="container">
<div class="page-title text-white text-center">
<h2 data-animate="fadeInUp" data-delay="1.2" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 1.2s;">
Weecomi Nedir ?</h2>
<ul class="custom-breadcrumb karla list-unstyled m-0 animated fadeInUp" data-animate="fadeInUp" data-delay="1.4" style="animation-duration: 0.6s; animation-delay: 1.4s;">
<li class="breadcrumb-item"><a href="index.html">Anasayfa</a></li>
<li class="breadcrumb-item"><a href="#">Weecomi Nedir ?</a></li>
</ul>
</div>
</div>
</section>
@stop
