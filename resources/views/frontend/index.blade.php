@extends('layouts.app')

@section("content")
<section class="main-banner bg-primary bg-rotate position-relative">

<div class="container">

<div class="row justify-content-center">

<div class="col-xl-8 col-lg-10">

<div class="banner-content text-center text-white"><h2 data-animate="fadeInUp" data-delay="1.2">Harcadıkça Kazan<br>Kazandıkça Harca</h2>

<p data-animate="fadeInUp" data-delay="1.3">Weecomi komisyon sistemiyle çalışan müşteriler için indirim, kobiler için müşteri ve bu ekosisteme dahil olan iş ortakları için de çok karlı bir kazanç sistemidir.</p>
<ul class="list-inline" data-animate="fadeInUp" data-delay="1.4">
<li><a class="btn btn-transparent" href="#">Özellikleri gör</a></li>
<li><a class="btn btn-transparent" href="#">Daha fazla bilgi</a></li>
</ul>
</div>
</div>
</div>

<span class="goDown" data-animate="fadeInUp" data-delay="1.5"><i class="fas fa-arrow-down bounce"></i>
</span>
</div>
</section>


<section class="home-features pt-175 pb-175">

<div class="container">

<div class="row">

<div class="col-lg-3 col-sm-6">

<div class="single-home-feature" data-animate="fadeInUp" data-delay=".1"> <img class="svg" src="img/high.svg" alt=""><h3>Alışveriş Yapın !</h3>

<p>Weecomi üyesi iş yerlerinden alışveriş yapın para puan kazanın.</p>
</div>
</div>

<div class="col-lg-3 col-sm-6">

<div class="single-home-feature" data-animate="fadeInUp" data-delay=".3"> <img class="svg" src="img/security.svg" alt=""><h3>Para Puan Biriktirin !</h3>

<p>Biriktirdiğiniz para puanlarınızla ücretsiz alışveriş yapın!</p>
</div>
</div>

<div class="col-lg-3 col-sm-6">

<div class="single-home-feature" data-animate="fadeInUp" data-delay=".5"> <img class="svg" src="img/guard.svg" alt=""><h3>Tavsiye Edin !</h3>

<p>Tavsiye ettiğiniz kişiler de sizinle beraber kazansın onlar alışveriş yapsın hem siz hem onlar kazansın.</p>
</div>
</div>

<div class="col-lg-3 col-sm-6">

<div class="single-home-feature" data-animate="fadeInUp" data-delay=".7"> <img class="svg" src="img/support.svg" alt=""><h3>İndirimlerden Yararlanın !</h3>

<p>Weecomi üyesi iş yerlerinden yapacağınız avantajlı alışverişlerle bütçenize destek olun !</p>
</div>
</div>
</div>
</div> </section> <section class="pricing-plans bg-light bg-rotate position-relative">

<div class="container">

<div class="row justify-content-center">

<div class="col-xl-6 col-lg-8">

<div class="section-title text-center"><h2 data-animate="fadeInUp" data-delay=".1">Paketlerimiz</h2>

<p data-animate="fadeInUp" data-delay=".2">Weecomi Üye Paketlerimiz</p>
</div>
</div>
</div>

<div class="row">

<div class="col-lg-3 col-sm-6">

<div class="single-pricing-plan text-center" data-animate="fadeInUp" data-delay=".1"><h3>Startup</h3>

<p>You can use your title texe here</p>
<ul class="karla list-unstyled text-left">
<li>RAM 1 GB</li>
<li>Core CPU 1</li>
<li>SSD Storage 20 GB</li>
<li>Transfer 1 TB</li>
<li>Network In 40 Gbps</li>
<li>Network Out 1000 Mbps</li>
</ul>

<span class="text-left"><sup>$</sup>19<sub class="karla">M</sub>
</span>

<div class="purchase bg-dark bg-rotate position-relative"> <a href="#" class="btn btn-transparent">Satın Al</a>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">

<div class="single-pricing-plan text-center" data-animate="fadeInUp" data-delay=".3">

<span class="popular">Popular Sale
</span><h3>Standard</h3>

<p>You can use your title texe here</p>
<ul class="karla list-unstyled text-left">
<li>RAM 1 GB</li>
<li>Core CPU 1</li>
<li>SSD Storage 20 GB</li>
<li>Transfer 1 TB</li>
<li>Network In 40 Gbps</li>
<li>Network Out 1000 Mbps</li>
</ul>

<span class="text-left"><sup>$</sup>29<sub class="karla">M</sub>
</span>

<div class="purchase bg-dark bg-rotate position-relative"> <a href="#" class="btn btn-transparent">Satın Al</a>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">

<div class="single-pricing-plan text-center" data-animate="fadeInUp" data-delay=".5"><h3>Business</h3>

<p>You can use your title texe here</p>
<ul class="karla list-unstyled text-left">
<li>RAM 1 GB</li>
<li>Core CPU 1</li>
<li>SSD Storage 20 GB</li>
<li>Transfer 1 TB</li>
<li>Network In 40 Gbps</li>
<li>Network Out 1000 Mbps</li>
</ul>

<span class="text-left"><sup>$</sup>39<sub class="karla">M</sub>
</span>

<div class="purchase bg-dark bg-rotate position-relative"> <a href="#" class="btn btn-transparent">Satın Al</a>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">

<div class="single-pricing-plan text-center" data-animate="fadeInUp" data-delay=".7"><h3>Startup</h3>

<p>You can use your title texe here</p>
<ul class="karla list-unstyled text-left">
<li>RAM 1 GB</li>
<li>Core CPU 1</li>
<li>SSD Storage 20 GB</li>
<li>Transfer 1 TB</li>
<li>Network In 40 Gbps</li>
<li>Network Out 1000 Mbps</li>
</ul>

<span class="text-left"><sup>$</sup>59<sub class="karla">M</sub>
</span>

<div class="purchase bg-dark bg-rotate position-relative"> <a href="#" class="btn btn-transparent">Satın Al</a>
</div>
</div>
</div>
</div>
</div> </section> <section class="pt-175 pb-175">

<div class="container">

<div class="row align-items-center">

<div class="col-lg-6">

<div class="section-title text-left"><h2 data-animate="fadeInUp" data-delay=".1">Neden Biz ?</h2>

<p data-animate="fadeInUp" data-delay=".2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
</div>

<div class="row">

<div class="col-sm-6">

<div class="single-reason" data-animate="fadeInUp" data-delay=".3"><h3>01. High Performance</h3>

<p>Sed ut perspiciatis unde omnis isterror sit accusantium</p>
</div>
</div>

<div class="col-sm-6">

<div class="single-reason" data-animate="fadeInUp" data-delay=".4"><h3>02. Enhance Security</h3>

<p>Sed ut perspiciatis unde omnis isterror sit accusantium</p>
</div>
</div>

<div class="col-sm-6">

<div class="single-reason" data-animate="fadeInUp" data-delay=".5"><h3>03. Spam Guard</h3>

<p>Sed ut perspiciatis unde omnis isterror sit accusantium</p>
</div>
</div>

<div class="col-sm-6">

<div class="single-reason" data-animate="fadeInUp" data-delay=".6"><h3>04. Unbeatable Support</h3>

<p>Sed ut perspiciatis unde omnis isterror sit accusantium</p>
</div>
</div>
</div> <a href="#" class="btn btn-primary" data-animate="fadeInUp" data-delay=".7">Satın al</a>
</div>

<div class="col-lg-6">

<div class="why-us-video text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1"> <img data-rjs="3" src="img/video-bg.jpg" alt="">

<p> <a class="youtube-popup" href="https://www.youtube.com/watch?v=wb49-oV0F78"> <img data-rjs="3" src="img/play.png" alt=""> </a>

<span>You Will Buy After Watching
</span></p>
</div>
</div>
</div>
</div> </section> <section class="bg-light bg-rotate position-relative">

<div class="container">

<div class="row justify-content-center">

<div class="col-xl-6 col-lg-8">

<div class="section-title text-center"><h2 data-animate="fadeInUp" data-delay=".1">Müşteri Yorumları</h2>

<p data-animate="fadeInUp" data-delay=".2">Yıllardır her marketten alışveriş yapmışımdır. Fakat Weecomi'nin Akmar'da kullanıldığını duyunca hemen koştum. Hem alacaklarımı aldım hem de aynı fiyata satılan ürünleri bütçemi sarsmadan almış oldum. Teşekkürler herşey için.</p>
</div>
</div>
</div>

<div class="review-slider-wrap">

<div class="swiper-container review-slider">

<div class="swiper-wrapper">

<div class="swiper-slide bg-dark bg-rotate single-review">

<div class="review-info"> <i class="fas fa-quote-left float-left"></i><h4>Esma Altıncı</h4>

<span>Saray 2 Pastanesi
</span>
</div>

<p>Müthiş bir yer ve tüm ürünleri çok taze.. Weecomi kartım ile indirim yaptılar. Çok mutluyum. Teşekkürler Saray2, Teşekkürler Weecomi</p>
</div>

<div class="swiper-slide bg-dark bg-rotate single-review">

<div class="review-info"> <i class="fas fa-quote-left float-left"></i><h4>Nazlı Kılıçarslan</h4>

<span>Miss Gelinlik
</span>
</div>
<p>Gelinliğim için Miss Gelinlik'i seçtim ve Weecomi kartım ile indirim aldım. Hayatımın en önemli anında Weecomi ayrıcalığı yaşadım. Teşekkür ederim.</p>

</div>

<div class="swiper-slide bg-dark bg-rotate single-review">

<div class="review-info"> <i class="fas fa-quote-left float-left"></i><h4>Kaan Ekici</h4>
  <span>Metyar Organizasyon
  </span>

<span>
</span>
</div>
<p>METYAR Organizasyon sahibi İsa Metyar'a turizm ile ilgili çok yardımcı olduğu için teşekkürler. Weecomi kartı kullanarak, daha ucuz bir tatil deneyimi yaşadık.</p>

</div>

<div class="swiper-slide bg-dark bg-rotate single-review">

<div class="review-info"> <i class="fas fa-quote-left float-left"></i><h4>Jale Öztürk</h4>
<span>Akmar
</span>
</div>

<p>Yıllardır her marketten alışveriş yapmışımdır. Fakat Weecomi'nin Akmar'da kullanıldığını duyunca hemen koştum. Hem alacaklarımı aldım hem de aynı fiyata satılan ürünleri bütçemi sarsmadan almış oldum. Teşekkürler herşey için.</p>
</div>


</div>
</div>
</div>

<div class="swiper-pagination review-pagination">
</div>
</div>

<div class="container">

<div class="mt-70">

<div class="row align-items-center">

<div class="col-lg-7 col-md-6">

<div class="news-letter-title" data-animate="fadeInUp" data-delay=".1"><h2>Weecomi Bülten'e Kayıt Ol</h2>

<p>Weecomi Yeniliklerden ve Parapuanlardan haberdar ol !</p>
</div>
</div>

<div class="col-lg-5 col-md-6">

<div data-animate="fadeInUp" data-delay=".3">

<form class="position-relative subscribe-form" action="#" method="post" name="mc-embedded-subscribe-form" target="_blank">
<input class="form-control" type="email" name="EMAIL" autocomplete="off" placeholder="Email adresi" required>
<input type="submit" class="btn btn-primary" value="Kayıt Ol">
</form>
</div>
</div>
</div>
</div>
</div> </section> <section class="pt-175 pb-120">

<div class="container">

<div class="row justify-content-center">

<div class="col-xl-6 col-lg-8">

<div class="section-title text-center"><h2 data-animate="fadeInUp" data-delay=".1">Weecomi'den Haberler</h2>

<p data-animate="fadeInUp" data-delay=".2">Weecomi yeniliklerden son haberler</p>
</div>
</div>
</div>

<div class="row">

<div class="col-md-4">

<div class="single-post" data-animate="fadeInUp" data-delay=".1"> <a href="#"> <img data-rjs="3" src="img/post1.jpg" alt=""> </a>

<div class="post-content">

<p class="post-info">Posted on <a href="#">Jan 19, 2017</a> by <a href="#">Gladys R. Bailey</a></p><h3> <a href="#">Do People Think Your Ecommerce Business Name Is 500 error</a></h3>

<p>At vero eos et accusamus et iusto odio issimos ducimus qui blanditiis praesentium oluptatum niti atque corrupti quos dolores.</p> <a href="#">Reading Continue<i class="fas fa-caret-right"></i></a>
</div>
</div>
</div>

<div class="col-md-4">

<div class="single-post" data-animate="fadeInUp" data-delay=".3"> <a href="#"> <img data-rjs="3" src="img/post2.jpg" alt=""> </a>

<div class="post-content">

<p class="post-info">Posted on <a href="#">Jan 19, 2017</a> by <a href="#">Gladys R. Bailey</a></p><h3> <a href="#">How This Company Creates Instant Awesome Websites</a></h3>

<p>At vero eos et accusamus et iusto odio issimos ducimus qui blanditiis praesentium oluptatum niti atque corrupti quos dolores.</p> <a href="#">Reading Continue<i class="fas fa-caret-right"></i></a>
</div>
</div>
</div>

<div class="col-md-4">

<div class="single-post" data-animate="fadeInUp" data-delay=".5"> <a href="#"> <img data-rjs="3" src="img/post3.jpg" alt=""> </a>

<div class="post-content">

<p class="post-info">Posted on <a href="#">Jan 19, 2017</a> by <a href="#">Gladys R. Bailey</a></p><h3> <a href="#">Technology That Helps Small Business Stay Even the Big Players</a></h3>

<p>At vero eos et accusamus et iusto odio issimos ducimus qui blanditiis praesentium oluptatum niti atque corrupti quos dolores.</p> <a href="#">Reading Continue<i class="fas fa-caret-right"></i></a>
</div>
</div>
</div>
</div>
</div> </section> <section>

<div class="container">

  <div class="section-title text-center"><h2 data-animate="fadeInUp" data-delay=".1">Weecomi Kobilerimiz</h2>

  <p data-animate="fadeInUp" data-delay=".2">En Son eklenen kobilerimiz</p>
  </div>

<ul class="our-clients list-unstyled d-md-flex justify-content-md-between">
<li data-animate="fadeInUp" data-delay=".1"><img data-rjs="3" src="https://weecomi.com/mp-include/SARAY2.png" alt="saray2"></li>
<li data-animate="fadeInUp" data-delay=".2"><img data-rjs="3" src="https://weecomi.com/mp-include/AKMAR.png" alt="akmar"></li>
<li data-animate="fadeInUp" data-delay=".3"><img data-rjs="3" src="https://weecomi.com/mp-include/DBC.png" alt="dbc"></li>
<li data-animate="fadeInUp" data-delay=".4"><img data-rjs="3" src="https://weecomi.com/mp-include/Hedefkurslari.png" alt="hedefkurslari"></li>
<li data-animate="fadeInUp" data-delay=".5"><img data-rjs="3" src="https://weecomi.com/mp-include/abseronmarina.png" alt="abseronmarina"></li>
<li data-animate="fadeInUp" data-delay=".6"><img data-rjs="3" src="https://weecomi.com/mp-include/SeaMansionsuites.png" alt="seanman"></li>
</ul>
</div> </section>
@stop

@section("jss")

<script>

$(document).ready(function(){
  $(".main-footer").css("margin-top","300px");
});

</script>

@stop
