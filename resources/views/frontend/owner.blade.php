@extends("layouts.app")

@section("content")
<section class="title-bg-dark">
<div class="container">
<div class="page-title text-white text-center">
<h2 data-animate="fadeInUp" data-delay="1.2" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 1.2s;">
Kurucumuz</h2>
<ul class="custom-breadcrumb karla list-unstyled m-0 animated fadeInUp" data-animate="fadeInUp" data-delay="1.4" style="animation-duration: 0.6s; animation-delay: 1.4s;">
<li class="breadcrumb-item"><a href="index.html">Anasayfa</a></li>
<li class="breadcrumb-item"><a href="#">Kurucumuz</a></li>
</ul>
</div>
</div>
</section>

<section class="pt-120">
<div class="container">
<div class="row">

<div class="col-lg-9">
<p>1983 yılında Ordu/Kumru ilçesinde doğmuş, 1989 ilk ve orta öğretimini Kumru'da tamamlamıştır. Liseyi ve üniversiteyi </p>
<p>dışarıdan bitirerek mevzu olmuştur. Genç yaşta ticaretin gücüne inanarak ilk işletmesini Kırklareli Lüleburgaz’da 17 yaşında ufak bir pizza firması kurmuş ve kısa sürede sektörde öncü olmuştur.</p>
<p>Daha büyük hedeflere odaklanan CIBIR, fırsatları incelemeye başlamış ve network marketing sektörü ile tanışmıştır. </p>
<p>Türkiye’de o dönemde network sahasında sadece yabancı firmaların bulunduğu dikkatini çekmiş ve neden bir türk firması bu sahada olmasın diyerek gerekli ARGE çalışmalarına başlamıştır. Bu sırada farklı network firmalarında çalışıp üst düzeylere kadar çıkmıştır. </p>
<p>Binlerce insanın ekonomik özğürlüğe kavuşmasını sağlamıştır. Hayatında her zaman daha mükemmelini aramış devamlı araştırmalarda bulunmuştur. 22 ülkeye seyahat ederek oradaki ekonomi ve sektörleri incelemiştir. 2012 yılında Azerbaycan'da Weecomi’nin ilk temellerini atmış bugüne kadar gece gündüz demeden bir çok zorlukları aşarak Weecomi’yi şuanki konumuna getirmiştir. Hedefleri arasında ise Weecomi çatısı altında milyonlarca bireyi fırsatlarla tanıştırmak ve milyonlar kazanmalarını sağlamak bulunmaktadır.</p>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">
<div class="single-member animated fadeInUp" data-animate="fadeInUp" data-delay=".3" style="animation-duration: 0.6s; animation-delay: 0.3s;">
<img data-rjs="3" class="owner-profile" src="https://weecomi.com/upload/studio/hamza-ozkan-cibir.png" alt="">
<div class="member-info bg-dark bg-rotate">
<h4>Hamza Özkan CIBIR</h4>
<span>Weecomi international Kurucu Başkanı</span>
<ul class="list-unstyled text-right">
  <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
  <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
  <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
  <li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
  <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

</div>
</div>
</section>
@stop

@section("jss")

<script>

$(document).ready(function(){
  $(".main-footer").css("margin-top","300px");
});

</script>

@stop
