@extends("layouts.app")

@section("content")
<section class="title-bg-dark">
<div class="container">
<div class="page-title text-white text-center">
<h2 data-animate="fadeInUp" data-delay="1.2" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 1.2s;">
Hakkımızda</h2>
<ul class="custom-breadcrumb karla list-unstyled m-0 animated fadeInUp" data-animate="fadeInUp" data-delay="1.4" style="animation-duration: 0.6s; animation-delay: 1.4s;">
<li class="breadcrumb-item"><a href="index.html">Anasayfa</a></li>
<li class="breadcrumb-item"><a href="#">Hakkımızda</a></li>
</ul>
</div>
</div>
</section>

<section class="pt-120 pb-120">

<div class="container">

<div class="row align-items-center">

<div class="col-lg-6">

<div class="about-us-title text-left"><h2 data-animate="fadeInUp" data-delay=".1">Hakkımızda</h2>

<span data-animate="fadeInUp" data-delay=".2">Weecomi ile kazanmaya hemen başlayın şimdi uygulamlarımızdan sizin için en uygun olan uygulamayı indirin hemen Weecomi dünyasını keşfedin !
</span>
</div>

<p data-animate="fadeInUp" data-delay=".4">Weecomi, We Commission anlamına gelmektedir. Şirket defalarca tekrarlanan ve başarıya ulaşan bir modeli baz alarak tüm dünya genelinde tek bir indirim kart sisteminin kullanımını hedeflemiştir. Sistem 2010 yılı başlarında arge çalışmalarına başlamış yazılım ve alt yapı çalışmaları tamamlanmasının akabinde 2015 yılında Azerbaycan'da faaliyetlerine başlamıştır. </p>
<p data-animate="fadeInUp" data-delay=".4">Globalleşen ve gelişen dünyada teknolojik alt yapısnı tamamen yenileyen weecomi; tüm sistemlerini mobil kullanıma uygun olarak yeniden düzenlemiş ve müşterierin kobilerin, kobilerin müşterilere ve iş ortaklarının da kobilere ulaşmasını kolaştıracak bir takım yeniliklere imza atmıştır. Sizlerde bu yeni dünyada yerinizi almak weecomi müşterisi, kobisi ya da iş ortağı olmak için acele edin.</p>
</div>

<div class="col-lg-6">

<div class="why-us-video why-us-video-sticky-tape text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">

<div class="sticky-tape"> <img data-rjs="3" src="img/video-bg.jpg" alt="">

<p> <a class="youtube-popup" href="https://www.youtube.com/watch?v=XeoApWe62Lo"> <img data-rjs="3" src="img/play.png" alt=""> </a>

<span>Tanıtım Videomuz
</span></p>
</div>
</div>
</div>
</div>
</div> </section>



<section class="bg-light bg-rotate position-relative">

<div class="container">

<div class="row justify-content-center">

<div class="col-xl-6 col-lg-8">

<div class="section-title text-center"><h2 data-animate="fadeInUp" data-delay=".1">Ekibimiz</h2>

<p data-animate="fadeInUp" data-delay=".2">Tüm ekibimiz</p>
</div>
</div>
</div>

<div class="row justify-content-center">

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".3"> <img data-rjs="3" src="img/member1.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>John D. Pierce</h4>

<span>Font-End Developer
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".4"> <img data-rjs="3" src="img/member2.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>Latoya E. Lemons</h4>

<span>Senior Creative Designer
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".5"> <img data-rjs="3" src="img/member3.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>Walter J. Rodriguez</h4>

<span>Junior UI/UX Designer
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".6"> <img data-rjs="3" src="img/member4.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>Melissa W. Ahmed</h4>

<span>Senior Marketing Executive
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".7"> <img data-rjs="3" src="img/member5.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>Verna F. Robinson</h4>

<span>Junior Mrketing Officer
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".8"> <img data-rjs="3" src="img/member6.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>John T. Adams</h4>

<span>PHP Developer
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay=".9"> <img data-rjs="3" src="img/member7.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>Addie O. Nguyen</h4>

<span>Back-End Developer
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3 col-md-4 col-sm-6">

<div class="single-member" data-animate="fadeInUp" data-delay="1"> <img data-rjs="3" src="img/member8.jpg" alt="">

<div class="member-info bg-dark bg-rotate"><h4>Erik Y. Stone</h4>

<span>SEO Expert
</span>
<ul class="list-unstyled text-right">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div> </section> <section class="pt-175">

<div class="container">
<ul class="happy-counter list-unstyled clearfix">
<li>

<span data-count="3254">0
</span>

<p>Happy Clients</p></li>
<li>

<span data-count="388">0
</span>

<p>Current User</p></li>
<li>

<span data-count="12967">0
</span>

<p>Downloads</p></li>
<li>

<span data-count="9582">0
</span>

<p>Social Share</p></li>
<li>

<span data-count="954">0
</span>

<p>Cup of coffee</p></li>
</ul>
</div> </section>

@stop

@section("jss")

<script>

$(document).ready(function(){
  $(".main-footer").css("margin-top","300px");
});

</script>

@stop
