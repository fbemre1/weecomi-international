@extends("layouts.app")

@section("content")
<section class="title-bg-dark">
<div class="container">
<div class="page-title text-white text-center">
<h2 data-animate="fadeInUp" data-delay="1.2" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 1.2s;">
İletişim</h2>
<ul class="custom-breadcrumb karla list-unstyled m-0 animated fadeInUp" data-animate="fadeInUp" data-delay="1.4" style="animation-duration: 0.6s; animation-delay: 1.4s;">
<li class="breadcrumb-item"><a href="index.html">Anasayfa</a></li>
<li class="breadcrumb-item"><a href="#">İletişim</a></li>
</ul>
</div>
</div>
</section>

<section class="pt-120 pb-120">

<div class="container">

<div class="row">

<div class="col-md-6">

<div class="contact-content"><h2 data-animate="fadeInUp" data-delay=".1">Bns Cloud is Reputated Company Since 1998 in

<span>United States
</span></h2>

<p data-animate="fadeInUp" data-delay=".3">There are many variations of passages of Lorem Ipsum majority have suffered <a href="tel:+1234567890">(+1) 234-567-890</a></p>
<ul class="list-unstyled karla" data-animate="fadeInUp" data-delay=".5">
<li>

<span>We Are Open
</span> Mon - Sat (9am - 6pm)</li>
<li>

<span>Weekends
</span> Sunday</li>
</ul>
</div>
</div>

<div class="col-md-6">
<ul class="contact-info karla list-unstyled">
<li data-animate="fadeInUp" data-delay=".1">

<span>Address
</span>3430 Red Hawk Road
<br>Mora, MN 55051</li>
<li data-animate="fadeInUp" data-delay=".3">

<span>Email
</span> <a href="mailto:you@yourdomain.com">you@yourdomain.com</a>,
<br> <a href="mailto:your.mail@example.com">your.mail@example.com</a></li>
<li data-animate="fadeInUp" data-delay=".5">

<span>Phone
</span> <a href="tel:+01234567898">(+01) 2345-67898</a>,
<br> <a href="tel:+9876543210">(+98) 7654-3210</a></li>
</ul>
<ul class="contact-social-icons share-icons list-inline" data-animate="fadeInUp" data-delay=".7">
<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
</ul>
</div>
</div>

<div class="row">

<div class="col-lg-6" data-animate="fadeInUp" data-delay=".1">

<div class="map" data-trigger="map" data-map-options='{"latitude": "37.386052", "longitude": "-122.083851", "zoom": "15", "api_key": "AIzaSyCjkssBA3hMeFtClgslO2clWFR6bRraGz0"}'>
</div>
</div>

<div class="col-lg-6">

<div class="contact-form">

<form class="karla" action="http://themelooks.net/demo/bnscloud/html/preview/sendmail.php" method="post">

<div class="row mb-0">

<div class="col-md-6">
<input type="text" name="contactName" class="form-control" placeholder="Frist name" data-parsley-required-message="Enter your name." required data-animate="fadeInUp" data-delay=".1">
</div>

<div class="col-md-6">
<input type="text" name="contactLastName" class="form-control" placeholder="Last name" data-animate="fadeInUp" data-delay=".2">
</div>

<div class="col-md-6">
<input type="email" name="contactEmail" class="form-control" placeholder="E-mail" data-parsley-required-message="Enter your e-mail." required data-animate="fadeInUp" data-delay=".3">
</div>

<div class="col-md-6">
<input type="text" name="contactPhone" class="form-control" placeholder="Phone" data-parsley-required-message="Enter your phone number" data-parsley-minlength="10" data-parsley-minlength-message="Minimum 10 digit." required data-animate="fadeInUp" data-delay=".4">
</div>
</div>

<div class="position-relative">
<textarea name="contactMessage" class="form-control" placeholder="Write your text" data-parsley-required-message="Enter your message." required data-animate="fadeInUp" data-delay=".5">
</textarea>
</div> <button class="btn btn-primary" data-animate="fadeInUp" data-delay=".6">Post Comment</button>
</form>
</div>
</div>
</div>
</div>
</section> 

@stop
