@extends("layouts.app")
@section("title") Emre Denem @endsection
@section("content")
    <section class="title-bg-dark">
        <div class="container">
            <div class="page-title text-white text-center">
                <h2 data-animate="fadeInUp" data-delay="1.2" class="animated fadeInUp"
                    style="animation-duration: 0.6s; animation-delay: 1.2s;">
                    Sayfa Bulunamadı</h2>
            </div>
        </div>
    </section>

    <section class="pt-120 pb-120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="about-us-title text-center"><h2 data-animate="fadeInUp" data-delay=".1">404</h2>
                        <span data-animate="fadeInUp" data-delay=".2">Üzgünüz Aradığınız sayfayı bulamadık!.
</span>
                    </div>

                </div>
            </div>
        </div>
    </section>
@stop

@section("jss")

    <script>

        $(document).ready(function () {
            $(".main-footer").css("margin-top", "300px");
        });

    </script>

@stop
