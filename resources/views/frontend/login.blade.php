@extends('layouts.app')

@section("content")
<section class="pt-120 famino" id="eller">
<div class="container">

<div id="row">

  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>

<div class="col-md-5 formin formcontainer">
  <h2>Kullanıcı Girişi</h2>


<form action="{{ route('login') }}" method="POST" aria-label="{{ __('Login') }}">

@csrf
<div class="form-group">
<label>Kullanıcı adı</label>
<input type="text" class="form-control" name="username" required>
</div>

<div class="form-group">
<label>Şifreniz </label>
<input type="password" class="form-control" name="password" required>
</div>

@if (session()->get('error'))


          <div class="alert alert-danger">
                                      <strong>{!! session()->get('error') !!}</strong>
                                  </div>


@endif

<button type="submit" class="btn btn-primary">Gönder</button>
<a href="{{ route('password.request') }}" class="tekia ">Şifremi Unuttum ?</a>



</form>



</div>

<div class="col-md-5 formcontainer">
  <img class="registerico" src="https://seouzmaniyiz.com/wp-content/uploads/2017/11/seoroket.gif">
  <ul>

				<li>

					<span class="baslik">Hesabım bulunmuyor.</span>
					<a href="https://weecomi.com/yeni-uyelik/" title="Hemen Başvur">Hemen Başvur</a>
				</li>
				<li>

					<span class="baslik">Giriş bilgilerinizi mi unuttunuz. ?</span>
					<a href="https://weecomi.com/sifremi-unuttum/" title="Kullanıcı Adı / Şifre">Kullanıcı Adı / Şifre</a>
				</li>
				<li>

					<span class="baslik">Sorularınız mı var. ?</span>
					<a href="https://weecomi.com/313/iletisim/" title="İletişim">İletişim</a>
				</li>
			</ul>
</div>

<div class="clear"></div>

</div>

</div>
</section>
  @stop
