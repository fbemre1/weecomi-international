@extends("layouts.app")
@section("css")
<link rel="stylesheet" type="text/css" href="{{ URL::to("") }}/css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{ URL::to("") }}/css/toastr.min.css">
@stop

@section("content")
<section class="pt-120" id="eller">
  <div id='stars'></div>
  <div id='stars2'></div>
  <div id='stars3'></div>
<div class="container">

<div id="row">



<div class="col-md-12 formin formcontainer">
  <h2>Yeni Kayıt</h2>
<form action="" method="POST" id="registerform">
  @csrf
<div class="row">
<div class="col-md-4">
<div class="form-group">
<label>Sponsor Kodu</label>
<input type="text" class="form-control" name="reference_code" value="{{ old('reference_code') }}">
</div>

<div class="form-group">
<label>Kullanıcı adı </label>
<input type="text" class="form-control" name="username" value="{{ old('username') }}">
</div>

<div class="form-group">
<label>Adınız </label>
<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
</div>

<div class="form-group">
<label>Soyadınız </label>
<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
</div>

<div class="form-group">
<label>Tc Kimlik No </label>
<input type="text" class="form-control" name="identity" value="{{ old('identity') }}">
</div>

<div class="form-group">
<label>Email adresi </label>
<input type="email" class="form-control" name="email" value="{{ old('email') }}">
</div>


<div class="form-group">
<label>Doğum Tarihiniz </label>
<div class="form-group">
<input type="text" id="datepicker" class="form-control" name="birthday" value="{{ old('birthday') }}">
</div>
</div>


</div>




<div class="col-md-4">



<div class="form-group">
<label>Ülke </label>
<select class="form-control country" name="country">
<option selected disabled>Seçiniz</option>
@foreach($countries as $country)
<option value="{{ $country->id }}">{{ $country->name }}</option>
@endforeach
</select>
</div>

<div class="form-group">
<label>Şehir </label>
<select class="form-control city" name="city">
<option selected disabled>Seçiniz</option>
</select>
</div>

<div class="form-group">
<label>İlçe </label>
<input type="text" class="form-control" name="district" value="{{ old('district') }}">
</div>

<div class="form-group">
<label>Telefon no </label>
<input type="text" class="form-control" name="telephone" value="{{ old('telephone') }}">
</div>

<div class="form-group">
<label>Şifre </label>
<input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}">
</div>

<div class="form-group">
<label>Tekrar Şifre </label>
<input type="password"  class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
</div>

</div>


<div class="col-md-4">



  <div class="form-group">
  <label>Adres </label>
  <textarea class="form-control" rows="5" name="address">{{ old('address') }}</textarea>
  </div>

  <div class="form-group">
    <input type="checkbox" checked name="agree"> <a href="https://weecomi.com//up/sozlesme/sozlesme_tr.php" target="_blank" style="color:#fff;">Sözleşmeyi Okudum Şartları Kabul Ediyorum.</a>
  </div>

  <button type="submit" class="btn btn-primary">Kayıt Ol</button>

</div>



</div>

</form>
</div>



<div class="clear"></div>

</div>

</div>
</section>
@stop

@section("jss")
<script src="{{ URL::to('') }}/backend/assets/js/toastr.min.js"></script>
<script src="{{ URL::to('') }}/backend/assets/js/jquery.form.min.js"></script>
<script src="{{ URL::to("") }}/js/jquery-ui.js"></script>
<script src="{{ URL::to("") }}/js/datepicker-tr.js"></script>
<script src="{{ URL::to("") }}/js/register.js"></script>
@stop
