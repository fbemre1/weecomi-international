<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sponsor_code',
        'registrar_id',
        'identity',
        'firstname',
        'lastname',
        'email',
        'username',
        'password',
        'country',
        'city',
        'address',
        'district',
        'user_type',
        'birthday',
        'telephone',
        'avatar',
        'district',
        'package_id',
        'payment_id',
        'status',
        'settlement_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function package()
    {
        return $this->belongsTo('App\Models\Packages','package_id');
    }



}
