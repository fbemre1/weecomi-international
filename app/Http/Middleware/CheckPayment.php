<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use App\Models\Packages;
use App\Models\BankLists;

class CheckPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $user = User::where('username',$request->username)->first();

      if(isset($user)){
        if(Hash::check($request->password,$user->password)){

          if($user->package_id == 0 && $user->status == 0){
            // get all packages
           $packages = Packages::all();
           $banklists = BankLists::all();

            return new Response(view('Backend.payment')->with('packages',$packages)->with('user',$request)->with('banklists',$banklists));
          }
          
        }




      }
      return $next($request);


    }
}
