<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Session;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }


    protected function sendFailedLoginResponse(Request $request)
   {

       return redirect('/giris')->withErrors(['username']);
   }



    public function username()
{
        return 'username';
}

    public function showLoginForm(){
      return view("frontend.login");
    }


    protected function authenticated(Request $request, $user)
    {
        // Varsayılan Dili Ayarla. Ülkeye Göre.
       $lang = substr($request->server('HTTP_ACCEPT_LANGUAGE'),0,2);
       Session::put('locale', $lang);




    }



}
