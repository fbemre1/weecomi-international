<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Countries;
use App\Models\Cities;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {



        $messages = [
          'username.required'     => 'Kullanıcı adınızı giriniz.',
          'username.unique'       => 'Kullanıcı adı başkası tarafından kullanılıyor.',
          'username.min'       => 'Kullanıcı adı en az 4 karakterli olmalıdır.',
          'username.alpha_dash'   => 'Geçerli bir kullanıcı adınız olmalı.',
          'email.required'         => 'Mail adresinizi giriniz.',
          'email.email'            => 'Geçerli bir mail adresiniz olmalı.',
          'email.unique'           => 'Mail adresi başkası tarafından kullanılıyor.',
          'password.required'     => 'Şifreniz boş olmamalı.',
          'password.min'          => 'Şifreniz en az 4 karakterli olmalıdır.',
          'firstname.required'     => 'Adınızı giriniz',
          'firstname.min'          => 'Adınız en az 2 karakter olmalıdır.',
          'lastname.required'     => 'Soyadınızı giriniz',
          'lastname.min'          => 'Soyadınız en az 2 karakter olmalıdır.',
          'identity.required' => 'Lütfen kimlik numaranızı girin.',
          'sponsor_code.required' => 'Lütfen Sponsor kodunuzu girin.',
          'country.required' => 'Lütfen Ülke Seçiniz.',
          'city.required' => 'Lütfen Şehir Seçiniz.',
          'address.required' => 'Lütfen Adres Seçiniz.',
          'birthday.required' => 'Lütfen Doğum tarihi Seçiniz.',
          'district.required' => 'Lütfen İl Seçiniz.',
          'telephone.required' => 'Lütfen telefon Giriniz.',
];

$rules = array(
    'username'  => 'required|alpha_dash|unique:users|min:4',
    'email'      => 'required|email|unique:users',
    'password'  => 'required|min:4',
    'firstname' => 'required|min:2',
    'lastname' => 'required|min:2',
    'identity' => 'required',
    'sponsor_code' => 'required',
    'country' => 'required',
    'city' => 'required',
    'address' => 'required',
    'birthday' => 'required',
    'district' => 'required',
    'telephone' => 'required'
);

return Validator::make($data,$rules,$messages);



    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'sponsor_code' => $data['sponsor_code'],
            'registrar_id' => 0,
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'identity' => $data['identity'],
            'email' => $data['email'],
            'country' => $data['country'],
            'address' => $data['address'],
            'user_type' => 0,
            'city' => $data["city"],
            'status' => 0,
            'package_id' => 0,
            'district' => $data["district"],
            'birthday' => $data['birthday'],
            'telephone' => $data['telephone'],
            'avatar' => 'default.png',
            'password' => Hash::make($data['password']),

        ]);
    }

    public function showRegistrationForm(){
      $countries = Countries::all();
      return view("frontend.register")->with('countries',$countries);
    }


}
