<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealersController extends Controller
{
    public function get_index(){
      return view("frontend.dealers");
    }
}
