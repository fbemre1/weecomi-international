<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\User_notifications;

class UserNotificationsController extends Controller
{
    public function get_view(){

      if(Sentinel::getUser()->user_type == 1){
      $userID = null;
      }

      else{
      $userID = Sentinel::getUser()->id;
      }

      $user = User_notifications::where("user_id",$userID)->limit(5)->orderBy("id","DESC")->update(array("view" => 1));

    }
}
