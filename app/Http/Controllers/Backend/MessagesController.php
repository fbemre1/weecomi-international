<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Messages;
use App\Models\Messagebox;
use App\Models\Users;
use App\Models\User_notifications;
use Sentinel;
use Validator;

class MessagesController extends Controller
{
    public function get_index($id){

      $messages = Messages::where("msg_box_id",$id)->orderBy("id","DESC")->get();
      $users = Users::all();
      $messagebox = Messagebox::where("id",$id)->first();

      foreach($users as $write){
        $userinfo[$write->id] = [
          "fullname" => $write->first_name." ".$write->last_name,
        ];
      }

      foreach($messages as $warn){

        $data[$warn->id] = [
        "sender_name" => $userinfo[$warn->sender_id]["fullname"],
        "content" => $warn->content,
        "created_at" => $warn->created_at,
        ];

      }

      return view("backend.message")->with("messages",$data)->with("messagebox",$messagebox);
    }

    // Mesaj Gönder
    public function post_form(Request $request){

      $u = Messagebox::where("id",$request->msg_id)->first();


      if($u->receiver_id == null && Sentinel::getUser()->id != $u->sender_id && Sentinel::getUser()->user_type == 2){

        $receiver = $u->sender_id;
      }
      else{

        if(Sentinel::getUser()->user_type == 2){
          $receiver = null;
        }
        else{

          if($u->sender_id == Sentinel::getUser()->id){
            $receiver = $u->receiver_id;
          }
          else{
            $receiver = $u->sender_id;
          }


        }


      }





      // Konu açıldı kapatıldı. olayı.
      if(Sentinel::getUser()->id == 1){
        Messagebox::where("id",$request->msg_id)->update(array("status" => 0));
        $notify = "secondary";
      }
      else{
        Messagebox::where("id",$request->msg_id)->update(array("status" => 1));
        $notify = "success";
      }

      // validasyonlar.
            $validator = Validator::make($request->all(), [
                   'msg_id' => 'required',
                   'msg_content' => 'required',
               ]);

      // Eğer hata varsa.
           if( $validator->fails() ) {
              return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
           }

      try{

        $data = array(
          "sender_id" => Sentinel::getUser()->id,
          "msg_box_id" => $request->msg_id,
          "content" => $request->msg_content
        );

        $messages = Messages::create($data);

        // Bildirimi oluştur.
        $op = User_notifications::create(array("user_id" => $receiver,"parameter" => "mesajdetay/".$request->msg_id));

        return response(["notify" => $notify,"fullname" => Sentinel::getUser()->first_name." ".Sentinel::getUser()->last_name,"status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
      }
      catch(\Expcetion $e){
        return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
      }




    }



}
