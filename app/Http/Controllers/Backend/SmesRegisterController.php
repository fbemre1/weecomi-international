<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BankLists;
use App\Models\Countries;
use App\Models\Packages;
use App\Models\Smes;
use App\Models\Sme_sectors;
use App\User;
use Validator;
use Hash;
use Image;
use Auth;
use App;
use Session;


class SmesRegisterController extends Controller
{
  // Template çek getir.
    public function get_index(){
      // Paketleri getir.
      $packages = Packages::all();
      // Banka adlarını getir.
      $banks = BankLists::all();
      // Tüm ülkeleri getir
      $countries = Countries::all();

      // Sektörleri çek
      $sectors = Sme_sectors::all();
      // Dili Çek
      $lang = Session::get("locale");
      if($lang != null){
        $lang = "sector_".$lang;
      }else{
        $lang = "sector_tr";
      }


      return view("backend.smes")->with("lang",$lang)->with("sectors",$sectors)->with("packages",$packages)->with("countries",$countries)->with("banklists",$banks);
    }

    // formdan gelen verileri kaydet.
    public function post_form(Request $request){





      $validator = Validator::make($request->all(), [
         'sponsor_code' => 'required|unique:smes',
         'sector' => 'required',
         'store_name' => 'required',
         'title' => 'required',
         'taxador_number' => 'required',
         'username' => 'required',
         'email' => 'required',
         'telephone' => 'required',
         'discount_rate' => 'required',
         'maps' => 'required',
         'working_start' => 'required',
         'working_end' => 'required',
         'country' => 'required',
         'city' => 'required',
         'district' => 'required',
         'address' => 'required',
     ]);

// Eğer hata varsa.
     if( $validator->fails() ) {
        return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
     }


     try{

       $avatar = "";

       // eğer firma logosu varsa yükle.
               if($request->hasFile('file')){
                 $image = $request->file('file');
                 $filename = time().".".$image->getClientOriginalExtension();
                 $location = public_path('smes_logo/'.$filename);
                 Image::make($image)->resize(150,150)->save($location);
                 $avatar = $filename;

               }


       // eğer firma logosu yoksa default çek gitsin.
               if($avatar == null){
                 $avatar = "default.png";
               }


               $data = array(
                 "sponsor_code" => $request->sponsor_code,
                 "registrar_id" => Auth::user()->id,
                 "sector" => $request->sector,
                 "store_name" => $request->store_name,
                 "title" => $request->title,
                 "taxname" => $request->taxador_number,
                 "username" => $request->username,
                 "email" => $request->email,
                 "telephone" => $request->telephone,
                 "discount_rate" => $request->discount_rate,
                 "maps" => $request->maps,
                 "working_start" => $request->working_start,
                 "working_end" => $request->working_end,
                 "country" => $request->country,
                 "city" => $request->city,
                 "district" => $request->district,
                 "address" => $request->address,
                 "avatar" => $avatar,
                 "password" => Hash::make($request->password),
               );

               // kobiyi oluştur gitsin.
               $operation = Smes::create($data);


       return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
     }
     catch(\Exception $e){
       echo $e->getMessage();
     return response(["status" => "error","head" => "Hata","content" =>  "Bir Hata Oluştu" ]);
     }


    }



}
