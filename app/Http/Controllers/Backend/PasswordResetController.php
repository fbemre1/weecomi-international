<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Countries;
use Image;
use App\User;
use Validator;
use Illuminate\Support\Facades\View;
use Sentinel;
use Cartalyst\Sentinel\Hashing\BcryptHasher;


class PasswordResetController extends Controller
{


public function __construct()
{
    $this->middleware(function ($request, $next) {
        $user = $this->user= Sentinel::getUser();
        View::share('user', $user);
        return $next($request);
    });
}


// Template çek.
    public function get_index(){
      return view('backend.reset_password');
    }

// Formu post et.
    public function post_form(Request $request){



      try{
        // validasyonlar.
              $validator = Validator::make($request->all(), [
                     'password' => 'required',
                     'confirm_password' => 'required',
                 ]);

        // Eğer hata varsa.
             if( $validator->fails() ) {
                return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
             }

// dataları hazırla.
            $data = array(
              "password" => password_hash($request->password,PASSWORD_DEFAULT),
            );
// yolla.
              $operation = User::where("id",Sentinel::getUser()->id)->update($data);
              return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
            }
              catch(\Exception $e){
                echo $e->getMessage();
                return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
              }




    }



}
