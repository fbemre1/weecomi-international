<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Packages;
use App\Models\Packages_details;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Collection;
use DataTables;
use Validator;
class PackagesController extends Controller
{
    // template çek.
    public function get_index(){
      $packages = Packages::all();
      return view('backend.packages')->with("packages",$packages);
    }



    // Datatables için verileri döndür.
        public function get_data(){

          $packages = Packages::all();

          return Datatables::of($packages)->editColumn('created_at', function ($packages)
          {
            return $packages["created_at"];
          })->make(true);

        }

// idiye göre getir. güncellemek için.
        public function post_row(Request $request){
          $package = Packages::where("id",$request->id)->first();
          return response($package);
        }

// Düzenleme
        public function post_form(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',
                       'package_name' => 'required',
                       'package_price' => 'required',
                       'package_kdv' => 'required',
                       'package_factor' => 'required',
                       "package_limit" => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


                 try{
                   $data = array(
                     "name" => $request->package_name,
                     "price" => $request->package_price,
                     "tax" => $request->package_kdv,
                     "factor" => $request->package_factor,
                     "limit" => $request->package_limit,
                   );

          $operation = Packages::where("id",$request->id)->update($data);

          return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
           }
           catch(\Exception $e){
             echo $e->getMessage();
            return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
           }

        }

// Paket silme işlemi.
        public function post_delete(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


                 try{

          $operation = Packages::where("id",$request->id)->delete();

          return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
           }
           catch(\Exception $e){

            return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
           }

        }

// paket ekleme işlemi.
        public function post_add(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'package_name' => 'required',
                       'package_price' => 'required',
                       'package_kdv' => 'required',
                       'package_factor' => 'required',
                       'package_limit' => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


                 try{

                   $data = array(
                     "name" => $request->package_name,
                     "price" => $request->package_price,
                     "tax" => $request->package_kdv,
                     "factor" => $request->package_factor,
                     "limit" => $request->package_limit,
                   );

          $operation = Packages::create($data);

          return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
           }
           catch(\Exception $e){
             echo $e->getMessage();
            return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
           }

        }








}
