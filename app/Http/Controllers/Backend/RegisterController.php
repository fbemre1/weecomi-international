<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\Users;
use CodeGenerator;
use Validator;
use Redirect;

class RegisterController extends Controller
{
    // Back Office Kayıt İşlemleri
    public function post_form(Request $request){

      // Referer Koddan Registrar_id Bulma.
      $query = Users::where("reference_code",$request->reference_code)->first();
      $reference_code = CodeGenerator::generate($request->reference_code,"dealers");


      // validasyonlar.
            $validator = Validator::make($request->all(), [
               'reference_code' => ['required', 'exists:users'],
               'identity' => 'required',
               'username' => 'required|unique:users',
               'email' => 'required|unique:users',
               'first_name' => 'required',
               'last_name' => 'required',
               'password' => 'required',
               'confirm_password' => 'required',
               'country' => 'required',
               'city' => 'required',
               'district' => 'required',
               'address' => 'required',
               'birthday' => 'required',
               'telephone' => 'required',
               'agree' => 'required',
           ]);

      // Eğer hata varsa.
           if( $validator->fails() ) {
              return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
           }


  try{



      $credentials = [
      'reference_code' => $reference_code,
      'registrar_id' => $query->id,
      'parent_id' => '0',
      'settlement_status' => '0',
      'identity' => $request->identity,
      'username' => $request->username,
      'email' =>  $request->email,
      'password' => $request->password,
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'country' => $request->country,
      'city' => $request->city,
      'district' => $request->district,
      'address' => $request->address,
      'birthday' => $request->birthday,
      'telephone' => $request->telephone,
      'status' => "0",
      'avatar' => 'default.png',
      ];



      Sentinel::register($credentials);


      return response(["status" => "success","head" => "Girişe yönlendiriliyorsunuz.","content" => "Başarıyla Kaydoldunuz !"]);

    }
    catch(\Exception $e){
      return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
    }



    }
}
