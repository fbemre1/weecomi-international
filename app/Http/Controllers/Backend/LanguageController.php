<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App;
use Config;


class LanguageController extends Controller
{
    // dil işlemleri
    public function get_index($locale =null){
      $locales= ["tr","en"];
      if(in_array($locale,$locales)){
      Session::put('locale', $locale);
      return redirect()->back()->with("language",$locale);
      }




    }
}
