<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Packages_details;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Packages;
use DataTables;
use Validator;

class PackagesDetailsController extends Controller
{

// Paket detaylarını getir. idiye göre.
  public function get_details(){
    $packagedetails = Packages_details::all();

    $packages = Packages::all();
    $data = new Collection;

    foreach($packages as $package){
       $packageinfo[$package->id] = $package->name;
    }

    foreach($packagedetails as $write){

      $data->push([
        "id" => $write->id,
        "package_id" => $packageinfo[$write->package_id],
        "text" => $write->text,
        "created_at" => $write->created_at,
        "action" => null,
      ]);

    }

    return Datatables::of($data)->editColumn('created_at', function ($data)
    {
      return $data["created_at"];
    })->make(true);

  }

// Paket Detay ekle.
  public function post_detail(Request $request){



    // validasyonlar.
          $validator = Validator::make($request->all(), [
                 'package_id' => 'required',
                 'package_text' => 'required',
             ]);

    // Eğer hata varsa.
         if( $validator->fails() ) {
            return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
         }


           try{

             $data = array(
               "package_id" => $request->package_id,
               "text" => $request->package_text,
             );

    $operation = Packages_details::create($data);

    return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
     }
     catch(\Exception $e){

      return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
     }

  }


// Paket detay düzenle çek. tekli.
  public function post_detail_row(Request $request){
    $details = Packages_details::where("id",$request->id)->first();
    return response($details);

  }

// Paket Detay tekli Düzenle post
public function post_update(Request $request){

  // validasyonlar.
        $validator = Validator::make($request->all(), [
               'package_id' => 'required',
               'package_text' => 'required',
           ]);

  // Eğer hata varsa.
       if( $validator->fails() ) {
          return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
       }

         try{

           $data = array(
             "package_id" => $request->package_id,
             "text" => $request->package_text,
           );

  $operation = Packages_details::where("id",$request->id)->update(array("package_id" => $request->package_id,"text" => $request->package_text));

  return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
   }
   catch(\Exception $e){

    return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
   }


}



// Paket detay sil.
public function post_delete(Request $request){



  // validasyonlar.
        $validator = Validator::make($request->all(), [
               'id' => 'required',
           ]);

  // Eğer hata varsa.
       if( $validator->fails() ) {
          return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
       }

         try{


  $operation = Packages_details::where("id",$request->id)->delete();

  return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
   }
   catch(\Exception $e){

    return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
   }


}




}
