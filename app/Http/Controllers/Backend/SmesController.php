<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Sme_sectors;
use Validator;

class SmesController extends Controller
{
    // çek template
    public function get_index(){
      return view("backend.sme_sectors");
    }
    // Datatables için verileri döndür.
        public function get_data(){

          $sectors = Sme_sectors::all();


          return Datatables::of($sectors)->editColumn('register_date', function ($sectors)
          {

          })->make(true);

        }

        // Formdan gelen verileri update et
        public function post_update(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',
                       'sector_tr' => 'required',
                       'sector_en' => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


               try{

                 $data = $request->except("_token");
                 $operation = Sme_sectors::where("id",$request->id)->update($data);

                 return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
               }
               catch(\Expception $e){
                 return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
               }
        }

        // Formdan gelen verileri Sil
        function post_delete(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


               try{

                 $data = $request->except("_token");
                 $operation = Sme_sectors::where("id",$request->id)->delete($data);

                 return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
               }
               catch(\Expception $e){
                 return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
               }



        }


        // Formdan gelen verileri ekle
        function post_add(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'sector_tr' => 'required',
                       'sector_en' => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


               try{

                 $data = $request->except("_token");
                 $operation = Sme_sectors::create($data);

                 return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
               }
               catch(\Expception $e){
                 return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
               }



        }


}
