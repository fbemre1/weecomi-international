<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Countries;

class NewMessageController extends Controller
{
    public function get_index(){
      $countries = Countries::all();
      return view("backend.new_message")->with("countries",$countries);
    }
}
