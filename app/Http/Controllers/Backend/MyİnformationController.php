<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Countries;
use Image;
use App\User;
use Validator;
use Illuminate\Support\Facades\View;
use Sentinel;

class MyİnformationController extends Controller
{


public function __construct()
{
    $this->middleware(function ($request, $next) {
        $user = $this->user= Sentinel::getUser();
        View::share('user', $user);
        return $next($request);
    });
}


// Template çek.
    public function get_index(){
      $countries = Countries::all();
      return view('backend.member_info')->with("countries",$countries);
    }

// Formu post et.
    public function post_form(Request $request){

// validasyonlar.
      $validator = Validator::make($request->all(), [
             'firstname' => 'required',
             'lastname' => 'required',
             'identity' => 'required',
             'email' => 'required|email',
             'telephone' => 'required',
             'birthday' => 'required',
             'country' => 'required',
             'city' => 'required',
             'district' => 'required',
             'address' => 'required',
         ]);

// Eğer hata varsa.
     if( $validator->fails() ) {
        return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
     }


      try{


// avatar boşa çek.
        $avatar = "";

// eğer avatar varsa yükle.
        if($request->hasFile('file')){
          $image = $request->file('file');
          $filename = time().".".$image->getClientOriginalExtension();
          $location = public_path('user_avatar/'.$filename);
          Image::make($image)->resize(150,150)->save($location);
          $avatar = $filename;

        }


// eğer avatar yoksa eski avatar kalsın.
        if($avatar == null){
          $avatar = Sentinel::getUser()->avatar;
        }

// dataları hazırla.
            $data = array(
              "identity" => $request->identity,
              "first_name" => $request->firstname,
              "last_name" => $request->lastname,
              "email" => $request->email,
              "country" => $request->country,
              "city" => $request->city,
              "district" => $request->district,
              "address" => $request->address,
              "birthday" => $request->birthday,
              "avatar" => $avatar,
              "telephone" => $request->telephone
            );

// yolla.
              $operation = User::where("id",Sentinel::getUser()->id)->update($data);
              return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
            }
              catch(\Exception $e){

                return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
              }




    }



}
