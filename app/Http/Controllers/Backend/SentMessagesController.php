<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Validator;
use DB;
use App\Models\Messagebox;
use App\Models\Users;
use App\Models\Countries;

class SentMessagesController extends Controller
{
    public function get_index(){

      $userRole = Sentinel::getUser()->user_type;

      // KUllanıcılar tablosu
      $users = Users::all();
      foreach($users as $write){
        $userinfo[$write->id] = [
          "first_name" => $write->first_name,
          "last_name" => $write->last_name,
          "avatar" => $write->avatar,
        ];
      }



      if($userRole == "1"){
        $messages = Messagebox::where("user_type",1)->paginate(25);
        foreach($messages as $write){
          // alan şirketse. recevier sıfırla. ama alan kişi usersa doldur yolla baba.
          if($write->receiver_id){
            $receiver = ucfirst($userinfo[$write->receiver_id]["first_name"])." ".$userinfo[$write->receiver_id]["last_name"];
          }
          else{
            $receiver = null;
          }

          $messagebox[$write->id] = [
            "id" => $write->id,
            "receiver_name" => $receiver,
            "sender_name" => ucfirst($userinfo[$write->sender_id]["first_name"])." ".$userinfo[$write->sender_id]["last_name"],
            "sender_id" => $write->sender_id,
            "avatar" => $userinfo[$write->sender_id]["avatar"],
            "receiver_id" => $write->receiver_id,
            "subject" => $write->subject,
            "status" => $write->status,
            "category" => $write->category,
            "country" => $write->country,
            "city" => $write->city,
            "area" => $write->area,
            "user_type" => $write->user_type,
            "created_at" => $write->created_at,
          ];

          $messages = $messagebox;

        }



      }
      else{



        $messages = Messagebox::where("sender_id",Sentinel::getUser()->id)->paginate(25);
        foreach($messages as $write){

          // alan şirketse. recevier sıfırla. ama alan kişi usersa doldur yolla baba.
          if($write->receiver_id){
            $receiver = ucfirst($userinfo[$write->receiver_id]["first_name"])." ".$userinfo[$write->receiver_id]["last_name"];
          }
          else{
            $receiver = null;
          }

          $messagebox[$write->id] = [
            "id" => $write->id,
            "receiver_name" => $receiver,
            "sender_name" => ucfirst($userinfo[$write->sender_id]["first_name"])." ".$userinfo[$write->sender_id]["last_name"],
            "sender_id" => $write->sender_id,
            "avatar" => $userinfo[$write->sender_id]["avatar"],
            "receiver_id" => $write->receiver_id,
            "subject" => $write->subject,
            "status" => $write->status,
            "category" => $write->category,
            "country" => $write->country,
            "city" => $write->city,
            "area" => $write->area,
            "user_type" => $write->user_type,
            "created_at" => $write->created_at,
          ];

          $messages = $messagebox;

        }

      }

     //  if($user->inRole('super-admin')){
     //   $messages = Messagebox::all();
     //
     // }else{
     //   $messages = Messagebox::where("receiver_id",$user->id)->get();
     // }

     $countries = Countries::all();



      return view("backend.sent_messages")->with("messagebox",$messages)->with("countries",$countries);


    }
}
