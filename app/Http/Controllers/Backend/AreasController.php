<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Countries;
use App\Models\Cities;
use App\Models\Area_combining;
use App\Models\Areas;
use Sentinel;
use DataTables;
use Validator;

class AreasController extends Controller
{
    public function get_index(){
      $countries = Countries::all();
      $DefaultAreaName = Countries::where("id",Sentinel::getUser()->country)->first();
      return view("backend.areas")->with('countries',$countries)->with('defaultarea',$DefaultAreaName);
    }



    public function get_area(){



      $data = areas::all();


      return Datatables::of($data)->editColumn('created_at', function ($data)
      {
        return $data["created_at"];
      })->make(true);


     }

     public function post_area(Request $request){

       try{

         // validasyonlar.
               $validator = Validator::make($request->all(), [
                      'area_name' => 'required',
                      'country_id' => 'required',
                  ]);

         // Eğer hata varsa.
              if( $validator->fails() ) {
                 return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
              }

         $op = Areas::create($request->except("_token"));
         return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
       }
       catch(\Exception $e){
         return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
       }

     }

     public function edit_area(Request $request){

       try{

         // validasyonlar.
               $validator = Validator::make($request->all(), [
                      'area_name' => 'required',
                      'country_id' => 'required',
                      'id' => 'required',
                  ]);

         // Eğer hata varsa.
              if( $validator->fails() ) {
                 return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
              }


         $op = Areas::where("id",$request->id)->update($request->except("_token"));
         return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
       }
       catch(\Exception $e){
         return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
       }

     }

     public function post_city(Request $request){

       try{

         // validasyonlar.
               $validator = Validator::make($request->all(), [
                      'area_id' => 'required',
                      'city.*' => 'required',
                  ]);

         // Eğer hata varsa.
              if( $validator->fails() ) {
                 return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
              }


              foreach($request->city as $write){
                $op = Area_combining::create(array("area_id" => $request->area_id,"city_id" => $write));
              }



         return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
       }
       catch(\Exception $e){
         return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
       }

     }

     public function get_city($area_id){

         $area = Area_combining::where("area_id",$area_id)->get();

         $cities = Cities::all();

         foreach($cities as $write){
            $cities[$write->id] = $write->name;
         }

         foreach($area as $write){
           echo '
           <tr>
             <td>'.$write->id.'</td>
             <td>'.$cities[$write->city_id].'</td>
             <td><a href="javascript:;" id='.$write->id.' onclick="removecity(this)"><i class="icon-remove"></i></a></td>
           </tr>
              ';
         }



     }


     public function remove_city($id){

       try{


              $op = Area_combining::where("id",$id)->delete();


         return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
       }
       catch(\Exception $e){
         echo $e->getMessage();
         return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
       }



     }


}
