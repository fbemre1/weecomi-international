<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Sentinel;
use Illuminate\Support\Str;
use App\Models\Countries;

class TeamController extends Controller
{
    public function get_index(){

      $countries = Countries::all();

      foreach($countries as $write){
        $country[$write->id] = str_slug($write->sortname);
      }

      $child = Users::where("package_id",">",0)->get();

      foreach($child as $write){
        if(isset($country[$write->country])){
          $countryFlag=$country[$write->country];
        }else{
          $countryFlag="";
        }
          $new[$write->registrar_id][]=array(
            "fullname" => $write->first_name." ".$write->last_name,
            "id" => $write->id,
            "countryflag" => $countryFlag,
            "tel" => $write->telephone,
            "email" => $write->email,
            "packageID" => $write->package_id,
          );

      }




      return view("backend.team")->with("users",$new);
    }


public function get_child($id){


  $countries = Countries::all();

  foreach($countries as $write){
    $country[$write->id] = str_slug($write->sortname);
  }

  $child = Users::where("package_id",">",0)->get();


  foreach($child as $write){
    if(isset($country[$write->country])){
      $countryFlag=$country[$write->country];
    }else{
      $countryFlag="";
    }

      $new[$write->registrar_id][]=array(

        "fullname" => $write->first_name." ".$write->last_name,
        "id" => $write->id,
        "countryflag" => $countryFlag,
        "tel" => $write->telephone,
        "email" => $write->email,
      );

    }

    echo "<ul class='submenu'>";
    foreach($new[$id] as $write){

      if(isset($country[$write['countryflag']])){
        $countryFlag=$country[$write['countryflag']];
      }else{
        $countryFlag="none";
      }

      if(Sentinel::getUser()->id){
        $messageperm = '<span class="teamspanright"><a href=""> <i class="icon-pencil"> </i> </a></span>';
      }

      $icon=null;
      if(isset($new[$write['id']]) AND !empty($new[$write['id']])){
        $icon='<i class="icon-plus"></i>';
      }



      echo '<li><a href="javascript:;" class="clicks" id='.$write['id'].'>'.$icon.'</a><img src="/flags/'.$write['countryflag'].'.png" width="25px">'.$write['fullname'].'
      <span id="teamspan"> / '.$write['tel'].' - </span>
      <span id="teamspan">'.$write['email'].'</span>
      '.$messageperm.'
      <span class="teamspanright"> Kariyer </span>
      </li>';
    }
    echo "</ul>";   }





  // $countries = Countries::all();
  //
  // foreach($countries as $write){
  //   $country[$write->id] = str_slug($write->sortname);
  // }
  //
  // $users = Users::where("registrar_id",$id)->get();
  // echo "<ul class='submenu'>";
  // foreach($users as $write){
  //
  //   if(isset($country[$write->country])){
  //     $countryFlag=$country[$write->country];
  //   }else{
  //     $countryFlag="";
  //   }
  //
  //   if(Sentinel::getUser()->id){
  //     $adminperm = '<span class="teamspanright"><a href=""> <i class="icon-pencil"> </i> </a></span>';
  //   }
  //
  //   $icon=null;
  //   if(isset($users[$write['id']]) AND !empty($users[$write['id']])){
  //     $icon='<i class="icon-plus"></i>';
  //   }
  //
  //
  //
  //   echo '<li><a href="javascript:;" class="clicks" id='.$write->id.'>'.$icon.'</a><img src="/flags/'.$countryFlag.'.png" width="25px">'.$write->first_name." ".$write->last_name.'
  //   <span id="teamspan"> / '.$write['telephone'].' - </span>
  //   <span id="teamspan">'.$write['email'].'</span>
  //   '.$adminperm.'
  //   <span class="teamspanright"> Kariyer </span>
  //   </li>';
  // }
  // echo "</ul>";   }



}
