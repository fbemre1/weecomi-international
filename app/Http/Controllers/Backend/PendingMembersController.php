<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transfer_tickets;
use App\User;
use Validator;
use App\Models\BankLists;
use App\Models\Packages;
use DataTables;
use Illuminate\Database\Eloquent\Collection;

class PendingMembersController extends Controller
{
  // Template çek getir.
    public function get_index(){
      // Paketleri getir.
      $packages = Packages::all();
      // Banka adlarını getir.
      $banks = BankLists::all();


      return view("backend.pending_members")->with("packages",$packages)->with("banks",$banks);
    }

// Datatables için verileri döndür.
    public function get_data(){

      $pendingmembers = Transfer_tickets::all()->whereNotIn("status",[1]);
      $data = new Collection;

      $packages = Packages::all();

      foreach ($packages as $package) {
      $packageinfo[$package->id]=$package->name;
      }

      $users = User::all();


      foreach($users as $user){
        $userinfo[$user->id] = array(
          "sponsor_code" => $user->reference_code,
          "fullname" => $user->first_name." ".$user->last_name,
          "email" => $user->email,
          "telephone" => $user->email,
          "created_at" => $user->created_at,
        );
      }



      foreach($pendingmembers as $write){



            $data->push([
              "id" => $write->id,
              "sponsor_code" => $userinfo[$write->user_id]["sponsor_code"],
              "fullname" => $userinfo[$write->user_id]["fullname"],
              "email" => $userinfo[$write->user_id]["email"],
              "telephone" => $userinfo[$write->user_id]["telephone"],
              "package_name" => $packageinfo[$write->package_id],
              "register_date" => $userinfo[$write->user_id]["created_at"],
              "action" => ''
            ]);

      }


      return Datatables::of($data)->editColumn('register_date', function ($data)
      {
        return $data["register_date"];
      })->make(true);

    }

// Parasını yatırmış üyeleri onayla.
    public function post_approve(Request $request){

      // validasyonlar.
            $validator = Validator::make($request->all(), [
                   'id' => 'required',
               ]);

      // Eğer hata varsa.
           if( $validator->fails() ) {
              return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
           }


             try{

      // eğer parayı yatırmışsa onayla. havaleyi.
      $approveone = Transfer_tickets::where("id",$request->id)->update(array("status" => 1));
      // eğer parayı yatırmışşa onayla üyelikdeki ayarlarıda yap.
      $selectuser = Transfer_tickets::where("id",$request->id)->first();
      $approvetwo = User::where("id",$selectuser->user_id)->update(array("status" => 1));
      return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
       }
       catch(\Exception $e){
        return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
       }


    }



    // Üyelerin Havele bildirimlerini sil.
        public function post_delete(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',
                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }


                 try{

          $approve = Transfer_tickets::where("id",$request->id)->delete();
          return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
           }
           catch(\Exception $e){
            return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
           }


        }

// Üye düzenleme bilgileri çek.
        public function post_edit_get(Request $request){

         $get_info = Transfer_tickets::where("id",$request->id)->first();
         $select = User::where("id",$get_info->user_id)->first();

         $data = array(
           "firstname" => $select->firstname,
           "lastname" => $select->lastname,
           "email" => $select->email,
           "telephone" => $select->telephone,
           "package_id" => $get_info->package_id,
           "bank_id" => $get_info->bank_id,
         );

         return response($data);
        }


// Düzenleme formundan gelen verileri kaydet.
        public function post_form(Request $request){

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',
                       'firstname' => 'required',
                       'lastname' => 'required',
                       'email' => 'required',
                       'telephone' => 'required',
                       'package_id' => 'required',
                       'bank_id' => 'required',
                   ]);

                   // Eğer hata varsa.
                        if( $validator->fails() ) {
                           return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
                        }

                        try{
                          // ilk bildirimini düzenleyelim.
                          $package = Transfer_tickets::where("id",$request->id)->update(array("package_id" => $request->package_id,"bank_id" => $request->bank_id));
                          // daha sonra üye bilgilerini düzenleyelim.
                          $select = Transfer_tickets::where("id",$request->id)->first();
                          $user = User::where("id",$select->user_id)->update(array("firstname" => $request->firstname,"lastname" => $request->lastname,"email" => $request->email,"telephone" => $request->telephone));
                          return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
                        }
                        catch(\Exception $e){
                          return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
                        }


        }








}
