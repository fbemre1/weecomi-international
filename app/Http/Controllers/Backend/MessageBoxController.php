<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Messagebox;
use App\Models\Users;
use Sentinel;
use App\Models\Countries;
use App\Models\Messages;
use App\Models\User_notifications;
use DB;
use Validator;

class MessageBoxController extends Controller
{


    // Mesajları rütbeye göre gösterelim.
    public function get_index(){

      $userRole = Sentinel::getUser()->user_type;

      // KUllanıcılar tablosu
      $users = Users::all();
      foreach($users as $write){
        $userinfo[$write->id] = [
          "first_name" => $write->first_name,
          "last_name" => $write->last_name,
          "avatar" => $write->avatar,
        ];
      }



      if($userRole == "1"){
        $messages = Messagebox::where("receiver_id",null)->orwhere("receiver_id",Sentinel::getUser()->id)->whereNotIn("sender_id",[Sentinel::getUser()->id])->paginate(25);

        foreach($messages as $write){
          // alan şirketse. recevier sıfırla. ama alan kişi usersa doldur yolla baba.
          if($write->receiver_id){
            $receiver = ucfirst($userinfo[$write->receiver_id]["first_name"])." ".$userinfo[$write->receiver_id]["last_name"];
          }
          else{
            $receiver = null;
          }

          $messagebox[$write->id] = [
            "id" => $write->id,
            "receiver_name" => $receiver,
            "sender_name" => ucfirst($userinfo[$write->sender_id]["first_name"])." ".$userinfo[$write->sender_id]["last_name"],
            "sender_id" => $write->sender_id,
            "avatar" => $userinfo[$write->sender_id]["avatar"],
            "receiver_id" => $write->receiver_id,
            "subject" => $write->subject,
            "status" => $write->status,
            "category" => $write->category,
            "country" => $write->country,
            "city" => $write->city,
            "area" => $write->area,
            "user_type" => $write->user_type,
            "created_at" => $write->created_at,
          ];

          $messages = $messagebox;

        }



      }
      else{
        $messages = Messagebox::where("receiver_id",Sentinel::getUser()->id)->paginate(25);
        foreach($messages as $write){

          // alan şirketse. recevier sıfırla. ama alan kişi usersa doldur yolla baba.
          if($write->receiver_id){
            $receiver = ucfirst($userinfo[$write->receiver_id]["first_name"])." ".$userinfo[$write->receiver_id]["last_name"];
          }
          else{
            $receiver = null;
          }

          $messagebox[$write->id] = [
            "id" => $write->id,
            "receiver_name" => $receiver,
            "sender_name" => ucfirst($userinfo[$write->sender_id]["first_name"])." ".$userinfo[$write->sender_id]["last_name"],
            "sender_id" => $write->sender_id,
            "avatar" => $userinfo[$write->sender_id]["avatar"],
            "receiver_id" => $write->receiver_id,
            "subject" => $write->subject,
            "status" => $write->status,
            "category" => $write->category,
            "country" => $write->country,
            "city" => $write->city,
            "area" => $write->area,
            "user_type" => $write->user_type,
            "created_at" => $write->created_at,
          ];

          $messages = $messagebox;

        }

      }


     //  if($user->inRole('super-admin')){
     //   $messages = Messagebox::all();
     //
     // }else{
     //   $messages = Messagebox::where("receiver_id",$user->id)->get();
     // }

     $countries = Countries::all();




      return view("backend.message_box")->with("messagebox",$messages)->with("countries",$countries);
    }


    // Mesaj Gönderme Form POST
    public function post_form(Request $request){



      // validasyonlar.
            $validator = Validator::make($request->all(), [
                   'subject' => 'required',
                   'category' => 'required',
               ]);

      // Eğer hata varsa.
           if( $validator->fails() ) {
              return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
           }


      // dd($request->all());

try{
      $messagebox = array(
        "sender_id" => Sentinel::getUser()->id,
        "receiver_id" => $request->user_id,
        "status" => 1,
        "subject" => $request->subject,
        "category" => $request->category,
        "country" => $request->country,
        "city" => $request->city,
        "user_type" => Sentinel::getUser()->user_type,
      );

      // İlk önce Messagebox'a yaz.
      $op1 = Messagebox::create($messagebox);

      $message = array(
      "msg_box_id" => $op1->id,
      "sender_id" => Sentinel::getUser()->id,
      "content" => $request->message_content,
      );

      // Daha sonra mesajlar tablosuna. mesajı at.
      $op2 = Messages::create($message);

      // Daha sonra bildirimi oluştur. view varsayılan sıfırdır burdan yollamaya gerek yok. değeri
      $op3 = User_notifications::create(array("user_id" => $request->user_id,"parameter" => "mesajdetay/".$op1->id));


      return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
    }
    catch(\Exception $e){
      echo $e->getMessage();
      return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
    }



    }

    // İsim Getirme Servisi.
    public function get_users(Request $request){

      $term = $request->term;
      $data = Users::WhereRaw("concat(first_name, ' ', last_name) like '%$term%' ")->orwhere("username",$term)->orwhere("identity",$term)->orwhere("email",$term)->orwhere("telephone",$term)->get();

      foreach($data as $write){
        $newdata = [
        "id" => $write->id,
        "value" => $write->first_name." ".$write->last_name,
         ];
      }




      return response($newdata);

    }





}
