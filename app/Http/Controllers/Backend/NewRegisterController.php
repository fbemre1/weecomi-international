<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Countries;
use App\Models\Packages;
use App\Models\Banklists;
use App\Models\Sme_register_ids;
use App\Models\Settlement_tree;
use App\Models\Users;
use App\Models\Transfer_tickets;
use DateTime;
use Validator;
use View;
use Sentinel;
use CodeGenerator;

class NewRegisterController extends Controller
{

  public function __construct(){
      $this->middleware(function ($request, $next) {
          $user = $this->user= Sentinel::getUser();
          View::share('user', $user);
          return $next($request);
      });
  }

    // yeni kayit template gir.
    public function get_index(Request $request){
      $countries = Countries::all();
      $packages = Packages::all();
      $banklists = Banklists::all();
      // eğer ağaçtan butona tıklayıp geldiyse.
      $selectregister = $request->all();


      // Üye Ağac numarası. reference_code
      $unique = CodeGenerator::generate(1,"freedealers");

      return view("backend.new_register")->with("registerinfo",$selectregister)->with("countries",$countries)->with("reference_code",$unique)->with("packages",$packages)->with("banklists",$banklists);
    }


    // Form post
    public function post_form(Request $request){




      if($request->payment_method == 2){
       $bank = "required";
      }
      else{
        $bank = '';
      }

      if($request->parent_id == null){
        $parent_id = 0;
      }
      else{
        $parent_id = $request->parent_id;
      }
      if($request->left_side == null){
        $left_side = 0;
      }
      else{
        $left_side = $request->left_side;
      }
      if($request->right_side == null){
        $right_side = 0;
      }
      else{
        $right_side = $request->right_side;
      }





      // validasyonlar.
            $validator = Validator::make($request->all(), [
               'reference_code' => 'required|unique:users',
               'username' => 'required|unique:users',
               'first_name' => 'required',
               'last_name' => 'required',
               'identity' => 'required',
               'email' => 'required|unique:users',
               'password' => 'required',
               'confirm_password' => 'required',
               'telephone' => 'required',
               'birthday' => 'required',
               'country' => 'required',
               'city' => 'required',
               'district' => 'required',
               'address' => 'required',
               'package_id' => 'required',
               'payment_method' => 'required',
               'bank_id' => $bank,
           ]);

      // Eğer hata varsa.
           if( $validator->fails() ) {
              return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
           }



           try{

             $data = array(
             "reference_code" => $request->reference_code,
             "registrar_id" => $request->registrar_id,
             "identity" => $request->identity,
             "first_name" => $request->first_name,
             "last_name" => $request->last_name,
             "email" => $request->email,
             "username" => $request->username,
             "password" => password_hash($request->password,PASSWORD_DEFAULT),
             "country" => $request->country,
             "city" => $request->city,
             "district" => $request->district,
             "address" => $request->address,
             "birthday" => $request->birthday,
             "telephone" => $request->telephone,
             "avatar" => "default.png",
             "package_id" => $request->package_id,
             "settlement_status" => 0,
             'status' => 1,
             );


             $register = Users::create($data);

             for($i=0;$i<$register->package->limit;$i++){
               // Bayi Kobi idleri Üret.
               $smeunique = CodeGenerator::generate(1,"freesmes");
               $sme_ids = Sme_register_ids::create(array("u_id" => $register->id,"sme_id" => $smeunique));

             }




             // Eğer ödeme havale ile olacaksa.
                        if($request->payment_method == 2){
                          $status = 0;

                          $data = array(
                            "user_id" => $register->id,
                            "package_id" => $request->package_id,
                            "method_id" => $request->payment_method,
                            "bank_id" => $request->bank_id,
                            "status" => 0,
                          );

                          $tickets = Transfer_tickets::create($data);

                        }else{
                          $select = Users::where("id",$register->id)->update(array("status" => 1));
                          // Üyenin ağacını oluştur. eğer ağaçtan yeni kayıt butonuna basıp geldiyse.

                          $data = array(
                            "u_id" => $register->id,
                            "parent_id" => $parent_id,
                            "left_side" => 0,
                            "right_side" => 0,
                          );
                          $create_tree = Settlement_tree::create($data);
                          // Üye Eğer sol tarafa eklenecekse. eğer ağaçtan yeni kayıt butonuna basıp geldiyse.
                          if($left_side == 1){
                            $updatetree = Settlement_tree::where("id",$parent_id)->update(array("left_side" => $create_tree->id));
                            // eğer üye yerleştiyse ağaca yerleşim bekleyenlerden durumunu 1 yapalım..
                            $updateuser = Users::where("id",$register)->update(array("settlement_status" => 1));
                          }
                          if($right_side == 1){
                            $updatetree = Settlement_tree::where("id",$parent_id)->update(array("right_side" => $create_tree->id));
                            // eğer üye yerleştiyse ağaca yerleşim bekleyenlerden durumunu 1 yapalım..
                            $updateuser = Users::where("id",$register)->update(array("settlement_status" => 1));
                          }



                        }



             return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
           }
           catch(\Exception $e){

           return response(["status" => "error","head" => "Hata","content" =>  "Bir Hata Oluştu" ]);
           }




    }

    // Yerleşim seç kaydı.
        public function get_template(Request $request){
          // Sponsor kişiyi bul.
          $user = Settlement_tree::find($request->parent_id);
          $sponsor = User::find($user->u_id);
          // Yerleşim yapılacak üyenin bilgileri.
          $settlement_user = User::find($request->user_id);
          // Hidden bilgileri gönder.
          $hidden = array(
            "registrar_id" => Auth::user()->id,
            "user_id" => $request->user_id,
            "parent_id" => $request->parent_id,
            "left_side" => $request->left_side,
            "right_side" => $request->right_side
          );

          return view("backend.settlement_register")->with("hidden",$hidden)->with("sponsor",$sponsor)->with("settlement_user",$settlement_user);
        }



    // Yerleşim kayıdı yap formdan gelen veriler.
    public function post_settlement(Request $request){


      // validasyonlar.
            $validator = Validator::make($request->all(), [
                   'parent_id' => 'required',
                   'user_id' => 'required',
               ]);

      // Eğer hata varsa.
           if( $validator->fails() ) {
              return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
           }


           try{

             if($request->left_side == null){
               $left_side = 0;
             }
             else{
               $left_side = $request->left_side;
             }
             if($request->right_side == null){
               $right_side = 0;
             }
             else{
               $right_side = $request->right_side;
             }


             // Ağacını oluştur.

             $parentfind = settlement_tree::where("id",$request->parent_id)->first();


             $data = array(
               "u_id" => $request->user_id,
               "parent_id" => $parentfind->parent_id,
               "left_side" => 0,
               "right_side" => 0,
             );
             $op = settlement_tree::create($data);
             // sağmı solmu eklenecek ayırt et. ekle gitsin.

             if($left_side == 1){
               $operation = Settlement_tree::where("u_id",$parentfind->u_id)->update(array("left_side" => $op->id));
               $updateuser = User::where("id",$request->user_id)->update(array("settlement_status" => 1));
             }
             if($right_side == 1){
               $operation = Settlement_tree::where("u_id",$parentfind->u_id)->update(array("right_side" => $op->id));
               $updateuser = User::where("id",$request->user_id)->update(array("settlement_status" => 1));
             }

             return response(["status" => "success","head" => "İşlem Başarılı","content" => "Değişiklikler Kaydedildi"]);
           }
           catch(\Exception $e){

             return response(["status" => "error","head" => "Hata","content" =>  "Bir Hata Oluştu" ]);
           }

    }




}
