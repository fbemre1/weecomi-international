<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BankLists;
use App\Models\Packages;
use DataTables;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Transfer_tickets;
use App\User;
use Validator;

class SettlementWaitMembers extends Controller
{
    // template çek.
    function get_index(){
      // Paketleri getir.
      $packages = Packages::all();
      // Banka adlarını getir.
      $banks = BankLists::all();

      return view("backend.settlement_wait_members")->with("packages",$packages)->with("banks",$banks);
    }

    // Datatables için verileri döndür.
        public function get_data(){
// ödemelerini yapmış. ama yerleşmemiş üyeleri getir.
          $settlementmembers = User::where("settlement_status",0)->where("status",1)->get();
          $data = new Collection;


          $packages = Packages::all();

            foreach ($packages as $package) {
              $list[$package->id]=$package->name;
            }

          foreach($settlementmembers as $write){

              $package_name = $list[$write->package_id];

              $data->push([
                "id" => $write->id,
                "sponsor_code" => $write->reference_code,
                "fullname" => ucfirst($write->first_name)." ".$write->last_name,
                "email" => $write->email,
                "telephone" => $write->telephone,
                "package_name" => $package_name,
                "register_date" => $write->created_at,
                "action" => ''
              ]);

          }

          return Datatables::of($data)->editColumn('register_date', function ($data)
          {
            return $data["register_date"];
          })->make(true);

        }

// yerleşim bekleyen üyeler sil.
        public function post_delete(Request $request){
          // sil derkene durumu 0 yap ordan onaylanmamışa düşsün ordanda silerse siler.

          // validasyonlar.
                $validator = Validator::make($request->all(), [
                       'id' => 'required',

                   ]);

          // Eğer hata varsa.
               if( $validator->fails() ) {
                  return response(["status" => "error","head" => "Hata","content" => $validator->errors()->all() ]);
               }

                 try{



          $operation = User::where("id",$request->id)->delete();

          return response(["status" => "success","head" => "İşlem Başarılı","content" => "Yaptığınız işlem başarılı bir şekilde gerçekleşti."]);
           }
           catch(\Exception $e){

            return response(["status" => "error","head" => "Hata","content" => "Bir Hata Oluştu" ]);
           }

        }








}
