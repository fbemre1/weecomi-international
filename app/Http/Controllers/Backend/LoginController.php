<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Redirect;
use Session;
use Carbon\Carbon;


class LoginController extends Controller
{
    public function post_form(Request $request){


       $credentials = [
         "username" => $request->username,
         "password" => $request->password,
       ];

     if(Sentinel::forceAuthenticate($credentials)){
       $lang = substr($request->server('HTTP_ACCEPT_LANGUAGE'),0,2);
       Session::put('locale', $lang);

       return redirect('/panel');
     }
     else{
       return redirect('giris')->with('error', 'Kullanıcı adı veya şifre yanlış.');

     }


    }
}
