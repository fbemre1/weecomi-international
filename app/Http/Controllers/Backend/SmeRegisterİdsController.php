<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Sme_register_ids;
use App\Models\Smes;
use DataTables;
use App\User;
use Auth;


class SmeRegisterİdsController extends Controller
{
  // Template çek.
    public function get_index(){
      return view("backend.sme_register_ids");
    }

    // Datatables için verileri döndür.
        public function get_data(){

          $Smeids = Sme_register_ids::where("u_id",Auth::user()->id)->get();

          $data = new Collection;

          $smes = Smes::all();

          foreach($smes as $write){
            $userinfo[$write->sponsor_code] = $write->title;
          }






          foreach($Smeids as $write){

            $data->push([
              "id" => $write->id,
              "sme_name" => @$userinfo[$write->sme_id],
              "sme_id" =>  $write->sme_id,
              "created_at" => $write->created_at,
            ]);

          }

          return Datatables::of($data)->editColumn('created_at', function ($data)
          {
            return $data["created_at"];
          })->make(true);

        }



}
