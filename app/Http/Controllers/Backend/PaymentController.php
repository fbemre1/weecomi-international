<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Models\Payments;
use App\Models\Transfer_tickets;

class PaymentController extends Controller
{
    // get payments
    public function get_payments(Request $request){
      header('Access-Control-Allow-Origin: *');
      try{
      $userfind = User::where('username',$request->user_id)->first();

if($userfind->package_id == 0){
        $packageupdate = User::where('username',$request->user_id)->update(array('package_id' => $request->package_id,'status' => 1));

        $data = array(
          "user_id" => $userfind->id,
          "method" => $request->method_id,
          "total_amount" => $request->total_amount,
        );
        $payments = Payments::create($data);
        return response(["status" => 200]);
      }


    }
    catch(\Exception $e){
      echo $e->getMessage();
    }


    }

    // get_tickets
    public function post_tickets(Request $request){



      $userfind = User::where('username',$request->userid)->first();

      $checktickets = Transfer_tickets::where('user_id',$userfind->id)->first();

      if(count($checktickets) > 0){
        return response(["status" => "error","head" => "Hata","content" => "Ödeme bildirimi zaten yapmışsınız."]);
        return false;

      }


      try{


      $data = array(
        "user_id" => $userfind->id,
        "package_id" =>$request->packageid,
        "method_id" =>$request->methodid,
        "bank_id" =>$request->bankselector,
        "status" => 0,
      );

      $operation = Transfer_tickets::create($data);
      return response(["status" => "success","head" => "İşlem Başarılı","content" => "Ödeme bildiriminiz başarıyla yapıldı. en kısa sürede hesabınız incelenip açılacaktır."]);
    }
    catch(\Exception $e){
      return response(["status" => "error","head" => "Hata","content" => $e->getMessage() ]);
    }




    }




}
