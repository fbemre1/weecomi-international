<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Response;
use App\Models\Packages;
use App\Models\BankLists;
use App\Models\User_notifications;
use App\Http\Controllers\PaymentController;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

// Notifications


      view()->composer('*', function ($view)
          {
              $user = request()->user();


              if($user){


                if($user->user_type == "1"){
                  $notifications = User_notifications::where("user_id",null)->where("view",0)->offset(0)->limit(5)->get();
                }
                else{
                  // Eğer giriş. yapan bayi ise
                  $notifications = User_notifications::where("user_id",$user->id)->where("view",0)->offset(0)->limit(5)->get();
                }

                $view->with('notifications', $notifications);




              }



          });




    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
