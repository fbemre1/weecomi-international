<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $fillable = [
    "area_name",
    "country_id",
    ];

    protected $table = "areas";

}
