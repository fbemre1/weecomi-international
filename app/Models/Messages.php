<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = [
    "sender_id",
    "msg_box_id",
    "content",
    ];

    protected $table = "messages";

}
