<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_notifications extends Model
{
    protected $fillable = [
    "user_id",
    "parameter",
    "view",
    ];

    protected $table = "user_notifications";
}
