<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sme_register_ids extends Model
{

    protected $table = "sme_ids";
    protected $fillable = [
      "sme_id",
      "u_id",
    ];


    public function smename()
    {
        return $this->belongsTo('App\Models\Smes','sponsor_code');
    }

}
