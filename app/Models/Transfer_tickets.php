<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer_tickets extends Model
{
    protected $table = "transfer_tickets";

    protected $fillable = [
      "user_id",
      "package_id",
      "method_id",
      "bank_id",
      "status"
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function bank(){
      return $this->belongsTo('App\Models\Banklists','bank_id');
    }

    public function package(){
      return $this->belongsTo('App\Models\Packages','package_id');
    }


}
