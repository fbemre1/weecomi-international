<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packages_details extends Model
{
    protected $table = "packages_details";

    protected $fillable = [
      "text",
      "package_id"
    ];

}
