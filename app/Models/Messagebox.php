<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messagebox extends Model
{
    protected $fillable = [
    "sender_id",
    "receiver_id",
    "status",
    "subject",
    "user_type",
    "category",
    "country",
    "city",
    "area",
    ];

    protected $table = "messagebox";
}
