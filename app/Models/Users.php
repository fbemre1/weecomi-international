<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class Users extends SentinelUser
{
    protected $fillable = [
      "reference_code",
      "old_reference_code",
      "registrar_id",
      "parent_id",
      "settlement_status",
      "identity",
      "username",
      "email",
      "password",
      "old_password",
      "permissions",
      "last_login",
      "first_name",
      "last_name",
      "country",
      "city",
      "area_id",
      "district",
      "address",
      "birthday",
      "telephone",
      "avatar",
      "status",
      "package_id",
      "settlement_model",
      "register_date",
      "package_date",
      "career_id",
      "uni_career_id",
      "old_user_id",
      "push_id",
      "sponsor_user_type",
      "user_type",
      "invoice_status",
];

protected $loginNames = ['username'];

public function package()
{
    return $this->belongsTo('App\Models\Packages','package_id');
}



}
