<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area_combining extends Model
{
    protected $fillable = [
    "area_id",
    "city_id",
    ];

    protected $table = "area_combining";

}
