<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settlement_tree extends Model
{
    protected $table = "settlement_tree";
    protected $fillable = [
      "u_id",
      "parent_id",
      "left_side",
      "right_side"
    ];

    public function user(){
        return $this->belongsTo('App\User','u_id');
    }



}
