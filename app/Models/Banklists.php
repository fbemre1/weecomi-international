<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banklists extends Model
{
    protected $table = "banklists";

    protected $fillable = [
      "bank_name",
      "branch_code",
      "holder_name",
      "account_no",
      "iban_no",
      "bank_image",
    ];


}
