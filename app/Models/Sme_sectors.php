<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sme_sectors extends Model
{
    protected $table = "smes_sectors";

    protected $fillable = [
      "sector_tr",
      "sector_en",
    ];

}
