<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class Smes extends SentinelUser
{
    protected $table = "smes";

    protected $fillable = [
      "reference_code",
      "old_reference_code",
      "registrar_id",
      "email",
      "username",
      "password",
      "old_password",
      "country",
      "city",
      "area_id",
      "district",
      "address",
      "telephone",
      "avatar",
      "sector",
      "store_name",
      "fullname",
      "taxname",
      "maps",
      "weekdays",
      "weekend",
      "sme_status",
      "sme_show",
      "sme_note",
      "register_date",
      "old_sme_id",      
    ];


    protected $loginNames = ['username'];

}
