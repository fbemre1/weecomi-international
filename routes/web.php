<?php

// Dil İşlemleri
Route::get('/lang/{locale?}','Backend\LanguageController@get_index');

// Weecomi Ülke,Şehir [Start]
Route::get('sehirler/{id}','Backend\CCDController@get_cities');
Route::get('sehirleracik/{id}','Backend\CCDController@get_publiccities');
// Weecomi Ülke,Şehir [Stop]

// Weecomi Paketleri [Start]
Route::get('paketgetir/{id}','Backend\PackagesController@get_packages');
Route::get('paketdetay/{id}','Backend\PackagesDetailsController@get_details');
Route::get('paketdetaymodal/{id}','Backend\PackagesDetailsController@get_detailsformodal');

// Weecomi Paketleri [Stop]


// Weecomi Backend [Start]
  Route::group(['prefix' => 'panel',  'middleware' => 'sentinelauth'], function(){
  Route::get('/','Backend\HomeController@get_index');

  Route::get('uyelikbilgilerim','Backend\MyİnformationController@get_index');
  Route::post('uyelikbilgilerim','Backend\MyİnformationController@post_form');

  Route::get('odemeyap','Backend\PaymentController@get_payments');
  Route::post('bildirimyap','Backend\PaymentController@post_tickets');
  Route::get('sifredegistir','Backend\PasswordResetController@get_index');
  Route::post('sifredegistir','Backend\PasswordResetController@post_form');

  Route::get('seoayarlari','Backend\SeoSettingsController@get_index');


  Route::get('paketayarlari','Backend\PackagesController@get_index');
  Route::post('paketayarlari','Backend\PackagesController@post_form');
  Route::get('paketayarlaricek','Backend\PackagesController@get_data');
  Route::get('paketdetaygetir','Backend\PackagesDetailsController@get_details');
  Route::post('paketdetayduzenlecek','Backend\PackagesDetailsController@post_detail_row');
  Route::post('paketdetayduzenle','Backend\PackagesDetailsController@post_update');
  Route::post('paketdetaysil','Backend\PackagesDetailsController@post_delete');
  Route::post('paketgetir','Backend\PackagesController@post_row');
  Route::post('paketsil','Backend\PackagesController@post_delete');
  Route::post('paketekle','Backend\PackagesController@post_add');
  Route::post('paketdetayekle','Backend\PackagesDetailsController@post_detail');


  Route::get('onaybekleyenuyeler','Backend\PendingMembersController@get_index');
  Route::post('onaybekleyenuyelersil','Backend\PendingMembersController@post_delete');
  Route::post('onaybekleyenuyeleronayla','Backend\PendingMembersController@post_approve');
  Route::get('onaybekleyenuyelercek','Backend\PendingMembersController@get_data');
  Route::post('onaybekleyenuyelerduzenlegetir','Backend\PendingMembersController@post_edit_get');
  Route::post('onaybekleyenuyeler','Backend\PendingMembersController@post_form');
  Route::get('yerlesimagacim/{any?}','Backend\SettlementTreeController@get_index');
  Route::get('yerlesimagacimcek/{any?}','Backend\SettlementTreeController@get_service');
  Route::get('yerlesimkayit/{any?}','Backend\NewRegisterController@get_template');
  Route::post('yerlesimkayit','Backend\NewRegisterController@post_settlement');
  Route::get('yenikayit','Backend\NewRegisterController@get_index')->name('yenikayit');
  Route::post('yenikayit','Backend\NewRegisterController@post_form');

  Route::get('yerlesimbekleyenuyeler','Backend\SettlementWaitMembers@get_index');
  Route::get('yerlesimbekleyenuyelercek','Backend\SettlementWaitMembers@get_data');
  Route::post('yerlesimbekleyenuyelersil','Backend\SettlementWaitMembers@post_delete');

  Route::get('kobikayit','Backend\SmesRegisterController@get_index');
  Route::post('kobikayit','Backend\SmesRegisterController@post_form');

  Route::get('sektorayarlari','Backend\SmesController@get_index');
  Route::get('sektorcek','Backend\SmesController@get_data');
  Route::post('sektorguncelle','Backend\SmesController@post_update');
  Route::post('sektorsil','Backend\SmesController@post_delete');
  Route::post('sektorekle','Backend\SmesController@post_add');


  Route::get('kobikayitidleri','Backend\SmeRegisterİdsController@get_index');
  Route::get('kobikayitidlericek','Backend\SmeRegisterİdsController@get_data');


  Route::get('gelenkutusu','Backend\MessageBoxController@get_index');
  Route::get('gonderilmis','Backend\SentMessagesController@get_index');
  Route::get('gelenkutusukulgetir','Backend\MessageBoxController@get_users');
  Route::post('gelenkutusu','Backend\MessageBoxController@post_form');

  Route::get('mesajdetay/{id}','Backend\MessagesController@get_index');
  Route::post('mesajdetay/{id}','Backend\MessagesController@post_form');
  Route::get('mesajolustur','Backend\NewMessageController@get_index');

  Route::get('bildirimler','Backend\UserNotificationsController@get_view');

  Route::get('ekibim','Backend\TeamController@get_index');
  Route::get('ekibimgetir/{id}','Backend\TeamController@get_child');

  Route::get('bolgeolustur','Backend\AreasController@get_index');
  Route::post('bolgeolustur','Backend\AreasController@post_area');
  Route::post('bolgeduzenle','Backend\AreasController@edit_area');
  Route::get('bolgegetir','Backend\AreasController@get_area');
  Route::post('sehirekle','Backend\AreasController@post_city');
  Route::get('sehirduzenle/{area_id}','Backend\AreasController@get_city');
  Route::get('sehirsil/{id}','Backend\AreasController@remove_city');


});
// Weecomi Backend [Stop]

// Authentication [Start]
Route::get('giris', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('giris', 'Backend\LoginController@post_form');
Route::post('cikis', 'Auth\LoginController@logout')->name('logout');

Route::get('kayit', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('kayit', 'Backend\RegisterController@post_form');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Authentication [Stop]

// Weecomi Frontend [Start]
//Route::get('giris','Frontend\LoginController@get_index');
//Route::get('kayit','Frontend\RegisterController@get_index');
Route::get('/', 'Frontend\HomeController@get_index')->name('home');
Route::get('hakkimizda','Frontend\AboutController@get_index')->name('hakkimizda');
Route::get('kurucumuz','Frontend\OwnerController@get_index')->name('kurucumuz');
Route::get('medyadabiz','Frontend\MediaController@get_index')->name('medyadabiz');
Route::get('belgelerimiz','Frontend\DocumentsController@get_index')->name('belgelerimiz');
Route::get('kobilerimiz','Frontend\SmesController@get_index')->name('kobilerimiz');
Route::get('bayilerimiz','Frontend\DealersController@get_index')->name('bayilerimiz');
Route::get('nedir','Frontend\WhatController@get_index')->name('nedir');
Route::get('iletisim','Frontend\ContactController@get_index')->name('iletisim');
// Weecomi Frontend [Stop]
